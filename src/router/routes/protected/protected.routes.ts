import { lazy } from "react";
import { _RouteObject } from "react-router-dom";

import { createRoute } from "../create-route";
import { protectedRoutePaths } from "..";

const PrivateLayout = lazy(() => import("@/core/components/PrivateLayout"));

// service providers
const ServiceProviders = lazy(
    () => import("@/pages/protected/ServiceProviders")
);
const SetServiceProvider = lazy(
    () => import("@/pages/protected/ServiceProviders/pages/SetServiceProvider")
);

// videos
const VideosRoot = lazy(() => import("@/pages/protected/Videos"));
const VideosHome = lazy(() => import("@/pages/protected/Videos/pages/Home"));
const VideoPlay = lazy(
    () => import("@/pages/protected/Videos/pages/VideoPlay")
);
const ConversionLog = lazy(
    () => import("@/pages/protected/Videos/pages/ConversionLogs")
);

// students report
const StudentReports = lazy(() => import("@/pages/protected/StudentsReport"));

// path revalidation
const PathRevalidation = lazy(
    () => import("@/pages/protected/PathRevalidation")
);

const MockTestBoard = lazy(() => import("@/pages/protected/MockTestBoard"));
const MockTestBoardExamType = lazy(
    () => import("@/pages/protected/MockTestBoard/components/Exam")
);
const MockTestBoardQuiz = lazy( 
    () => import("@/pages/protected/MockTestBoard/components/NewQuizes")
);

const NotFound = lazy(() => import("@/pages/shared/NotFound"));

export const protectedRoutes: _RouteObject<"protected">[] = [
    createRoute({
        path: protectedRoutePaths.base,
        element: PrivateLayout,
        children: [
            // service providers
            createRoute({
                path: protectedRoutePaths.serviceProviders,
                element: ServiceProviders,
            }),
            createRoute({
                path: protectedRoutePaths.createServiceProvider,
                element: SetServiceProvider,
            }),
            createRoute({
                path: protectedRoutePaths.updateServiceProvider,
                element: SetServiceProvider,
            }),

            // videos
            createRoute({
                path: protectedRoutePaths.videos,
                element: VideosRoot,
                children: [
                    createRoute({
                        path: protectedRoutePaths.videos,
                        element: VideosHome,
                    }),
                    createRoute({
                        path: protectedRoutePaths.videoPlay,
                        element: VideoPlay,
                    }),
                    createRoute({
                        path: protectedRoutePaths.conversionLog,
                        element: ConversionLog,
                    }),
                ],
            }),

            // students report
            createRoute({
                path: protectedRoutePaths.studentsReport,
                element: StudentReports,
            }),

            // path revalidation
            createRoute({
                path: protectedRoutePaths.pathRevalidation,
                element: PathRevalidation,
            }),

            createRoute({
                path: protectedRoutePaths.mockTestBoard,
                element: MockTestBoard,
            }),
            createRoute({
                path: protectedRoutePaths.examType,
                element: MockTestBoardExamType,
            }),
            createRoute({
                path: protectedRoutePaths.quiz,
                element: MockTestBoardQuiz,
            }),

            createRoute({
                path: "*",
                element: NotFound,
            }),
        ],
    }),
];
