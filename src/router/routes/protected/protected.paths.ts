export const protectedRoutePaths = {
    base: "/",

    // service providers
    serviceProviders: "/service-providers",
    createServiceProvider: "/service-providers/create",
    updateServiceProvider: "/service-providers/:providerId/update",

    // videos
    videos: "/videos",
    videoPlay: "/videos/:filename",
    conversionLog: "/videos/conversion-log",

    // students report
    studentsReport: "/students-report",

    // path revalidation
    pathRevalidation: "/revalidate-paths",

    mockTestBoard: "/mock-test-board",
    examType: "/mock-test-board/exam",
    quiz: "/mock-test-board/exam/quiz",
};
