import { lazy } from "react";
import { _RouteObject } from "react-router-dom";

import { createRoute } from "../create-route";
import { publicRoutePaths } from "./public.paths";

const SignIn = lazy(() => import("@/pages/public/SignIn"));
const HomePage = lazy(() => import("@/pages/public/HomePage"));
const University = lazy(() => import("@/pages/public/University"));
const UniversityDetails = lazy(
  () => import("@/pages/public/UniversityDetails")
);
const Course = lazy(() => import("@/pages/public/Course"));
const Categories = lazy(() => import("@/pages/public/Category"));
const PublicLayout = lazy(() => import("@/core/components/PublicLayout"));
const Consultancy = lazy(() => import("@/pages/public/Consultancy"));
const Blogs = lazy(() => import("@/pages/public/Blogs"));
const BlogsPage = lazy(() => import("@/pages/public/BlogPage"));
const Find = lazy(() => import("@/pages/public/Find"));
const StepFormWrapper = lazy(
  () => import("@/pages/public/Find/components/StepFormWrapper")
);
const Contact = lazy(() => import("@/pages/public/ContactUs"));

export const publicRoutes: _RouteObject<"public">[] = [
  createRoute({
    path: publicRoutePaths.base,
    element: PublicLayout,
    children: [
      createRoute({
        path: publicRoutePaths.signin,
        element: SignIn,
      }),

      createRoute({
        path: publicRoutePaths.homepage,
        element: HomePage,
      }),

      createRoute({
        path: publicRoutePaths.country,
        element: University,
      }),

      createRoute({
        path: publicRoutePaths.universityDetails,
        element: UniversityDetails,
      }),

      createRoute({
        path: publicRoutePaths.course,
        element: Course,
      }),

      createRoute({
        path: publicRoutePaths.category,
        element: Categories,
      }),

      createRoute({
        path: publicRoutePaths.blogs,
        element: Blogs,
      }),

      createRoute({
        path: publicRoutePaths.blogsUsa,
        element: Blogs,
      }),

      createRoute({
        path: publicRoutePaths.blogsAustralia,
        element: Blogs,
      }),

      createRoute({
        path: publicRoutePaths.blogsUk,
        element: Blogs,
      }),

      createRoute({
        path: publicRoutePaths.blogsPoland,
        element: Blogs,
      }),

      createRoute({
        path: publicRoutePaths.blogsFinland,
        element: Blogs,
      }),

      createRoute({
        path: publicRoutePaths.blogsRussia,
        element: Blogs,
      }),

      createRoute({
        path: publicRoutePaths.blogsUkraine,
        element: Blogs,
      }),

      createRoute({
        path: publicRoutePaths.consultancy,
        element: Consultancy,
      }),

      createRoute({
        path: publicRoutePaths.blogsContent,
        element: BlogsPage,
      }),

      createRoute({
        path: publicRoutePaths.contact,
        element: Contact,
      }),

      createRoute({
        path: publicRoutePaths.find,
        element: StepFormWrapper,
        children: [
          createRoute({
            path: publicRoutePaths.find,
            element: Find,
          }),
        ],
      }),
    ],
  }),
];
