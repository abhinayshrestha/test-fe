import { API_ENDPOINT } from "@/config/api.config";

export const excelFileExtensions = ["xlsx", "xls", "xml", "xlsm", "xlsb"];

export const imageExtensions = ".jpg, .jpeg, .png, .svg";

export const videoExtensions = ".mp4";

export const attachmentExtensions = `${imageExtensions.split(",").map((extension) => `${extension}`)}, .pdf, .doc, .docx, ${excelFileExtensions.map(
    (extension) => `.${extension}, `
)}`;

export const getImageUrlByPath = async (path: string) => {
    try {
        const res = await fetch(`${API_ENDPOINT}/image/?path=${path}`);
        const blob = await res.blob();
        const objectUrl = URL.createObjectURL(blob);

        return objectUrl;
    } catch (error) {
        console.log(error);
    }
};

export const getImageUrl = (imagePath: string) => {
    return `${API_ENDPOINT}/image/?path=${imagePath}`;
};
