import { MenuProps } from "antd";
import { WindowsOutlined } from "@ant-design/icons";
import {
    IoSettingsOutline,
    IoVideocamOutline,
    IoArrowRedoCircleOutline,
} from "react-icons/io5";
import { SiGoogleclassroom } from "react-icons/si";
import { PiStudent } from "react-icons/pi";
import { BsBox } from "react-icons/bs";

import { routePaths } from "@/router";

type MenuItem = Required<MenuProps>["items"][number];

function getSidebarItem(
    label: React.ReactNode,
    key: ValueOf<typeof routePaths>,
    icon?: any,
    children?: MenuItem[] | null,
    type?: "group"
): MenuItem | null {
    return {
        label,
        key,
        icon,
        children,
        type,
    } as MenuItem;
}

export const getSidebarItems = (): MenuProps["items"] => {
    return [
        getSidebarItem("Dashboard", routePaths.base, <WindowsOutlined />),
        getSidebarItem(
            "Service Providers",
            routePaths.serviceProviders,
            <BsBox size={15} />
        ),
        getSidebarItem("SEO", "seo", <IoSettingsOutline />),
        getSidebarItem(
            "Video Library",
            routePaths.videos,
            <IoVideocamOutline size={17} />
        ),
        getSidebarItem(
            "Students Report",
            routePaths.studentsReport,
            <PiStudent size={17} />
        ),
        getSidebarItem(
            "Path Revalidation",
            routePaths.pathRevalidation,
            <IoArrowRedoCircleOutline size={17} />
        ),
        getSidebarItem(
            "Mock Test Board",
            routePaths.mockTestBoard,
            <SiGoogleclassroom size={17} />
        ),
    ];
};
