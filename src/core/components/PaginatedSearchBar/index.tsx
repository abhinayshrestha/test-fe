import { IInitalFilterValue } from "../GenericTable/hooks/useServerSidePagination";

import SearchBar from "../SearchBar";

interface IPaginatedSearchBarProps {
    initialFilterList: IInitalFilterValue;
    handleServerSideTableChange: (pagination: IInitalFilterValue) => void;
    filterKey: string;
}

const PaginatedSearchBar = ({
    initialFilterList,
    handleServerSideTableChange,
    filterKey,
}: IPaginatedSearchBarProps) => {
    const handleInputSubmit = (query: string) => {
        handleServerSideTableChange({
            ...initialFilterList,
            [filterKey]: query,
        });
    };

    return <SearchBar onSubmit={handleInputSubmit} />;
};

export default PaginatedSearchBar;
