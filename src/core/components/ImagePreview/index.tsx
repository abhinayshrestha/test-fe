/*
    Use this component for images being displayed using /image
    This will provide a placeholder when the image is being loaded
*/

import { useState } from "react";
import { Image } from "antd";
import { twMerge } from "tailwind-merge";

import { getImageUrl } from "@/utils/file.utils";
import placeholderImage from "@/assets/images/placeholder-image.png";

interface IImagePreviewProps {
    imagePath: string;
    alt?: string;
    width?: number;
    height?: number;
    className?: string;
    placeholderClassName?: string;
}

const ImagePreview = ({
    imagePath,
    alt,
    width,
    height,
    className,
    placeholderClassName,
}: IImagePreviewProps) => {
    const [isLoading, setIsLoading] = useState(true);

    return (
        <div>
            <Image
                src={getImageUrl(imagePath)}
                alt={alt || "image"}
                className={twMerge(
                    "object-cover",
                    isLoading ? "hidden" : "block",
                    className
                )}
                style={{
                    width: width || "100px",
                    height: height || "auto",
                }}
                onLoad={() => setIsLoading(false)}
            />

            <Image
                src={placeholderImage}
                alt="default image"
                className={twMerge(
                    "animate-pulse rounded",
                    isLoading ? "block" : "hidden",
                    placeholderClassName
                )}
                style={{
                    width: width || "100px",
                    height: height || "auto",
                }}
                preview={false}
            />
        </div>
    );
};

export default ImagePreview;
