import { Modal } from "antd";

interface IConfirmationModalProps {
    title: string;
    description: string;
    isLoading?: boolean;
    isOpen: boolean;
    setIsOpen: (isOpen: boolean) => void;
    onConfirm: () => void;
    onCancel?: () => void;
}

const ConfirmationModal = ({
    title,
    description,
    isLoading,
    isOpen,
    setIsOpen,
    onConfirm,
    onCancel,
}: IConfirmationModalProps) => {
    const handleCancellation = () => {
        setIsOpen(false);
        onCancel?.();
    };

    return (
        <Modal
            title={title}
            open={isOpen}
            okText="Confirm"
            onOk={onConfirm}
            confirmLoading={isLoading}
            onCancel={handleCancellation}
        >
            <p className="text-gray-700">{description}</p>
        </Modal>
    );
};

export default ConfirmationModal;
