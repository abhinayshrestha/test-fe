import React, { useEffect, useMemo, useState } from "react";

import { ContentState, EditorState, convertToRaw } from 'draft-js';
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";

import { wordCount } from "@/core/components/PrivateLayout/utils/string.utils";
import { Space, Typography, theme } from "antd";
import htmlToDraft from "html-to-draftjs";
import { Editor } from "react-draft-wysiwyg";
import styled, { css } from "styled-components";
import draftToHtml from "draftjs-to-html";

const { useToken } = theme;

interface ICustomEditorType {
  setDescription: React.Dispatch<React.SetStateAction<string | null>>;
  fromDrawer?: boolean | undefined;
  initialValue?: string | null;
  placeholder?: string | undefined;
  fromResponse?: boolean;
  editorDisabled?: boolean;
  minWord?: number;
  maxWord?: number;
}

export interface IEditorWrapperType {
  fromResponse?: boolean;
  isFocused: boolean;
  color?: string;
}

const EditorWrapper = styled.div<IEditorWrapperType>`
  .wrapper-class {
    ${(props) =>
      props?.isFocused
        ? css`
            border: 1px solid ${props.color};
          `
        : css`
            border: 1px solid #ccc;
          `}
  }
  .wrapper-class:hover {
    ${(props) => css`
      border: 1px solid ${props.color};
    `}
  }
  .toolbar-class {
    background: #f6f6f6;
    padding: 0.1rem;
  }
  .editor-class {
    padding: 0rem 0.4rem;
    ${(props) =>
      props?.fromResponse
        ? css`
            min-height: 130px;
          `
        : css`
            min-height: 280px;
          `}
  }
  .rdw-inline-wrapper {
  }
  .public-DraftStyleDefault-block {
    margin: 3px;
  }
`;
const CustomEditor = ({
  setDescription,
  fromDrawer,
  initialValue,
  placeholder,
  fromResponse,
  editorDisabled,
  minWord,
  maxWord,
}: ICustomEditorType) => {
  const { token } = useToken();
  const [editorState, setEditorState] = useState(() =>
    EditorState.createEmpty()
  );
  const [isFocused, setIsFocused] = useState<boolean>(false);

  const normalText = useMemo(() => {
    const blocksData = convertToRaw(editorState.getCurrentContent()).blocks;
    let text = "";
    blocksData.forEach((bData: any) => {
      text = text.concat(bData.text);
    });
    return text || "";
  }, [editorState]);

  useEffect(() => {
    if (initialValue) {
      const blocksFromHtml = htmlToDraft(initialValue);
      const { contentBlocks, entityMap } = blocksFromHtml;
      const contentState = ContentState.createFromBlockArray(
        contentBlocks,
        entityMap
      );
      const initialEditorState = EditorState.createWithContent(contentState);
      setEditorState(initialEditorState);
      setDescription(
        draftToHtml(convertToRaw(initialEditorState.getCurrentContent()))
      );
    }
  }, [initialValue, setDescription]);

  const onEditorStateChange = (updatedEditorState: any) => {
    if (!editorDisabled) {
      setEditorState(updatedEditorState);
      setDescription(
        draftToHtml(convertToRaw(updatedEditorState.getCurrentContent()))
      );
    }
  };

  const toolbarConfig = {
    // options: fromDrawer ? ['inline', 'fontSize', 'list', 'textAlign'] : undefined,
    inline: {
      visible: true,
      inDropdown: fromDrawer,
      bold: { visible: true },
      italic: { visible: true },
      underline: { visible: true },
      strikeThrough: { visible: true },
      monospace: { visible: true },
    },
    list: {
      visible: true,
      inDropdown: fromDrawer,
      unordered: { visible: true },
      ordered: { visible: true },
      indent: { visible: true },
      outdent: { visible: true },
    },
    textAlign: {
      visible: true,
      inDropdown: fromDrawer,
      left: { visible: true },
      center: { visible: true },
      right: { visible: true },
      justify: { visible: true },
    },
    blockType: { visible: true },
    fontSize: { visible: true },
    fontFamily: { visible: true },
    colorPicker: { visible: true },
    link: {
      visible: true,
      inDropdown: fromDrawer,
      addLink: { visible: true },
      removeLink: { visible: true },
    },
    image: {
      visible: true,
      fileUpload: true,
      url: true,
    },
    history: {
      visible: true,
      inDropdown: fromDrawer,
      undo: { visible: true },
      redo: { visible: true },
    },
  };

  return (
    <EditorWrapper
      fromResponse={fromResponse}
      isFocused={isFocused}
      color={token.colorPrimaryHover}
    >
      <Editor
        onFocus={() => setIsFocused(true)}
        onBlur={() => setIsFocused(false)}
        placeholder={placeholder || "Description here..."}
        wrapperClassName="wrapper-class"
        editorClassName="editor-class"
        toolbarClassName="toolbar-class"
        editorState={editorState}
        onEditorStateChange={onEditorStateChange}
        toolbar={toolbarConfig}
        toolbarCustomButtons={
          minWord && maxWord
            ? [
                <Space key={1} size={1}>
                  <Typography.Text
                    style={{
                      color:
                        wordCount(normalText) < minWord
                          ? "orange"
                          : wordCount(normalText) > maxWord
                            ? "red"
                            : "green",
                    }}
                  >
                    {wordCount(normalText) < minWord ||
                    wordCount(normalText) > maxWord
                      ? `Answer should have ${minWord}-${maxWord} words`
                      : ""}
                  </Typography.Text>
                </Space>,
                <Space
                  key={2}
                  size={1}
                  style={{
                    position: "absolute",
                    right: 8,
                    bottom: 8,
                  }}
                >
                  <Typography.Text
                    style={{
                      color:
                        wordCount(normalText) < minWord
                          ? "orange"
                          : wordCount(normalText) > maxWord
                            ? "red"
                            : "green",
                    }}
                  >
                    {`
              ${wordCount(normalText)}
              /${maxWord}`}
                  </Typography.Text>
                </Space>,
              ]
            : []
        }
      />
    </EditorWrapper>
  );
};

export default CustomEditor;
