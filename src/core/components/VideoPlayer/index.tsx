import { useState, useRef, useEffect } from "react";
import Hls from "hls.js";

import VideoCore from "./components/VideoCore";

interface IVideoPlayerProps {
    src: string;
}

const VideoPlayer = ({ src }: IVideoPlayerProps) => {
    const [volumeLevel, setVolumeLevel] = useState(1);

    const videoRef = useRef<HTMLVideoElement>(null);

    useEffect(() => {
        if (Hls.isSupported() && videoRef.current) {
            const hls = new Hls();
            hls.loadSource(src);
            hls.attachMedia(videoRef.current);
        }

        if (videoRef.current) {
            videoRef.current.focus();
            // setVolumeLevel(videoRef.current.volume);
        }
    });

    const toggleFullscreenMode = () => {
        if (document.fullscreenElement) {
            return document.exitFullscreen();
        }

        videoRef.current?.requestFullscreen();
    };

    const toggleMuteMode = () => {
        if (videoRef.current) {
            videoRef.current.muted = !videoRef.current.muted;
        }
    };

    const controlVolume = () => {
        if (videoRef.current) {
            setVolumeLevel(videoRef.current.volume);
        }
    };

    const skipVideo = (direction: "FORWARD" | "BACKWARD") => {
        if (!videoRef.current) {
            return;
        }

        if (direction === "BACKWARD") {
            videoRef.current.currentTime -= 5;
        } else {
            videoRef.current.currentTime += 5;
        }
    };

    const togglePictureInPictureMode = () => {
        if (!videoRef.current) {
            return;
        }

        if (document.pictureInPictureElement) {
            return document.exitPictureInPicture();
        }

        videoRef.current.requestPictureInPicture();
    };

    const handleKeyDown = (event: React.KeyboardEvent<HTMLDivElement>) => {
        switch (event.code.toLowerCase()) {
            case "keyf":
                toggleFullscreenMode();
                break;
            case "keym":
                toggleMuteMode();
                break;
            case "keyi":
                togglePictureInPictureMode();
                break;
            case "arrowup":
            case "arrowdown":
                controlVolume();
                break;
            case "arrowleft":
                event.preventDefault();
                skipVideo("BACKWARD");
                break;
            case "arrowRight":
                event.preventDefault();
                skipVideo("FORWARD");
                break;
            default:
                break;
        }
    };

    return (
        <div className="relative h-[500px]" onKeyDown={handleKeyDown}>
            <VideoCore videoRef={videoRef} />
        </div>
    );
};

export default VideoPlayer;
