import styled, { keyframes } from "styled-components";

const fadeInOut = keyframes`
    0% {
        opacity: 1;
    }
    100% {
        opacity: 0;
    }
`;

export const StyledVolumeLevel = styled.div`
    animation: ${fadeInOut} 0.5s linear;
`;
