import { memo } from "react";

interface IVideoCoreProps {
    videoRef: React.RefObject<HTMLVideoElement>;
}

const VideoCore = ({ videoRef }: IVideoCoreProps) => {
    return (
        /* eslint-disable-next-line jsx-a11y/media-has-caption */
        <video
            ref={videoRef}
            controls
            width="100%"
            className="max-h-full focus:outline-none"
            autoPlay
        />
    );
};

export default memo(VideoCore, (prevProps, nextProps) => {
    // Compare if videoRef and onKeyDown are the same between renders
    return prevProps.videoRef === nextProps.videoRef;
});
