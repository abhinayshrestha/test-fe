import { StyledVolumeLevel } from "../styles";

interface IVolumeLevelProps {
    volume: number;
}

const VolumeLevel = ({ volume }: IVolumeLevelProps) => {
    return (
        <StyledVolumeLevel
            className="absolute top-[5%] left-1/2 -translate-x-1/2 px-3 py-1 rounded text-white"
            style={{
                backgroundColor: "rgba(0, 0, 0, 0.5)",
            }}
        >
            <span>{volume * 100} %</span>
        </StyledVolumeLevel>
    );
};

export default VolumeLevel;
