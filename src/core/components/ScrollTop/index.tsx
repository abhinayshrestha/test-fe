import { useState } from "react";
import { BackTop } from "antd";

const ScrollTop = () => {
  const [showScroll, setShowScroll] = useState(false);

  const checkScrollTop = () => {
    if (!showScroll && window.scrollY > 400) {
      setShowScroll(true);
    } else if (showScroll && window.scrollY <= 400) {
      setShowScroll(false);
    }
  };

  const scrollTop = () => {
    window.scrollTo({ top: 0, behavior: "smooth" });
  };

  window.addEventListener("scroll", checkScrollTop);

  return (
    showScroll && (
      <div
        className=" fixed bottom-10 right-7 cursor-pointer z-20 "
        onClick={scrollTop}
      >
        <BackTop />
      </div>
    )
  );
};

export default ScrollTop;
