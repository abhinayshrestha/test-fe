import { Button, Drawer, Menu } from "antd";
import { useState } from "react";
import { Link } from "react-router-dom";
import { CloseOutlined, MenuOutlined } from "@ant-design/icons";
import Logo from "../../Logo";

const { SubMenu } = Menu;

const NavBar = () => {
  const [isMenuOpen, setIsMenuOpen] = useState(false);

  const links = [
    { label: "Home", link: "/" },
    { label: "Institute", link: "/" },
    { label: "Blog", link: "/blogs" },
    { label: "About Us", link: "/" },
    { label: "Contact", link: "/contact" },
  ];

  const showDrawer = () => {
    setIsMenuOpen(true);
  };

  const onClose = () => {
    setIsMenuOpen(false);
  };

  return (
    <div className="bg-white sticky top-0 z-50 shadow ">
      <div className="container mx-auto">
        <div className="flex items-center justify-between h-16 text-black mx-4">
          {/* Logo to be seen on all screen */}
          <div className="flex-shrink-0 border">
            <Link to="/" className="flex items-center">
              <Logo width={80} />
            </Link>
          </div>

          {/*  Menu Button for small screen size  */}
          <div className="lg:hidden md:hidden flex space-x-3">
            <div className="space-x-2">
              <Button type="default">Login</Button>
              <Button type="primary">Sign Up</Button>
            </div>

            <Button
              type="primary"
              icon={<MenuOutlined />}
              onClick={showDrawer}
            />

            <Drawer
              title={
                <div className="drawer-header space-x-2 flex items-center">
                  <Button
                    type="text"
                    onClick={onClose}
                    className="bg-red-600 text-white rounded-full flex items-center justify-center p-2"
                  >
                    <CloseOutlined />
                  </Button>

                  <span>Menu</span>
                </div>
              }
              placement="right"
              onClose={onClose}
              open={isMenuOpen}
              closable={false}
              width="80%"
            >
              <div className="text-black ">
                {links.map((link) => (
                  <Link
                    key={link.label}
                    to={link.link}
                    onClick={onClose}
                    style={{ color: "inherit", textDecoration: "none" }}
                  >
                    <div className="p-2 font-medium border-b border-black-200">
                      {link.label}
                    </div>
                  </Link>
                ))}
              </div>
            </Drawer>
          </div>

          {/* Navigation Menu - Shown on large and medium screens */}
          <div className="hidden lg:flex lg:space-x-10 md:flex md:space-x-0">
            {links.map((link) => (
              <div
                className="lg:p-4 md:px-3 md:py-4 font-medium underline-from-left"
                key={link.label}
              >
                <Link
                  to={link.link}
                  style={{ color: "inherit", textDecoration: "none" }}
                >
                  {link.label}
                </Link>
              </div>
            ))}
          </div>

          {/* Buttons - Shown on large and medium screens */}
          <div className="hidden lg:block space-x-3 md:block">
            <Button type="default">Login</Button>
            <Button type="primary">Sign Up</Button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default NavBar;
