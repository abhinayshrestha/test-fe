import React from "react";
import { Divider, Layout, Menu } from "antd";
import {
  FacebookOutlined,
  InstagramOutlined,
  LinkedinOutlined,
  XOutlined,
} from "@ant-design/icons";
import { Link } from "react-router-dom";

const { Footer } = Layout;

const PageFooter = () => {
  return (
    <Footer className="bg-primary text-white">
      <div className="text-white  container mx-auto ">
        <div className=" grid  my-12  500:gap-[50px]  sm:gap-[50px]  md:gap-[70px]  gap-[100px] md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 ">
          {/* Left Side */}

          <div className="  space-y-4">
            <h2 className="text-xl font-bold ">Hobes Tech</h2>
            <p className="text-sm ">
              Guiding students worldwide toward academic success with
              personalized support, expert advice, and comprehensive services,
              unlocking opportunities for global education exploration.
            </p>

            <div className="flex flex-wrap text-3xl gap-x-4 gap-y-2">
              <Link
                to="https://www.facebook.com.np "
                target="_blank"
                className="text-white hover:text-blue-600"
              >
                <FacebookOutlined />
              </Link>
              <Link
                to="https://www.twitter.com"
                target="_blank"
                className="text-white hover:text-black"
              >
                <XOutlined />
              </Link>
              <Link
                to="https://www.linkedin.com"
                target="_blank"
                className="text-white hover:text-blue-600"
              >
                <LinkedinOutlined />
              </Link>
              <Link
                to="https://www.instagram.com"
                target="_blank"
                className="text-white hover:text-pink-600"
              >
                <InstagramOutlined />
              </Link>
            </div>
          </div>

          {/* study prep */}

          <div className="space-y-4 ">
            <h2 className="text-xl font-bold ">Study Destination</h2>
            <div className="flex flex-wrap text-sm ">
              <ul className="space-y-2 ">
                <li>
                  <Link
                    to="/study-destination/link1"
                    className="text-white hover:underline hover:text-white  hover:underlinehover:font-semibold font-normal"
                  >
                    United States
                  </Link>
                </li>
                <li>
                  <Link
                    to="/study-destination/link2"
                    className=" text-white hover:underline hover:text-white hover:font-semibold font-normal"
                  >
                    Canada
                  </Link>
                </li>
                <li>
                  <Link
                    to="/study-destination/link3"
                    className="text-white hover:underline hover:text-white hover:font-semibold font-normal"
                  >
                    Australia
                  </Link>
                </li>
                <li>
                  <Link
                    to="/study-destination/link4"
                    className="text-white hover:underline hover:text-white hover:font-semibold font-normal"
                  >
                    Portugal
                  </Link>
                </li>
                <li>
                  <Link
                    to="/study-destination/link5"
                    className="text-white hover:underline hover:text-white hover:font-semibold font-normal"
                  >
                    Bangladesh
                  </Link>
                </li>
              </ul>
            </div>
          </div>
          {/* test prep */}

          <div className="space-y-4 ">
            <h2 className="text-xl font-bold">Test Preperation</h2>
            <div className="flex flex-wrap text-sm  ">
              <ul className="space-y-2">
                <li>
                  <Link
                    to="/test-preperation/link1"
                    className="text-white hover:underline hover:text-white hover:font-semibold font-normal"
                  >
                    IELTS
                  </Link>
                </li>
                <li>
                  <Link
                    to="/test-preperation/link2"
                    className="text-white hover:underline hover:text-white hover:font-semibold font-normal"
                  >
                    TOEFL
                  </Link>
                </li>
                <li>
                  <Link
                    to="/test-preperation/link3"
                    className="text-white hover:underline hover:text-white hover:font-semibold font-normal"
                  >
                    PTE
                  </Link>
                </li>
                <li>
                  <Link
                    to="/test-preperation/link4"
                    className="text-white hover:underline hover:text-white hover:font-semibold font-normal"
                  >
                    GRE
                  </Link>
                </li>
                <li>
                  <Link
                    to="/test-preperation/link5"
                    className="text-white hover:underline hover:text-white hover:font-semibold font-normal"
                  >
                    GMAT
                  </Link>
                </li>
              </ul>
            </div>
          </div>

          {/* Contact */}
          <div className=" space-y-4">
            <h2 className="text-xl font-bold">Contact Info</h2>
            <div className="space-y-2 ">
              <p className="text-sm">Balaju, Kathmandu</p>
              <p className="text-sm">+977-01-4000000</p>
              <p className="text-sm">info@hobes.tech</p>
            </div>
          </div>
        </div>

        <div className="text-center w-full border-t-[1px] p-4 text-sm">
          Copyright © 2024 All Rights Reserved to Hobes Tech.
        </div>
      </div>
    </Footer>
  );
};

export default PageFooter;
