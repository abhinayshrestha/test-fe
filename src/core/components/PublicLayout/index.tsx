import { useEffect } from "react";
import { Layout } from "antd";
import { Outlet, useLocation } from "react-router-dom";

import NavBar from "./components/Navbar";
import PageFooter from "./components/PageFooter";
import ScrollTop from "../ScrollTop";

const PublicLayout = () => {

  const { pathname } = useLocation();

  useEffect(() => {
    window.scrollTo({ top: 0, left: 0, behavior: "auto" });
  }, [pathname]);
  
  return (
    <Layout>
      <NavBar />
      <Outlet />
      <PageFooter />
      <ScrollTop/>
    </Layout>
  );
};

export default PublicLayout;
