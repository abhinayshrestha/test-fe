import { Button } from "antd";
import { BellOutlined } from "@ant-design/icons";
import { Header } from "antd/es/layout/layout";

import { useTitle } from "@/providers/TitleProvider";
import PrimarySearch from "@/core/PrimarySearch";
import { useSidebar } from "@/providers/SidebarProvider";

import HeaderBackButton from "./HeaderBackButton";
import ProfilePreview from "../../ProfilePreview";

const CustomHeader = () => {
    const { title } = useTitle();
    const { isCollapsed } = useSidebar();

    return (
        <Header className="flex items-center justify-between bg-gray-200 border-b border-gray-300 h-[48px] px-6">
            <div className="flex items-center space-x-3">
                <HeaderBackButton />

                {title && (
                    <h5 className="font-semibold capitalize text-base">
                        {title}
                    </h5>
                )}
            </div>

            {isCollapsed && (
                <div className="w-[300px]">
                    <PrimarySearch />
                </div>
            )}

            <div className="flex items-center space-x-5">
                <Button icon={<BellOutlined />} type="text" />

                <ProfilePreview />
            </div>
        </Header>
    );
};

export default CustomHeader;
