import { protectedRoutePaths } from "@/router";

export const routePathsWithBackButton = [
    protectedRoutePaths.createServiceProvider,
    protectedRoutePaths.updateServiceProvider,
    protectedRoutePaths.videoPlay,
    protectedRoutePaths.conversionLog,
];
