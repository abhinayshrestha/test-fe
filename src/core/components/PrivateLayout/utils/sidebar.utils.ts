import { protectedRoutePaths } from "@/router";

export const activeRoutesInfo: { base: string; activeRoutePaths: string[] }[] =
    [
        {
            base: protectedRoutePaths.base,
            activeRoutePaths: [protectedRoutePaths.base],
        },
        {
            base: protectedRoutePaths.serviceProviders,
            activeRoutePaths: [
                protectedRoutePaths.serviceProviders,
                protectedRoutePaths.createServiceProvider,
                protectedRoutePaths.updateServiceProvider,
            ],
        },
        {
            base: protectedRoutePaths.videos,
            activeRoutePaths: [
                protectedRoutePaths.videos,
                protectedRoutePaths.videoPlay,
                protectedRoutePaths.conversionLog,
            ],
        },
        {
            base: protectedRoutePaths.studentsReport,
            activeRoutePaths: [protectedRoutePaths.studentsReport],
        },
        {
            base: protectedRoutePaths.pathRevalidation,
            activeRoutePaths: [protectedRoutePaths.pathRevalidation],
        },
    ];
