import { Image } from "antd";

import logo from "@/assets/vectors/logo.svg";

interface ILogoProps {
  width?: number;
}

const Logo = ({ width }: ILogoProps) => {
  return (
    <Image
      //   className=" outline-none border-none border-0 bg-transparent "
      src={logo}
      alt="logo"
      width={width || 60}
      preview={false}
    />
  );
};

export default Logo;
