import { useMutation, useQuery } from "react-query";

import { publicApiList } from "@/api";
import performApiAction from "@/utils/http/perform-api-action";
import { IRevalidationPath } from "@/schema/revalidation.schema";

const { getRevalidationPaths, revalidatePath } = publicApiList.revalidation;

export const useGetRevalidationPaths = () => {
  return useQuery({
    queryKey: [getRevalidationPaths.queryKeyName],
    queryFn: () => {
      return performApiAction<IRevalidationPath[]>(getRevalidationPaths);
    },
  });
};

export const useRevalidatePath = () => {
  return useMutation({
    mutationFn: (path: string) => {
      return performApiAction(revalidatePath, {
        requestData: {
          path,
        },
        disableSuccessToast: true,
        disableFailureToast: true,
      });
    },
  });
};
