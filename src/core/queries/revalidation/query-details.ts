import { REVALIDATION_API_ENDPOINT } from "@/config/revalidation-api.config";
import {
    IApiDetails,
    RequestAuthType,
    RequestMethodEnum,
} from "@/schema/http.schema";

const revalidation: { [key: string]: IApiDetails } = {
    getRevalidationPaths: {
        controllerName: "",
        requestMethod: RequestMethodEnum.GET,
        queryKeyName: "GET_REVALIDATION_PATHS",
        requestAuthType: RequestAuthType.NOAUTH,
        baseApiEndpoint: REVALIDATION_API_ENDPOINT,
    },
    revalidatePath: {
        controllerName: "",
        requestMethod: RequestMethodEnum.POST,
        queryKeyName: "REVALIDATE_PATH",
        requestAuthType: RequestAuthType.NOAUTH,
        baseApiEndpoint: REVALIDATION_API_ENDPOINT,
    },
};

export default revalidation;
