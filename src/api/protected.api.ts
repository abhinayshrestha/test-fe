import serviceProviders from "@/pages/protected/ServiceProviders/api/query-details";
import studentsReport from "@/pages/protected/StudentsReport/api/query-details";
import videos from "@/pages/protected/Videos/api/query-details";

export const protectedApiList = {
    serviceProviders,
    videos,
    studentsReport,
};
