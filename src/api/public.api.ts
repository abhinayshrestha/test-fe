import revalidation from "@/core/queries/revalidation/query-details";
import auth from "@/pages/public/SignIn/api/query-details";

export const publicApiList = {
    auth,
    revalidation,
};
