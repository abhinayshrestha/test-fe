const apiEndpointMap = {
    "admin.bhasika.com": {
        development: "https://api.bhasika.com/testnp",
        production: "https://api.bhasika.com/testnp",
    },
    "bhasika-admin.vercel.app": {
        development: "https://testnpapi.hobes.tech/testnp",
        production: "https://testnpapi.hobes.tech/testnp",
    },
};

const defaultApiEndpointObj = apiEndpointMap["bhasika-admin.vercel.app"];

export const getApiEndpoint = () => {
    const domain = window.location.hostname;
    const environment = process.env.NODE_ENV || "development";

    const apiEndpointObj =
        apiEndpointMap[domain as keyof typeof apiEndpointMap] ||
        defaultApiEndpointObj;

    return apiEndpointObj[environment as keyof typeof apiEndpointObj];
};

export const API_ENDPOINT = getApiEndpoint();
