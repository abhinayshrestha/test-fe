const apiEndpointMap = {
    "bhasika-admin.vercel.app": "https://bhasika.hobes.tech/api/revalidate",
    "admin.bhasika.com": "https://bhasika.com/api/revalidate",
};

const defaultApiEndpoint = apiEndpointMap["bhasika-admin.vercel.app"];

export const getRevalidationApiEndpoint = () => {
    const domain = window.location.hostname;
    const apiEndpoint =
        apiEndpointMap[domain as keyof typeof apiEndpointMap] ||
        defaultApiEndpoint;

    return apiEndpoint;
};

export const REVALIDATION_API_ENDPOINT = getRevalidationApiEndpoint();
