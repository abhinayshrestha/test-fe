export interface ISignInRequest {
    email: string;
    password: string;
}

export interface ISignInResponse {
    access_token: string;
    refresh_token: string;
    userEmail: string;
}

export interface IAuthUser {
    email: string;
}
