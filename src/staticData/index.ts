interface CourseCategory {
  id: number;
  name: string;
  icon: string;
}

export const courseCategories: CourseCategory[] = [
    {
        "id": 3,
        "name": "Architecture & Building & Design",
        "icon": "mdi mdi-city"
    },
    {
        "id": 14,
        "name": "Sports & Personal Services",
        "icon": "mdi mdi-run"
    },
    {
        "id": 5,
        "name": "Business & Management",
        "icon": "mdi mdi-calculator"
    },
    {
        "id": 9,
        "name": "Law & Criminology",
        "icon": "mdi mdi-gavel"
    },
    {
        "id": 13,
        "name": "Music & Performing Arts",
        "icon": "mdi mdi-human"
    },
    {
        "id": 11,
        "name": "Media & Communications",
        "icon": "mdi mdi-television-guide"
    },
    {
        "id": 1,
        "name": "Computer Science & IT",
        "icon": "mdi mdi-laptop-chromebook"
    },
    {
        "id": 4,
        "name": "Arts, Humanities & Social Sciences",
        "icon": "mdi mdi-account-switch"
    },
    {
        "id": 2,
        "name": "Agriculture & Environmental Studies",
        "icon": "mdi mdi-food-apple"
    },
    {
        "id": 15,
        "name": "Tourism & Hospitality",
        "icon": "mdi mdi-silverware-variant"
    },
    {
        "id": 6,
        "name": "Education & Social Work",
        "icon": "mdi mdi-book-open"
    },
    {
        "id": 10,
        "name": "Maths & Sciences",
        "icon": "mdi mdi-division"
    },
    {
        "id": 7,
        "name": "Engineering & Technology",
        "icon": "mdi mdi-math-compass"
    },
    {
        "id": 8,
        "name": "Languages & Culture",
        "icon": "mdi mdi-alphabetical"
    },
    {
        "id": 12,
        "name": "Medicine & Health",
        "icon": "mdi mdi-hospital-building"
    }   
];





