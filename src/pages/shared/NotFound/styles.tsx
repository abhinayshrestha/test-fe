import { Result } from "antd";
import styled from "styled-components";

export const StyledResult = styled(Result)`
    padding: 0px !important;
`;
