import { useQuery } from "react-query";

import { protectedApiList } from "@/api";
import performApiAction from "@/utils/http/perform-api-action";
import { IStudentReportItem } from "@/schema/students-report.schema";
import {
  IGenericPaginatedRequest,
  IGenericPaginatedResponse,
} from "@/schema/http.schema";

const { getStudentsReport } = protectedApiList.studentsReport;

export const useGetStudentsReport = (requestData: IGenericPaginatedRequest) => {
  return useQuery({
    queryKey: [getStudentsReport.queryKeyName, requestData],
    queryFn: () => {
      return performApiAction<IGenericPaginatedResponse<IStudentReportItem[]>>(
        getStudentsReport,
        {
          requestData,
          disableSuccessToast: true,
        }
      );
    },
  });
};
