import {
    IApiDetails,
    RequestAuthType,
    RequestMethodEnum,
} from "@/schema/http.schema";

const prefix = "report";

const studentsReport: { [key: string]: IApiDetails } = {
    getStudentsReport: {
        controllerName: `${prefix}/applied-student`,
        requestMethod: RequestMethodEnum.POST,
        queryKeyName: "GET_STUDENT_REPORTS",
        requestAuthType: RequestAuthType.AUTH,
    },
};

export default studentsReport;
