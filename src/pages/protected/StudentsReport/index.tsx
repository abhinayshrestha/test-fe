import { useEffect } from "react";
import { Spin } from "antd";
import { ColumnsType } from "antd/es/table";

import { useTitle } from "@/providers/TitleProvider";
import { IStudentReportItem } from "@/schema/students-report.schema";
import GenericTable from "@/core/components/GenericTable";
import useServerSidePagination from "@/core/components/GenericTable/hooks/useServerSidePagination";
import { PaginatedDataFilterFieldTypeEnum } from "@/schema/shared.schema";

import { useGetStudentsReport } from "./api/queries";

const StudentReports = () => {
    const { setTitle } = useTitle();

    const { initialFilterList, handleServerSideTableChange } =
        useServerSidePagination();

    const { data: studentsReportData, isLoading: isLoadingStudentsReportData } =
        useGetStudentsReport(initialFilterList);

    const reportColumns: ColumnsType<IStudentReportItem> = [
        {
            title: "Student's Name",
            key: "studentName",
            dataIndex: "studentName",
            render: (studentName) => {
                return <span className="capitalize">{studentName}</span>;
            },
        },
        {
            title: "Student's Contact",
            key: "mobileNo",
            dataIndex: "mobileNo",
        },
        {
            title: "Student's Email",
            key: "email",
            dataIndex: "email",
        },
        {
            title: "Service Provider",
            key: "consultancyName",
            dataIndex: "consultancyName",
            render: (consultancyName) => {
                return <span className="capitalize">{consultancyName}</span>;
            },
        },
        {
            title: "Date",
            key: "date",
            dataIndex: "date",
        },
    ];

    useEffect(() => {
        setTitle("Students Report");

        return () => {
            setTitle("");
        };
    }, [setTitle]);

    return (
        <Spin spinning={isLoadingStudentsReportData}>
            <div className="space-y-4">
                <p className="text-gray-700">
                    These are the students who booked schedules through Bhasika.
                </p>

                <GenericTable
                    columns={reportColumns}
                    dataSource={studentsReportData?.content}
                    searchFilterKey="name"
                    filterInfo={[
                        {
                            type: PaginatedDataFilterFieldTypeEnum.RANGE,
                            label: "Select Start and End Dates",
                            name: "dateRange",
                        },
                    ]}
                    initialFilterList={initialFilterList}
                    handleServerSideTableChange={handleServerSideTableChange}
                    serverSideDataParams={{
                        currentPageIndex: studentsReportData?.currentPageIndex,
                        numberOfElements: studentsReportData?.numberOfElements,
                        totalElements: studentsReportData?.totalElements,
                        totalPages: studentsReportData?.totalPages,
                    }}
                />
            </div>
        </Spin>
    );
};

export default StudentReports;
