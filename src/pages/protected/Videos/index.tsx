import { Outlet, useLocation } from "react-router-dom";
import { useSubscription } from "react-stomp-hooks";
import { useQueryClient } from "react-query";
import dayjs from "dayjs";
import { message } from "antd";

import useVideoStore from "@/stores/video.store";
import { protectedRoutePaths } from "@/router";

import videos from "./api/query-details";

const VideosRoot = () => {
  const queryClient = useQueryClient();
  const location = useLocation();

  const {
    isConversionInProgress,
    setIsConversionInProgress,
    conversionLogs,
    setConversionLogs,
  } = useVideoStore();

  useSubscription("/user/topic/video-conversion", (logData: any) => {
    if (logData) {
      if (!isConversionInProgress) {
        setIsConversionInProgress(true);
      }

      if (logData.body.toLowerCase().includes("no video found")) {
        setIsConversionInProgress(false);

        if (protectedRoutePaths.videos === location.pathname) {
          // if on videos home page
          return queryClient.invalidateQueries({
            queryKey: [videos.getVideosList.queryKeyName],
          });
        }

        if (protectedRoutePaths.conversionLog === location.pathname) {
          // if on conversion logs page
          return message.success("The video has been converted successfully");
        }
      }

      return setConversionLogs([
        ...conversionLogs,
        {
          timestamp: dayjs().format("HH:mm:ss:SSS"),
          message: logData.body,
        },
      ]);
    }
  });

  return <Outlet />;
};

export default VideosRoot;
