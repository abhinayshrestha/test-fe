import {
    IApiDetails,
    RequestAuthType,
    RequestBodyType,
    RequestMethodEnum,
} from "@/schema/http.schema";

const prefix = "video";

const videos = {
    uploadVideo: {
        controllerName: prefix,
        requestMethod: RequestMethodEnum.POST,
        queryKeyName: "UPLOAD_VIDEO",
        requestAuthType: RequestAuthType.AUTH,
        requestBodyType: RequestBodyType.FORMDATA,
    },
    getUploadedVideos: {
        controllerName: `${prefix}/uploaded-list`,
        requestMethod: RequestMethodEnum.GET,
        queryKeyName: "GET_UPLOADED_VIDEOS",
        requestAuthType: RequestAuthType.AUTH,
    },
    getConvertedVideos: {
        controllerName: `${prefix}/converted-list`,
        requestMethod: RequestMethodEnum.GET,
        queryKeyName: "GET_CONVERTED_VIDEOS",
        requestAuthType: RequestAuthType.AUTH,
    },
    getVideosList: {
        controllerName: `${prefix}/video-list`,
        requestMethod: RequestMethodEnum.POST,
        queryKeyName: "GET_VIDEOS_LIST",
        requestAuthType: RequestAuthType.AUTH,
    },
    convertVideo: {
        controllerName: `${prefix}/convert_video`,
        requestMethod: RequestMethodEnum.POST,
        queryKeyName: "CONVERT_VIDEO",
        requestAuthType: RequestAuthType.AUTH,
    },
};

export default videos;
