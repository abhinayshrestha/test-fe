import {
  UseMutateFunction,
  useMutation,
  useQuery,
  useQueryClient,
} from "react-query";

import { protectedApiList } from "@/api";
import {
  IUploadingVideo,
  IVideo,
  IVideoListResponse,
  IVideoUploadRequest,
} from "@/schema/video.schema";
import performApiAction from "@/utils/http/perform-api-action";
import {
  IGenericPaginatedRequest,
  IGenericPaginatedResponse,
} from "@/schema/http.schema";

const {
  uploadVideo,
  getUploadedVideos,
  getConvertedVideos,
  convertVideo,
  getVideosList,
} = protectedApiList.videos;

export const useUploadVideo = (
  videoConversionMutationFn: UseMutateFunction<unknown, Error, string, unknown>,
  removeUploadingVideo: (videoUid: string) => void,
  videoUid?: string
) => {
  return useMutation({
    mutationKey: [uploadVideo.queryKeyName, videoUid],
    mutationFn: (requestData: IVideoUploadRequest) => {
      return performApiAction<string>(uploadVideo, {
        requestData,
      });
    },
    onSuccess: (videoPath) => {
      videoConversionMutationFn(videoPath);
    },
    onSettled: () => {
      if (videoUid) {
        removeUploadingVideo(videoUid);
      }
    },
  });
};

export const useGetUploadedVideos = () => {
  return useQuery({
    queryKey: [getUploadedVideos.queryKeyName],
    queryFn: () => {
      return performApiAction<IVideoListResponse>(getUploadedVideos);
    },
  });
};

export const useGetConvertedVideos = () => {
  return useQuery({
    queryKey: [getConvertedVideos.queryKeyName],
    queryFn: () => {
      return performApiAction<IVideoListResponse>(getConvertedVideos);
    },
  });
};

export const useGetVideosList = (requestData: IGenericPaginatedRequest) => {
  return useQuery({
    queryKey: [getVideosList.queryKeyName, requestData],
    queryFn: () => {
      return performApiAction<IGenericPaginatedResponse<IVideo[]>>(
        getVideosList,
        {
          requestData: {
            ...requestData,
            videoConversionStatus: "ALL",
          },
          disableSuccessToast: true,
        }
      );
    },
  });
};

export const useConvertVideo = () => {
  const queryClient = useQueryClient();

  return useMutation({
    mutationFn: (videoPath: string) => {
      return performApiAction(convertVideo, {
        requestData: {
          videoPath,
        },
        disableSuccessToast: true,
      });
    },
    onSuccess: () => {
      queryClient.invalidateQueries({
        queryKey: [getConvertedVideos.queryKeyName],
      });
      queryClient.invalidateQueries({
        queryKey: [getUploadedVideos.queryKeyName],
      });
    },
  });
};
