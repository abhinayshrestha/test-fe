import { LoadingOutlined } from "@ant-design/icons";

import useVideoStore from "@/stores/video.store";

const UploadInfo = () => {
    const { uploadingVideosList } = useVideoStore();

    if (uploadingVideosList.length === 0) {
        return null;
    }

    return (
        <div className="bg-white border rounded-md p-3 max-w-[750px] space-y-1">
            <h3 className="font-semibold text-primary">Uploading Videos</h3>

            {uploadingVideosList.map((video) => {
                return (
                    <div
                        className="flex items-center justify-between border-b py-2 last:border-none"
                        key={video.id}
                    >
                        <h4 className="first-letter:capitalize text-gray-700">
                            {video.title}
                        </h4>
                        <LoadingOutlined />
                    </div>
                );
            })}
        </div>
    );
};

export default UploadInfo;
