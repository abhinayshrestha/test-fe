import { Button } from "antd";
import { FaPlay } from "react-icons/fa6";

const VideoPlayBtn = () => {
    return (
        <Button
            icon={<FaPlay />}
            type="text"
            shape="circle"
            className="flex items-center justify-center bg-accent text-gray-700"
        />
    );
};

export default VideoPlayBtn;
