import { useState, useEffect } from "react";
import { Form, Input, Modal, Upload, UploadProps, message } from "antd";
import { UploadChangeParam, UploadFile } from "antd/es/upload";
import { InboxOutlined } from "@ant-design/icons";
import TextArea from "antd/es/input/TextArea";

import { videoExtensions } from "@/utils/file.utils";
import useVideoStore from "@/stores/video.store";

import { useConvertVideo, useUploadVideo } from "../api/queries";

const { Dragger } = Upload;

interface IVideoUploadModalProps {
  isOpen: boolean;
  setIsOpen: (isOpen: boolean) => void;
}

const VideoUploadModal = ({ isOpen, setIsOpen }: IVideoUploadModalProps) => {
  const [videoFile, setVideoFile] = useState<any>(null);

  const { uploadingVideosList, setUploadingVideosList, removeUploadingVideo } =
    useVideoStore();

  const [videoUploadForm] = Form.useForm();

  const { mutate: convertVideo } = useConvertVideo();

  const { mutate: uploadVideo, isLoading: isUploadingVideo } = useUploadVideo(
    convertVideo as any,
    removeUploadingVideo,
    videoFile?.uid
  );

  useEffect(() => {
    videoUploadForm.setFieldsValue({
      name: videoFile?.name,
    });
  }, [videoFile, videoUploadForm]);

  useEffect(() => {
    if (isOpen) {
      setVideoFile(null);
    }
  }, [isOpen]);

  const closeModal = () => {
    setIsOpen(false);
  };

  const handleVideoUpload: UploadProps["onChange"] = (
    info: UploadChangeParam<UploadFile>
  ) => {
    setVideoFile(
      info.fileList.length === 0
        ? null
        : info.fileList[info.fileList.length - 1]
    );
  };

  const handleFormFinish = (values: any) => {
    if (!videoFile) {
      return message.error("Please upload a video file");
    }

    const { name, description } = values;

    uploadVideo({
      videoUid: videoFile.uid,
      video: videoFile.originFileObj,
      name,
      description,
    });

    setUploadingVideosList([
      ...uploadingVideosList,
      { id: videoFile.uid, title: name },
    ]);
    closeModal();
  };

  return (
    <Modal
      title="Upload Video"
      open={isOpen}
      onCancel={closeModal}
      onOk={() => videoUploadForm.submit()}
      okText="Upload"
    >
      <p className="text-gray-700 mb-3">
        Make sure that the video file is less than 1GB in size.
      </p>

      <Form
        form={videoUploadForm}
        onFinish={handleFormFinish}
        layout="vertical"
      >
        <Form.Item>
          <Dragger
            fileList={videoFile ? [videoFile] : []}
            accept={videoExtensions}
            listType="picture"
            onChange={handleVideoUpload}
            beforeUpload={() => false}
          >
            <div className="text-gray-700">
              <InboxOutlined className="text-[32px]" />

              <p>Click here or drag and drop video file to upload.</p>
            </div>
          </Dragger>
        </Form.Item>

        <Form.Item name="name" label="Video Title">
          <Input />
        </Form.Item>

        <Form.Item name="description" label="Description">
          <TextArea className="h-[100px] resize-none" />
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default VideoUploadModal;
