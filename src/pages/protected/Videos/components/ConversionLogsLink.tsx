import { Button } from "antd";
import { Link } from "react-router-dom";

import { protectedRoutePaths } from "@/router";
import { LoadingOutlined } from "@ant-design/icons";

const ConversionLogsLink = () => {
    return (
        <Link to={protectedRoutePaths.conversionLog}>
            <Button
                icon={<LoadingOutlined />}
                className="flex items-center border-primary text-primary"
            >
                View Logs
            </Button>
        </Link>
    );
};

export default ConversionLogsLink;
