import { Spin } from "antd";

import useServerSidePagination from "@/core/components/GenericTable/hooks/useServerSidePagination";
import PaginatedList from "@/core/components/PaginatedList";

import { useGetVideosList } from "../api/queries";

import VideoCard from "./VideoCard";

const VideosList = () => {
    const { initialFilterList, handleServerSideTableChange } =
        useServerSidePagination();

    const { data: videosData, isLoading: isLoadingVideosData } =
        useGetVideosList(initialFilterList);

    return (
        <div className="space-y-4">
            <p className="text-gray-700">
                These are the videos that have been converted and are ready to
                play.
            </p>

            <PaginatedList
                isLoading={isLoadingVideosData}
                isEmpty={videosData?.content?.length === 0}
                emptyMsg="No Videos Found"
                initialFilterList={initialFilterList}
                handleServerSideTableChange={handleServerSideTableChange}
                serverSideDataParams={{
                    currentPageIndex: videosData?.currentPageIndex,
                    numberOfElements: videosData?.numberOfElements,
                    totalElements: videosData?.totalElements,
                    totalPages: videosData?.totalPages,
                }}
            >
                <div className="space-y-3">
                    {videosData?.content?.map((video) => {
                        return <VideoCard {...video} key={video.id} />;
                    })}
                </div>
            </PaginatedList>
        </div>
    );
};

export default VideosList;
