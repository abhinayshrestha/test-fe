import { useMemo } from "react";
import { Link } from "react-router-dom";
import { TbProgressAlert } from "react-icons/tb";
import { Tag } from "antd";

import { IVideo, VideoConversionStatusEnum } from "@/schema/video.schema";
import useVideoStore from "@/stores/video.store";

import VideoPlayBtn from "./VideoPlayBtn";
import ConversionLogsLink from "./ConversionLogsLink";

const VideoCard = ({
    id,
    name,
    description,
    path,
    video_conversion_status,
}: IVideo) => {
    const { isConversionInProgress } = useVideoStore();

    const trimmedName = useMemo(() => {
        return name.split(".")[0];
    }, [name]);

    const videoPath = useMemo(() => {
        return path.split("/")[1].split(".")[0];
    }, [path]);

    return (
        <div className="flex items-center justify-between space-x-5 p-3 bg-white rounded-md max-w-[750px]">
            <div className="space-y-1">
                <h3 className="font-semibold text-primary first-letter:capitalize">
                    {trimmedName}
                </h3>

                {description && description !== "undefined" && (
                    <p className="text-gray-700 first-letter:capitalize">
                        {description}
                    </p>
                )}
            </div>

            {video_conversion_status ===
                VideoConversionStatusEnum.COMPLETED && (
                <Link to={`/videos/${videoPath}`}>
                    <VideoPlayBtn />
                </Link>
            )}

            {video_conversion_status === VideoConversionStatusEnum.STARTED &&
                isConversionInProgress && <ConversionLogsLink />}

            {video_conversion_status === VideoConversionStatusEnum.PENDING && (
                <Tag
                    color="green"
                    className="flex items-center text-sm px-3 py-1 gap-x-1"
                >
                    <TbProgressAlert />
                    Queued
                </Tag>
            )}
        </div>
    );
};

export default VideoCard;
