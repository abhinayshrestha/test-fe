import { useRef, useEffect } from "react";

import { useTitle } from "@/providers/TitleProvider";
import useVideoStore from "@/stores/video.store";

import DataUnavailable from "@/core/components/DataUnavailable";
import BackButton from "@/core/components/BackButton";

const ConversionLogs = () => {
    const { conversionLogs, setConversionLogs, isConversionInProgress } =
        useVideoStore();

    const logDataRef = useRef<HTMLDivElement>(null);

    const { setTitle } = useTitle();

    useEffect(() => {
        setTitle("Video Conversion Log");

        return () => {
            setTitle("");
        };
    }, [setTitle]);

    useEffect(() => {
        return () => {
            if (!isConversionInProgress) {
                setConversionLogs([]);
            }
        };
    }, [isConversionInProgress, setConversionLogs]);

    useEffect(() => {
        if (logDataRef.current) {
            logDataRef.current.scrollIntoView({
                behavior: "smooth",
                block: "end",
                inline: "nearest",
            });
        }
    });

    if (conversionLogs.length === 0) {
        return (
            <DataUnavailable title="No video is being converted at this moment" />
        );
    }

    return (
        <div ref={logDataRef} className="max-w-[700px] space-y-5">
            <div className="space-y-1">
                {conversionLogs.map((line) => {
                    return (
                        <div className="flex" key={line.timestamp}>
                            <p className="w-[120px] shrink-0">
                                {line.timestamp}
                            </p>

                            <p>{line.message}</p>
                        </div>
                    );
                })}
            </div>

            {!isConversionInProgress && (
                <div className="flex flex-col space-y-2">
                    <span>The video has been successfully converted.</span>

                    <div>
                        <BackButton type="primary" />
                    </div>
                </div>
            )}
        </div>
    );
};

export default ConversionLogs;
