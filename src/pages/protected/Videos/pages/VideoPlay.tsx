import { useParams } from "react-router-dom";

import { API_ENDPOINT } from "@/config/api.config";

import VideoPlayer from "@/core/components/VideoPlayer";

const VideoPlay = () => {
    const { filename } = useParams();

    return (
        <div className="space-y-3">
            <h3 className="text-base font-semibold">{filename}</h3>

            <VideoPlayer src={`${API_ENDPOINT}/video/play/${filename}/`} />
        </div>
    );
};

export default VideoPlay;
