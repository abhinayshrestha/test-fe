import { useState, useEffect } from "react";
import { Button } from "antd";
import { UploadOutlined } from "@ant-design/icons";

import { useTitle } from "@/providers/TitleProvider";
import useVideoStore from "@/stores/video.store";

import VideoUploadModal from "../components/VideoUploadModal";
import VideosList from "../components/VideosList";
import ConversionLogsLink from "../components/ConversionLogsLink";
import UploadInfo from "../components/UploadInfo";

const VideosHome = () => {
    const [isVideoUploadModalOpen, setIsVideoUploadModalOpen] = useState(false);

    const { isConversionInProgress } = useVideoStore();

    const { setTitle } = useTitle();

    useEffect(() => {
        setTitle("Video Library");

        return () => {
            setTitle("");
        };
    }, [setTitle]);

    return (
        <>
            {isVideoUploadModalOpen && (
                <VideoUploadModal
                    isOpen={isVideoUploadModalOpen}
                    setIsOpen={setIsVideoUploadModalOpen}
                />
            )}

            <div className="space-y-4">
                <div className="flex items-center justify-between">
                    <h3 className="text-base font-semibold">
                        Converted Videos
                    </h3>

                    <div className="flex items-center space-x-3">
                        <Button
                            type="primary"
                            icon={<UploadOutlined />}
                            onClick={() => setIsVideoUploadModalOpen(true)}
                        >
                            Upload Video
                        </Button>

                        {isConversionInProgress && <ConversionLogsLink />}
                    </div>
                </div>

                <UploadInfo />

                <VideosList />
            </div>
        </>
    );
};

export default VideosHome;
