import { Form, Input, Modal, message } from "antd";

import { IRevalidationPath } from "@/schema/revalidation.schema";
import { useRevalidatePath } from "@/core/queries/revalidation/queries";
import { convertStrIntoSlug, insertPathVariables } from "@/utils/string.utils";
import { Primitive } from "@/schema/http.schema";
import { pathRevalidationConfirmationMsg } from "@/utils/revalidation.utils";

interface IDynamicPathRevalidationProps {
  isOpen: boolean;
  setIsOpen: React.Dispatch<React.SetStateAction<boolean>>;
  revalidationPath?: IRevalidationPath | null;
}

const DynamicPathRevalidation = ({
  isOpen,
  setIsOpen,
  revalidationPath,
}: IDynamicPathRevalidationProps) => {
  const [revalidationForm] = Form.useForm();

  const { mutate: revalidatePath, isLoading: isRevalidatingPath } =
    useRevalidatePath();

  const closeModal = () => {
    setIsOpen(false);
    revalidationForm.resetFields();
  };

  const handleFormFinish = (values: any) => {
    if (!revalidationPath?.pathVariables) {
      return;
    }

    let pathVariablesObj: { [key: string]: Primitive } = {};

    revalidationPath.pathVariables.forEach((pathVariable) => {
      const pathVariableValue = values[pathVariable.name];

      pathVariablesObj = {
        ...pathVariablesObj,
        [pathVariable.name]: pathVariable.isSlug
          ? convertStrIntoSlug(pathVariableValue)
          : pathVariableValue,
      };
    });

    const revalidationPathStr = insertPathVariables(
      revalidationPath.pathname,
      pathVariablesObj
    );

    revalidatePath(revalidationPathStr, {
      onSuccess: () => {
        message.success(
          `${revalidationPathStr} has been revalidated successfully`
        );
        closeModal();
      },
    });
  };

  if (!revalidationPath?.pathVariables) {
    return null;
  }

  return (
    <Modal
      title={`Revalidate ${revalidationPath.pathname}`}
      open={isOpen}
      onCancel={closeModal}
      onOk={() => revalidationForm.submit()}
      okText="Revalidate"
      confirmLoading={isRevalidatingPath}
    >
      <div className="space-y-3">
        <p className="text-gray-700">{pathRevalidationConfirmationMsg}</p>

        <Form
          layout="vertical"
          form={revalidationForm}
          onFinish={handleFormFinish}
        >
          {revalidationPath.pathVariables.map((pathVariable) => {
            return (
              <Form.Item
                label={pathVariable.label}
                name={pathVariable.name}
                rules={[
                  {
                    required: true,
                    message: `Please enter a value for ${pathVariable.label}`,
                  },
                ]}
                key={pathVariable.name}
              >
                <Input />
              </Form.Item>
            );
          })}
        </Form>
      </div>
    </Modal>
  );
};

export default DynamicPathRevalidation;
