import { useState, useEffect } from "react";
import { Button, message } from "antd";
import { IoArrowRedoCircleOutline } from "react-icons/io5";

import { IRevalidationPath } from "@/schema/revalidation.schema";
import { useRevalidatePath } from "@/core/queries/revalidation/queries";
import ConfirmationModal from "@/core/components/ConfirmationModal";
import { pathRevalidationConfirmationMsg } from "@/utils/revalidation.utils";

import DynamicPathRevalidation from "./DynamicPathRevalidation";

const RevalidationPathCard = (props: IRevalidationPath) => {
  const { pathname, title, pathVariables, description } = props;

  const [isRevalidationModalOpen, setIsRevalidationModalOpen] = useState(false);
  const [activeRevalidationPath, setActiveRevalidationPath] =
    useState<IRevalidationPath | null>(null);
  const [isConfirmationModalOpen, setIsConfirmationModalOpen] = useState(false);

  const { mutate: revalidatePath, isLoading: isRevalidatingPath } =
    useRevalidatePath();

  useEffect(() => {
    if (!isRevalidationModalOpen) {
      setActiveRevalidationPath(null);
    }
  }, [isRevalidationModalOpen]);

  const handleRevalidateClick = () => {
    if (pathVariables && pathVariables?.length > 0) {
      setIsRevalidationModalOpen(true);
      return setActiveRevalidationPath(props);
    }

    setIsConfirmationModalOpen(true);
  };

  return (
    <>
      <DynamicPathRevalidation
        isOpen={isRevalidationModalOpen}
        setIsOpen={setIsRevalidationModalOpen}
        revalidationPath={activeRevalidationPath}
      />

      <ConfirmationModal
        isOpen={isConfirmationModalOpen}
        setIsOpen={setIsConfirmationModalOpen}
        title={`Revalidate ${pathname}`}
        description={`${pathRevalidationConfirmationMsg} Please confirm to continue.`}
        onConfirm={() =>
          revalidatePath(pathname, {
            onSuccess: () => {
              message.success("The path has been successfully revalidated");
              setIsConfirmationModalOpen(false);
            },
          })
        }
        isLoading={isRevalidatingPath}
      />

      <div className="flex space-x-5 items-center justify-between bg-white p-3 rounded-md shadow-sm max-w-[700px]">
        <div className="space-y-2">
          <h5 className="font-semibold text-primary">{pathname}</h5>

          <p className="first-letter:capitalize text-gray-700">{title}</p>

          {description && (
            <p className="first-letter:capitalize text-gray-700 italic">
              {description}
            </p>
          )}
        </div>

        <Button
          type="primary"
          icon={<IoArrowRedoCircleOutline size={15} />}
          className="flex items-center justify-center"
          onClick={handleRevalidateClick}
        >
          Revalidate
        </Button>
      </div>
    </>
  );
};

export default RevalidationPathCard;
