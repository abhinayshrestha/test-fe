import { useEffect } from "react";
import { Spin } from "antd";

import { useTitle } from "@/providers/TitleProvider";
import { useGetRevalidationPaths } from "@/core/queries/revalidation/queries";

import RevalidationPathCard from "./components/RevalidationPathCard";

const PathRevalidation = () => {
    const { setTitle } = useTitle();

    const { data: revalidationPaths, isLoading: isLoadingRevalidationPaths } =
        useGetRevalidationPaths();

    useEffect(() => {
        setTitle("Path Revalidation");
    }, [setTitle]);

    return (
        <Spin spinning={isLoadingRevalidationPaths}>
            <div className="space-y-4">
                <p className="text-gray-700">
                    Revalidate paths on Bhasika manually if required.
                </p>

                <div className="space-y-3">
                    {revalidationPaths?.map((path) => {
                        return (
                            <RevalidationPathCard
                                {...path}
                                key={path.pathname}
                            />
                        );
                    })}
                </div>
            </div>
        </Spin>
    );
};

export default PathRevalidation;
