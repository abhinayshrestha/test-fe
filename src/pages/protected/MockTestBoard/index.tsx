import { useTitle } from "@/providers/TitleProvider";
import { protectedRoutePaths, useNavigate } from "@/router";
import { Button, Col, Row } from "antd";
import { useEffect } from "react";
import CardDetails from "./components/CardDetails";

const MockTestBoard = () => {
  const { setTitle } = useTitle();

  const navigate = useNavigate();
  useEffect(() => {
    setTitle("MockTest Board");

    return () => {
      setTitle("");
    };
  }, [setTitle]);

  return (
    <Row gutter={[12, 12]}>
      <Col span={24} style={{ textAlign: "end" }}>
        <Button
          type="primary"
          onClick={() => navigate(protectedRoutePaths.examType)}
        >
          Create
        </Button>
      </Col>
      <Col span={24}>
        <CardDetails />{" "}
      </Col>
    </Row>
  );
};

export default MockTestBoard;
