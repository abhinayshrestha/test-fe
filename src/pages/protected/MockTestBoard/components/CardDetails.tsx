import Flexbox from "@/core/components/Flexbox";
import { useNavigate } from "@/router";
import {
  CaretRightOutlined,
  DeleteFilled,
  EditFilled,
  EyeFilled,
} from "@ant-design/icons";
import type { CollapseProps } from "antd";
import {
  Button,
  Col,
  Collapse,
  Divider,
  Empty,
  Image,
  Row,
  Space,
  Tooltip,
  Typography,
  theme,
} from "antd";
import dayjs from "dayjs";
import React, { useMemo, useState } from "react";
import ReactHtmlParser from "react-html-parser";
import copyIcon from "@/assets/vectors/copyIcon.svg";

const CardDetails: React.FC = () => {
  const { token } = theme.useToken();
  const navigate = useNavigate();

  const [toBeDeletedAssignment, setToBeDeleteAssignment] = useState<
    number | null
  >(null);

  const panelStyle = useMemo(() => {
    return {
      marginBottom: 16,
      background: "white",
      // background: token.colorFillAlter,
      borderRadius: 8,
      border: "none",
      // maxWidth: "100%"
    };
  }, []);

  const assignmentData = useMemo(() => {
    return [
      {
        id: 107,
        points: 56,
        postedOn: "2024-03-14 13:23:09.826115",
        dueDate: "2024-03-15 00:03:00",
        batchName: "Engineer Math ",
        assignmentName: "new quiz",
        description: '<p style="text-align:left;">rules and regulation</p>\n',
        assigned: 0,
        turnedIn: 0,
        duration: 5,
        scheduleDate: "2024-03-14",
      },
      {
        id: 105,
        points: 50,
        postedOn: "2024-03-12 14:14:33.166599",
        dueDate: "2024-03-26 00:00:00",
        batchName: "Engineer Math ",
        assignmentName: "Recusandae Qui qui ",
        description: "<p>a fas</p>\n",
        assigned: 2,
        turnedIn: 0,
        duration: 30,
        scheduleDate: "2024-03-15",
      },
    ];
  }, []);

  const collapsibleItems: CollapseProps["items"] = useMemo(() => {
    if (assignmentData) {
      return assignmentData?.map((aData: any, index: number) => {
        return {
          key: index,
          style: panelStyle,
          showArrow: false,
          label: (
            <>
              <Flexbox justify="space-between" align="center" flexWrap="wrap">
                <Space>
                  <img src={copyIcon} alt="icon" />
                  <Typography.Text style={{ color: token.colorPrimary }}>
                    {aData?.assignmentName}
                  </Typography.Text>
                </Space>
                <Typography.Text>{`Due ${dayjs(aData?.dueDate).format(
                  "DD MMM YYYY"
                )}`}</Typography.Text>
              </Flexbox>
              <Flexbox>
                <Typography.Text>{`Posted on ${dayjs(aData?.postedOn).format(
                  "DD MMM YYYY"
                )}`}</Typography.Text>
              </Flexbox>
            </>
          ),
          extra: (
            <Space style={{ paddingInline: 8 }}>
              <Tooltip title="Edit">
                <Button
                  type="text"
                  size="small"
                  shape="circle"
                  icon={<EditFilled />}
                  style={{
                    color: token.colorPrimary,
                  }}
                />
              </Tooltip>
              <Tooltip title="View">
                <Button
                  type="text"
                  size="small"
                  shape="circle"
                  icon={<EyeFilled />}
                  style={{
                    color: token.colorPrimary,
                  }}
                />
              </Tooltip>
              <Tooltip title="Delete">
                <Button
                  type="text"
                  size="small"
                  shape="circle"
                  icon={<DeleteFilled />}
                  style={{
                    color: token.colorPrimary,
                  }}
                  onClick={(event) => {
                    event.stopPropagation();
                    setToBeDeleteAssignment(aData.id);
                  }}
                />
              </Tooltip>
            </Space>
          ),
          children: (
            <>
              <Typography.Text>
                {ReactHtmlParser(aData?.description)}
              </Typography.Text>
              <Flexbox justify="flex-end" flexWrap="wrap" className="gap-2">
                <Space direction="vertical" size={1} className="text-center">
                  <Typography.Text
                    style={{
                      fontSize: 24,
                      fontWeight: 500,
                      color: token.colorPrimary,
                    }}
                  >
                    {aData?.turnedIn}
                  </Typography.Text>
                  <Typography.Text>Turned In</Typography.Text>
                </Space>
                <Divider
                  type="vertical"
                  style={{ backgroundColor: "#CED4DA", height: "58px" }}
                />
                <Space direction="vertical" size={1} className="text-center">
                  <Typography.Text
                    style={{
                      fontSize: 24,
                      fontWeight: 500,
                      color: token.colorPrimary,
                    }}
                  >
                    {aData?.assigned}
                  </Typography.Text>
                  <Typography.Text>Assigned</Typography.Text>
                </Space>
                <Divider
                  type="vertical"
                  style={{ backgroundColor: "#CED4DA", height: "58px" }}
                />
                <Space direction="vertical" size={1} className="text-center">
                  <Typography.Text
                    style={{
                      fontSize: 24,
                      fontWeight: 500,
                      color: token.colorPrimary,
                    }}
                  >
                    {aData?.points}
                  </Typography.Text>
                  <Typography.Text>Points</Typography.Text>
                </Space>
              </Flexbox>
            </>
          ),
        };
      });
    }
  }, [assignmentData, panelStyle, token]);

  return (
    <>
      <Row justify="center" align="top" className="h-full w-full">
        {collapsibleItems && collapsibleItems?.length > 0 ? (
          <Col
            style={{
              width: "100%",
              maxWidth: "900px",
            }}
          >
            <Collapse
              bordered={false}
              // defaultActiveKey={[0]}
              expandIcon={({ isActive }) => (
                <CaretRightOutlined rotate={isActive ? 90 : 0} />
              )}
              className="w-full bg-transparent"
              items={collapsibleItems}
            />
          </Col>
        ) : (
          <Col span={24} className="grid h-full place-items-center">
            <Empty />
          </Col>
        )}
      </Row>
    </>
  );
};

export default CardDetails;
