import Flexbox from "@/core/components/Flexbox";
import CustomEditor from "@/core/components/WysiwygEditor/CustomEditor";
import {
  ArrowLeftOutlined,
  InboxOutlined,
  PaperClipOutlined,
} from "@ant-design/icons";
import {
  Button,
  Checkbox,
  Col,
  DatePicker,
  Form,
  Input,
  InputNumber,
  Row,
  Space,
  Tag,
  TreeSelect,
  Typography,
  Upload,
  UploadProps,
} from "antd";
import { useState } from "react";
import { BiImage } from "react-icons/bi";
import { PiGoogleDriveLogo } from "react-icons/pi";
import quizLogo from "@/assets/vectors/quizLogo.svg";
import { videoExtensions } from "@/utils/file.utils";
import { UploadChangeParam, UploadFile } from "antd/es/upload";
import { protectedRoutePaths, useNavigate, useParams } from "@/router";

const { SHOW_PARENT } = TreeSelect;

const AttachmentsAdd = () => {
  return (
    <Flexbox align="center">
      <span className="mr-3 cursor-pointer  rounded-lg bg-red-100 px-2 py-[6px]">
        <PiGoogleDriveLogo size={24} className=" text-primary" />
      </span>
      <span className="mr-3 cursor-pointer  rounded-lg bg-red-100 px-2 py-[6px]">
        <BiImage size={24} className=" text-primary" />
      </span>
      <span className="cursor-pointer rounded-lg bg-red-100 px-2 py-[6px]">
        <PaperClipOutlined size={24} className=" text-primary" />
      </span>
    </Flexbox>
  );
};
const { Dragger } = Upload;

const QuizForm = () => {
  const [assignmentForm] = Form.useForm();
  const params = useParams();
  const navigate = useNavigate();
  const [instructions, setInstructions] = useState<string | null>(null);
  const [videoFile, setVideoFile] = useState<any>(null);

  const finishHandler = (values: any) => {
    navigate(protectedRoutePaths.quiz);
    console.log("14", values);
  };

  const handleVideoUpload: UploadProps["onChange"] = (
    info: UploadChangeParam<UploadFile>
  ) => {
    setVideoFile(
      info.fileList.length === 0
        ? null
        : info.fileList[info.fileList.length - 1]
    );
  };
  const tagRender = (tagProps: any) => {
    const { label, closable, onClose } = tagProps;
    const onPreventMouseDown = (event: any) => {
      event.preventDefault();
      event.stopPropagation();
    };

    console.log(instructions);
    return (
      <Tag
        onMouseDown={onPreventMouseDown}
        closable={closable}
        onClose={onClose}
        style={{
          marginRight: 3,
          background: "transparent",
          borderRadius: 12,
        }}
      >
        {label}
      </Tag>
    );
  };

  return (
    <div className="flex min-h-full flex-col p-[2vw]">
      <Flexbox align="center" justify="space-between" className="gap-2 pb-2">
        <Space>
          <img src={quizLogo} alt="quiz" height={20} width={30} />
          <Typography.Text className="text-primary">New Quiz</Typography.Text>
        </Space>
        <Space>
          <Button
            type="text"
            icon={<ArrowLeftOutlined />}
            onClick={() => navigate(protectedRoutePaths.mockTestBoard)}
          >
            Go Back
          </Button>
          <Button
            type="primary"
            htmlType="submit"
            onClick={() => {

              assignmentForm.submit();
            }}
          >
            Submit
          </Button>
        </Space>
      </Flexbox>
      <Flexbox className="flex flex-grow">
        <Row justify="space-between" gutter={[8, 8]}>
          <Col xs={24} lg={12}>
            <Form
              form={assignmentForm}
              onFinish={finishHandler}
              layout="vertical"
              className="flex w-full flex-col gap-3 rounded-md bg-white p-4"
            >
              <Form.Item
                label="Title"
                name="title"
                rules={[
                  { required: true, message: "Please enter quiz title!" },
                ]}
              >
                <Input.TextArea
                  placeholder="Please enter assignment title"
                  autoSize={{ minRows: 3 }}
                />
              </Form.Item>
              <Form.Item name="instructions">
                <CustomEditor
                  placeholder="Instructions here..."
                  setDescription={setInstructions}
                />
              </Form.Item>
            </Form>
          </Col>
          <Col xs={24} lg={12}>
            <Form
              form={assignmentForm}
              onFinish={finishHandler}
              layout="vertical"
              className="flex h-full w-full flex-col gap-3 rounded-md bg-white p-4"
            >
              <Row gutter={[16, 16]}>
                <Col span={24}>
                  <Form.Item label="Schedule Date" name="scheduleDate">
                    <DatePicker
                      changeOnBlur
                      className="w-full"
                      placement="bottomLeft"
                      placeholder="select schedule date"
                      format="DD MMM YYYY"
                    />
                  </Form.Item>
                </Col>
                <Col span={24}>
                  <Form.Item
                    label="Duration (in minutes)"
                    name="duration"
                    rules={[
                      {
                        required: true,
                        message: "Please enter assessment duration!",
                      },
                    ]}
                  >
                    <InputNumber
                      type="number"
                      min={0}
                      style={{ width: "100%" }}
                      placeholder="Enter time duration"
                    />
                  </Form.Item>
                </Col>
                <Col lg={12} md={12} xs={24}>
                  <Form.Item
                    label="Points"
                    name="points"
                    rules={[
                      {
                        required: true,
                        message: "Please enter total points!",
                      },
                    ]}
                  >
                    <InputNumber
                      type="number"
                      min={0}
                      style={{ width: "100%" }}
                      placeholder="Enter points"
                    />
                  </Form.Item>
                </Col>
                <Col lg={12} md={12} xs={24}>
                  <Form.Item label="Due Date" name="dueDate">
                    <DatePicker
                      changeOnBlur
                      className="w-full"
                      placement="bottomLeft"
                      placeholder="select due date"
                      showTime={{ format: "hh:mm A" }}
                      format="DD MMM YYYY hh:mm A"
                    />
                  </Form.Item>
                </Col>
                <Col span={24}>
                  <Dragger
                    fileList={videoFile ? [videoFile] : []}
                    accept={videoExtensions}
                    listType="picture"
                    onChange={handleVideoUpload}
                    beforeUpload={() => false}
                  >
                    <div className="text-gray-700">
                      <InboxOutlined className="text-[32px]" />

                      <p>Click here or drag and drop video file to upload.</p>
                    </div>
                  </Dragger>
                </Col>
              </Row>
            </Form>
          </Col>
        </Row>
      </Flexbox>
    </div>
  );
};

export default QuizForm;
