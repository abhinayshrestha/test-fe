import { useTitle } from "@/providers/TitleProvider";
import { useNavigate } from "@/router";
import { ArrowLeftOutlined } from "@ant-design/icons";
import { Button, Col, Empty, Form, Row, Select, Space } from "antd";
import { useEffect, useState } from "react";
import AssignmentForm from "./AssignmentForm";
import Quiz from "./Quiz";
import QuizForm from "./QuizForm";

const Exam = () => {
    const [selectedService, setSelectedService] = useState<any>({});
    const [selectedExamType, setSelectedExamType] = useState<any>({});

    const [selectedValues, setSelectedValues] = useState({
        serviceValue: undefined,
        examValue: undefined,
    });
    const navigate = useNavigate();
    const { setTitle } = useTitle();
    const [examForm] = Form.useForm();

    const examType = [
        { label: "Reading", value: "reading" },
        { label: "Writing", value: "writing" },
        { label: "Listening", value: "listening" },
        { label: "Speaking", value: "speaking" },
    ];
    const serviceType = [
        { label: "TOFEL", value: "tofel" },
        { label: "GRE", value: "gre" },
        { label: "IELTS", value: "ielts" },
        { label: "MOCK TEST", value: "mocktest" },
    ];

    useEffect(() => {
        setTitle("MockTest Board");

        return () => {
            setTitle("");
        };
    }, [setTitle]);
    console.log(selectedValues?.examValue);
    return (
        <Row gutter={[12, 12]}>
            <Col span={24} style={{ textAlign: "end" }}>
                <Button
                    icon={<ArrowLeftOutlined />}
                    onClick={() => navigate(-1)}
                >
                    Back
                </Button>
            </Col>
            <Col span={24}>
                <Form name="examForm">
                    <Space>
                        <Form.Item
                            rules={[
                                {
                                    required: true,
                                    message: "Please select a service",
                                },
                            ]}
                        >
                            <Select
                                style={{ minWidth: "10rem" }}
                                mode="tags"
                                placeholder="Select or enter the service"
                                showSearch
                                options={serviceType}
                                value={selectedService?.serviceId}
                                filterOption={(
                                    input: string,
                                    option?: { label: string; value: string }
                                ) =>
                                    (option?.label ?? "")
                                        .toLowerCase()
                                        .includes(input.toLowerCase())
                                }
                                allowClear
                                onClear={() => setSelectedService({})}
                                onChange={(value: any, options: any) => {
                                    setSelectedService((prevValue: any) => {
                                        const newValue = value?.filter(
                                            (item: any) =>
                                                item !== prevValue.serviceId
                                        );
                                        const newOption = options?.filter(
                                            (option: any) =>
                                                option.value !==
                                                prevValue.serviceId
                                        );

                                        const isManual =
                                            !newOption[0]?.label &&
                                            !newOption?.value;

                                        console.log({ newOption });
                                        setSelectedValues({
                                            ...selectedValues,
                                            serviceValue: value,
                                        });

                                        return {
                                            isManual,
                                            serviceId: isManual
                                                ? newValue[0] || undefined
                                                : newOption[0]?.value,
                                            ServiceName: isManual
                                                ? newValue[0]
                                                : newOption[0]?.label,
                                        };
                                    });
                                }}
                            />
                        </Form.Item>
                        <Form.Item
                            rules={[
                                {
                                    required: true,
                                    message: "Please select a exam type",
                                },
                            ]}
                        >
                            <Select
                                style={{ minWidth: "10rem" }}
                                mode="tags"
                                placeholder="Select or enter the exam type"
                                showSearch
                                options={examType}
                                value={selectedExamType?.examTypeId}
                                filterOption={(
                                    input: string,
                                    option?: { label: string; value: string }
                                ) =>
                                    (option?.label ?? "")
                                        .toLowerCase()
                                        .includes(input.toLowerCase())
                                }
                                allowClear
                                onClear={() => setSelectedExamType({})}
                                onChange={(value: any, options: any) => {
                                    setSelectedExamType((prevValue: any) => {
                                        const newValue = value?.filter(
                                            (item: any) =>
                                                item !== prevValue.examTypeId
                                        );
                                        const newOption = options?.filter(
                                            (option: any) =>
                                                option.value !==
                                                prevValue.examTypeId
                                        );

                                        const isManual =
                                            !newOption[0]?.label &&
                                            !newOption?.value;
                                        setSelectedValues({
                                            ...selectedValues,
                                            examValue: value,
                                        });

                                        console.log({ newOption });

                                        return {
                                            isManual,
                                            examTypeId: isManual
                                                ? newValue[0] || undefined
                                                : newOption[0]?.value,
                                            ExamTypeName: isManual
                                                ? newValue[0]
                                                : newOption[0]?.label,
                                        };
                                    });
                                }}
                            />
                        </Form.Item>
                    </Space>
                </Form>
            </Col>
            <Col span={24}>
                {selectedValues?.serviceValue?.[0] === "ielts" &&
                    selectedValues?.examValue?.[0] === "writing" && (
                        <AssignmentForm />
                    )}
                {selectedValues?.serviceValue?.[0] === "ielts" &&
                    (selectedValues?.examValue?.[0] === "reading" ||
                        selectedValues?.examValue?.[0] === "listening") && (
                        <QuizForm />
                    )}

                {selectedValues?.serviceValue?.[0] === undefined &&
                    selectedValues?.examValue?.[0] === undefined && <Empty />}
                {selectedValues?.serviceValue?.[0] === "mocktest" && <Quiz />}
            </Col>
        </Row>
    );
};

export default Exam;
