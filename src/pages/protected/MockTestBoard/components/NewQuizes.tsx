import BackButton from "@/core/components/BackButton";
import Flexbox from "@/core/components/Flexbox";
import { useTitle } from "@/providers/TitleProvider";
import { protectedRoutePaths, useNavigate } from "@/router";
import {
  DeleteOutlined,
  PlusOutlined
} from "@ant-design/icons";
import {
  Button,
  Card,
  Checkbox,
  Col,
  DatePicker,
  Divider,
  Form,
  Input,
  InputNumber,
  Row,
  Select,
  Space,
  Typography,
  theme
} from "antd";
import { useEffect } from "react";

const { useToken } = theme;

const NewQuizes = () => {
  const [quizFormInstance] = Form.useForm();
  const { token } = useToken();
  const { setTitle } = useTitle();

  const navigate = useNavigate();
  useEffect(() => {
    setTitle("Create Quiz");

    return () => {
        setTitle("");
    };
}, [setTitle]);
  return (
    <Row className="flex w-full justify-center ">
      <Col span={24}>
        <Flexbox align="center" justify="space-between" className="gap-2 pb-4">
          <Button
            type="text"
            className="text-[#A62A22]"
            icon={<BackButton />}
          />

          <Button
            type="primary"
            onClick={() => navigate(protectedRoutePaths.mockTestBoard)}
          >
            Submit
          </Button>
        </Flexbox>
      </Col>
      <Card
        title={
          <Flexbox className="flex flex-col flex-wrap">
            <Typography.Text className="text-[#FFF]">
              Quiz Title
            </Typography.Text>
            <Typography.Text className="text-[#FFF]">
              Hi, Please take your time to complete this quiz.{" "}
            </Typography.Text>
          </Flexbox>
        }
        bordered={false}
        headStyle={{
          padding: 24,
          background: token.colorPrimary,
          display: "flex",
          flexWrap: "wrap",
        }}
        style={{ width: "100%", maxWidth: "1100px" }}
      >
        <Form
          wrapperCol={{ offset: 2, span: 22 }}
          form={quizFormInstance}
          name="dynamic_form_complex"
          autoComplete="off"
          initialValues={{ quizList: [{}] }}
        >
          <Space
            direction="vertical"
            style={{
              display: "none",
              width: "100%",
            }}
          >
            <Form.Item label="Title" name="title">
              <Input placeholder="title" />
            </Form.Item>
            <Form.Item name="instructions">
              <Input.TextArea
                placeholder="Instructions(optional)"
                autoSize={{ minRows: 10, maxRows: 14 }}
                className="text-[#6C757D]"
              />
            </Form.Item>

            <Form.Item label="Duration" name="duration">
              <InputNumber
                type="number"
                min={0}
                placeholder="Enter time duration"
              />
            </Form.Item>
            <Form.Item label="Points" name="points">
              <InputNumber type="number" min={0} placeholder="Enter points" />
            </Form.Item>
            <Form.Item label="Due Date" name="dueDate">
              <DatePicker
                className="w-full"
                placement="bottomLeft"
                placeholder="select due date"
                showTime={{ format: "hh:mm A" }}
                format="DD MMM YYYY hh:mm A"
              />
            </Form.Item>
          </Space>

          <Form.List name="quizList">
            {(fields, { add, remove }) => {
              return (
                <div
                  style={{
                    display: "flex",
                    rowGap: 16,
                    flexDirection: "column",
                  }}
                >
                  {fields.map((field) => (
                    <Card
                      key={field.key}
                      size="small"
                      title={
                        <>
                          <Form.Item
                            hidden
                            name={[field.name, "questionId"]}
                            labelCol={{ span: 1 }}
                            wrapperCol={{ span: 22 }}
                            label={field.name + 1}
                          >
                            <Input />
                          </Form.Item>
                          <Form.Item
                            name={[field.name, "question"]}
                            labelCol={{ span: 1 }}
                            wrapperCol={{ span: 22 }}
                            label={field.name + 1}
                            style={{ width: "100%", margin: 0 }}
                          >
                            <Input.TextArea
                              placeholder="Enter question"
                              autoSize={{ minRows: 1, maxRows: 5 }}
                            />
                          </Form.Item>
                        </>
                      }
                      extra={
                        <DeleteOutlined
                          onClick={() => {
                            remove(field.name);
                          }}
                        />
                      }
                      style={{ background: "#F5F5F5" }}
                      headStyle={{ padding: 10 }}
                    >
                      <Form.Item name={[field.name, "initialOptions"]} hidden>
                        <Select mode="multiple" />
                      </Form.Item>
                      <Form.Item style={{ display: "flex" }}>
                        <Form.List name={[field.name, "options"]}>
                          {(subFields, subOpt) => (
                            <Flexbox
                              direction="column"
                              className="w-full gap-2"
                            >
                              {subFields.map((subField) => (
                                <Space key={subField.key} className=" w-full">
                                  <Form.Item
                                    hidden
                                    className="m-0"
                                    style={{ width: "100%" }}
                                    name={[subField.name, "optionValueId"]}
                                  >
                                    <Input />
                                  </Form.Item>
                                  <Form.Item
                                    className="m-0"
                                    style={{ width: "100%" }}
                                    name={[subField.name, "isAnswer"]}
                                    valuePropName="checked"
                                  >
                                    <Checkbox />
                                  </Form.Item>
                                  <Form.Item
                                    className="m-0"
                                    style={{ width: "100%" }}
                                    name={[subField.name, "optionValue"]}
                                  >
                                    <Input
                                      placeholder={`Option ${
                                        subField.name + 1
                                      }`}
                                      style={{
                                        minWidth: "480px",
                                        width: "100%",
                                      }}
                                    />
                                  </Form.Item>
                                  <DeleteOutlined
                                    onClick={() => {
                                      subOpt.remove(subField.name);
                                    }}
                                  />
                                </Space>
                              ))}
                              <Space>
                                <Button
                                  type="text"
                                  style={{ color: token.colorPrimary }}
                                  icon={<PlusOutlined />}
                                  onClick={() => subOpt.add()}
                                  block
                                >
                                  Add Option
                                </Button>
                              </Space>
                            </Flexbox>
                          )}
                        </Form.List>
                      </Form.Item>
                      <Divider style={{ marginBottom: 6 }} />
                      <Row justify="space-between">
                        <Form.Item
                          label="Points"
                          name={[field.name, "weight"]}
                          style={{ marginBottom: 0 }}
                          labelCol={{ span: 15 }}
                          wrapperCol={{ span: 9 }}
                        >
                          <Input size="small" style={{ width: "50px" }} />
                        </Form.Item>

                       
                      </Row>
                    </Card>
                  ))}
                  <Flexbox justify="space-between">
                    <Space>
                      <Button
                        type="primary"
                        icon={<PlusOutlined />}
                        onClick={() => add()}
                        block
                      >
                        Add Question
                      </Button>
                    </Space>
                  </Flexbox>
                </div>
              );
            }}
          </Form.List>
        </Form>
      </Card>
    </Row>
  );
};

export default NewQuizes;
