import Flexbox from "@/core/components/Flexbox";
import CustomEditor from "@/core/components/WysiwygEditor/CustomEditor";
import { ArrowLeftOutlined, PaperClipOutlined } from "@ant-design/icons";
import {
  Button,
  Checkbox,
  Col,
  DatePicker,
  Form,
  Input,
  InputNumber,
  Row,
  Space,
  Tag,
  TreeSelect,
  Typography,
} from "antd";
import { useState } from "react";
import { BiImage } from "react-icons/bi";
import { PiGoogleDriveLogo } from "react-icons/pi";
import { useNavigate, useParams } from "react-router-dom";
import quizLogo from "@/assets/vectors/quizLogo.svg";

const { SHOW_PARENT } = TreeSelect;

const AttachmentsAdd = () => {
  return (
    <Flexbox align="center">
      <span className="mr-3 cursor-pointer  rounded-lg bg-red-100 px-2 py-[6px]">
        <PiGoogleDriveLogo size={24} className=" text-primary" />
      </span>
      <span className="mr-3 cursor-pointer  rounded-lg bg-red-100 px-2 py-[6px]">
        <BiImage size={24} className=" text-primary" />
      </span>
      <span className="cursor-pointer rounded-lg bg-red-100 px-2 py-[6px]">
        <PaperClipOutlined size={24} className=" text-primary" />
      </span>
    </Flexbox>
  );
};

const AssignmentForm = () => {
  const [assignmentForm] = Form.useForm();
  const params = useParams();
  const navigate = useNavigate();
  const [instructions, setInstructions] = useState<string | null>(null);

  const finishHandler = (values: any) => {};

  const tagRender = (tagProps: any) => {
    const { label, closable, onClose } = tagProps;
    const onPreventMouseDown = (event: any) => {
      event.preventDefault();
      event.stopPropagation();
    };

    console.log(instructions);
    return (
      <Tag
        onMouseDown={onPreventMouseDown}
        closable={closable}
        onClose={onClose}
        style={{
          marginRight: 3,
          background: "transparent",
          borderRadius: 12,
        }}
      >
        {label}
      </Tag>
    );
  };

  return (
    <div className="flex min-h-full flex-col p-[2vw]">
      <Flexbox align="center" justify="space-between" className="gap-2 pb-2">
        <Space>
          <img src={quizLogo} alt="quiz" height={20} width={30} />
          <Typography.Text className="text-primary">
            New Assignment
          </Typography.Text>
        </Space>
        <Space>
          <Button
            type="text"
            icon={<ArrowLeftOutlined />}
            onClick={() =>
              navigate(`/teacher-class-board/${params.classData}/assignment`)
            }
          >
            Go Back
          </Button>
          <Button
            type="primary"
            onClick={() => {
              // setIsModalOpen(true)
            }}
          >
            Submit
          </Button>
        </Space>
      </Flexbox>
      <Flexbox className="flex flex-grow">
        <Row justify="space-between" gutter={[8, 8]}>
          <Col xs={24} lg={12}>
            <Form
              form={assignmentForm}
              layout="vertical"
              className="flex w-full flex-col gap-3 rounded-md bg-white p-4"
            >
              <Form.Item
                label="Title"
                name="title"
                rules={[
                  { required: true, message: "Please enter quiz title!" },
                ]}
              >
                <Input.TextArea
                  placeholder="Please enter assignment title"
                  autoSize={{ minRows: 3 }}
                />
              </Form.Item>
              <Form.Item name="instructions">
                <CustomEditor
                  placeholder="Instructions here..."
                  setDescription={setInstructions}
                />
              </Form.Item>
            </Form>
          </Col>
          <Col xs={24} lg={12}>
            <Form
              form={assignmentForm}
              layout="vertical"
              className="flex h-full w-full flex-col gap-3 rounded-md bg-white p-4"
              onFinish={finishHandler}
            >
              <Row gutter={[16, 16]}>
                <Col span={24}>
                  <Form.Item label="Schedule Date" name="scheduleDate">
                    <DatePicker
                      changeOnBlur
                      className="w-full"
                      placement="bottomLeft"
                      placeholder="select schedule date"
                      format="DD MMM YYYY"
                    />
                  </Form.Item>
                </Col>
                <Col lg={12} md={12} xs={24}>
                  <Form.Item
                    label="Points"
                    name="points"
                    rules={[
                      {
                        required: true,
                        message: "Please enter total points!",
                      },
                    ]}
                  >
                    <InputNumber
                      type="number"
                      min={0}
                      style={{ width: "100%" }}
                      placeholder="Enter points"
                    />
                  </Form.Item>
                </Col>
                <Col lg={12} md={12} xs={24}>
                  <Form.Item label="Due Date" name="dueDate">
                    <DatePicker
                      changeOnBlur
                      className="w-full"
                      placement="bottomLeft"
                      placeholder="select due date"
                      showTime={{ format: "hh:mm A" }}
                      format="DD MMM YYYY hh:mm A"
                    />
                  </Form.Item>
                </Col>
              </Row>
              <Form.Item
                name="hasWordCountSetting"
                valuePropName="checked"
                style={{ marginBottom: 8 }}
              >
                <Checkbox>Word Count Setting</Checkbox>
              </Form.Item>
              <Form.Item
                noStyle
                shouldUpdate={(prevValues, currentValues) =>
                  prevValues.hasWordCountSetting !==
                  currentValues.hasWordCountSetting
                }
              >
                {({ getFieldValue }) =>
                  getFieldValue("hasWordCountSetting") && (
                    <Row gutter={[16, 16]}>
                      <Col lg={12} md={12} xs={24}>
                        <Form.Item
                          label="Minimum words"
                          name="minWordCount"
                          rules={[
                            {
                              required: true,
                              message: "Please enter minimum word count!",
                            },
                          ]}
                        >
                          <InputNumber
                            type="number"
                            min={0}
                            style={{ width: "100%" }}
                            placeholder="Enter minimum word count"
                          />
                        </Form.Item>
                      </Col>
                      <Col lg={12} md={12} xs={24}>
                        <Form.Item
                          label="Maximum words"
                          name="maxWordCount"
                          rules={[
                            {
                              required: true,
                              message: "Please enter maximum word count!",
                            },
                          ]}
                        >
                          <InputNumber
                            type="number"
                            min={0}
                            style={{ width: "100%" }}
                            placeholder="Enter maximum word count"
                          />
                        </Form.Item>
                      </Col>
                    </Row>
                  )
                }
              </Form.Item>
            </Form>
          </Col>
        </Row>
      </Flexbox>
    </div>
  );
};

export default AssignmentForm;
