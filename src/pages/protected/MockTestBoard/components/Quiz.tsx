import Flexbox from "@/core/components/Flexbox";
import { useTitle } from "@/providers/TitleProvider";
import { useNavigate } from "@/router";
import {
    Button,
    Card,
    Col,
    Form,
    Radio,
    Row,
    Space,
    Typography,
    theme,
} from "antd";
import { useEffect, useState } from "react";

const { useToken } = theme;
const Quiz = () => {
  const [quizFormInstance] = Form.useForm();
  const { token } = useToken();
  const { setTitle } = useTitle();
  const [currentQuestionIndex, setCurrentQuestionIndex] = useState(0);
  const navigate = useNavigate();

  useEffect(() => {
    setTitle("Create Quiz");
    return () => {
      setTitle("");
    };
  }, [setTitle]);

  const handleSubmit = () => {
    console.log("Form submitted!");
  };

  const handleNextQuestion = () => {
    if (currentQuestionIndex < 3) {
      setCurrentQuestionIndex((prevIndex) => prevIndex + 1);
    } else {
      handleSubmit();
    }
  };

  const initialValues = {
    quizList: [
      {
        question: "What is the capital of France?",
        options: [
          { optionText: "Paris" },
          { optionText: "London" },
          { optionText: "Berlin" },
          { optionText: "Madrid" },
        ],
      },
      {
        question: "Who wrote 'To Kill a Mockingbird'?",
        options: [
          { optionText: "Harper Lee" },
          { optionText: "J.K. Rowling" },
          { optionText: "Stephen King" },
          { optionText: "Charles Dickens" },
        ],
      },
      {
        question: "What is the chemical symbol for water?",
        options: [
          { optionText: "H2O" },
          { optionText: "CO2" },
          { optionText: "NaCl" },
          { optionText: "O2" },
        ],
      },
      {
        question: "Which planet is known as the Red Planet?",
        options: [
          { optionText: "Mars" },
          { optionText: "Venus" },
          { optionText: "Jupiter" },
          { optionText: "Mercury" },
        ],
      },
    ],
  };

  return (
    <Row className="flex w-full justify-center">
      <Card
        title={
          <Flexbox className="flex flex-col flex-wrap">
            <Typography.Text className="text-[#FFF]">
              Quiz Title
            </Typography.Text>
            <Typography.Text className="text-[#FFF]">
              Hi, Please take your time to complete this quiz.
            </Typography.Text>
          </Flexbox>
        }
        bordered={false}
        headStyle={{
          padding: 24,
          background: token.colorPrimary,
          display: "flex",
          flexWrap: "wrap",
        }}
        style={{ width: "100%", maxWidth: "1100px" }}
      >
        <Form
          wrapperCol={{ offset: 2, span: 22 }}
          form={quizFormInstance}
          name="dynamic_form_complex"
          autoComplete="off"
          initialValues={initialValues}
          onFinish={handleSubmit}
        >
          <Card style={{backgroundColor:token.colorFillSecondary}}>
            <Row gutter={[6, 6]}>
              <Col span={24}>
                <Space>
                  <span style={{ fontWeight: "bold" }}>
                    {currentQuestionIndex + 1}:
                  </span>
                  <Typography.Text>
                    {initialValues.quizList[currentQuestionIndex].question}
                  </Typography.Text>
                </Space>
              </Col>
              <Col
                span={24}
                style={{
                  display: "flex",
                  justifyContent: "left",
                  marginLeft: "1rem",
                }}
              >
                <Form.Item
                  name={`quizList[${currentQuestionIndex}].options`}
                  initialValue={[]}
                >
                  <Radio.Group>
                    
                    {initialValues.quizList[currentQuestionIndex].options.map(
                      (option, optionIndex) => (
                        <Col span={24} key={optionIndex}>
                          <Radio value={option.optionText}  style={{width:'max-content'}}> 
                            {option.optionText}
                          </Radio>
                        </Col>
                      )
                    )}
                  </Radio.Group>
                </Form.Item>
              </Col>
              <Col span={24} style={{ textAlign: "end" }}>
                <Form.Item>
                  <Button type="primary" onClick={handleNextQuestion}>
                    {currentQuestionIndex < 3 ? "Next" : "Submit"}
                  </Button>
                </Form.Item>
              </Col>
            </Row>
          </Card>
        </Form>
      </Card>
    </Row>
  );
};

export default Quiz;
