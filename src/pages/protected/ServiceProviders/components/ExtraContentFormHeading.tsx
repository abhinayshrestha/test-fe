import { FaRegTrashAlt } from "react-icons/fa";
import { theme } from "antd";

import { StyledDeleteButton } from "../styles";

interface IExtraContentFormHeadingProps {
    title: string;
    onDeleteClick: () => void;
}

const ExtraContentFormHeading = ({
    title,
    onDeleteClick,
}: IExtraContentFormHeadingProps) => {
    const { useToken } = theme;
    const { token } = useToken();

    return (
        <div className="flex items-center justify-between">
            <h4 className="font-semibold text-primary">{title}</h4>

            <StyledDeleteButton
                icon={<FaRegTrashAlt />}
                type="text"
                className="flex items-center justify-center bg-gray-200 text-primary"
                token={token}
                onClick={onDeleteClick}
            />
        </div>
    );
};

export default ExtraContentFormHeading;
