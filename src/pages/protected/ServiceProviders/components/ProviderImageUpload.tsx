import { UploadOutlined } from "@ant-design/icons";
import { Upload, UploadProps, theme } from "antd";
import { UploadChangeParam, UploadFile } from "antd/es/upload";
import { useParams } from "react-router-dom";

import { getImageUrl, imageExtensions } from "@/utils/file.utils";
import ImagePreview from "@/core/components/ImagePreview";
import { ProviderTypeEnum } from "@/schema/service-provider.schema";

import { ExtraInfoTrigger, ExtraInfoWrapper, StyledFormItem } from "../styles";
import { useGetServiceProviderDetails } from "../api/queries";

interface IProviderImageUploadProps {
    imageFile?: any;
    setImageFile: React.Dispatch<any>;
}

const ProviderImageUpload = ({
    imageFile,
    setImageFile,
}: IProviderImageUploadProps) => {
    const { useToken } = theme;
    const { token } = useToken();

    const { providerId } = useParams();

    const { data: serviceProviderDetails } = useGetServiceProviderDetails(
        Number(providerId) || 0
    );

    const handleImageUpload: UploadProps["onChange"] = (
        info: UploadChangeParam<UploadFile>
    ) => {
        setImageFile(info.fileList.length === 0 ? null : info.file);
    };

    const getProviderImageSrc = () => {
        if (serviceProviderDetails?.consultancy?.profilePicture) {
            return getImageUrl(
                serviceProviderDetails.consultancy.profilePicture
            );
        }

        return "does-not-exist";
    };

    return (
        <StyledFormItem>
            <ExtraInfoWrapper className="flex flex-col space-y-3">
                {providerId && (
                    <ImagePreview
                        imagePath={
                            serviceProviderDetails?.consultancy
                                ?.profilePicture || ""
                        }
                        className={
                            serviceProviderDetails?.consultancy
                                ?.consultancyType ===
                            ProviderTypeEnum.PRIVATE_TUTOR
                                ? "rounded"
                                : ""
                        }
                    />
                )}

                <Upload
                    listType="picture"
                    accept={imageExtensions}
                    fileList={imageFile ? [imageFile] : []}
                    beforeUpload={() => false}
                    onChange={handleImageUpload}
                >
                    <ExtraInfoTrigger token={token}>
                        <UploadOutlined />
                        <span>Upload Image</span>
                    </ExtraInfoTrigger>
                </Upload>
            </ExtraInfoWrapper>
        </StyledFormItem>
    );
};

export default ProviderImageUpload;
