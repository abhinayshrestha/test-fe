interface IExtraInfoContainerProps {
    title: string;
    icon: React.ReactNode;
}

const ExtraInfoContainer = ({ title, icon }: IExtraInfoContainerProps) => {
    return (
        <div className="border border-dashed border-gray-300 rounded-sm py-2 cursor-pointer">
            <div className="flex items-center justify-center space-x-2 text-primary">
                {icon}
                <span>{title}</span>
            </div>
        </div>
    );
};

export default ExtraInfoContainer;
