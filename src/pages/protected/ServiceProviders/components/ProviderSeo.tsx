import { Col, Form, Input, Row, Upload, UploadProps, theme } from "antd";
import { PlusOutlined, UploadOutlined } from "@ant-design/icons";
import { UploadChangeParam, UploadFile } from "antd/es/upload";
import { useParams } from "react-router-dom";

import { getImageUrl, imageExtensions } from "@/utils/file.utils";
import ImagePreview from "@/core/components/ImagePreview";

import {
    ExtraContentFormWrapper,
    ExtraInfoTrigger,
    ExtraInfoWrapper,
} from "../styles";
import { useGetServiceProviderDetails } from "../api/queries";

import ExtraContentFormHeading from "./ExtraContentFormHeading";

interface IProviderSeoProps {
    displayInput: boolean;
    setDisplayInput: React.Dispatch<React.SetStateAction<boolean>>;
    metaImageFile?: any;
    setMetaImageFile: React.Dispatch<any>;
}

const ProviderSeo = ({
    displayInput,
    setDisplayInput,
    metaImageFile,
    setMetaImageFile,
}: IProviderSeoProps) => {
    const { useToken } = theme;
    const { token } = useToken();

    const { providerId } = useParams();

    const { data: serviceProviderDetails } = useGetServiceProviderDetails(
        parseInt(providerId as string, 10) || 0
    );

    const handleMetaImageUpload: UploadProps["onChange"] = (
        info: UploadChangeParam<UploadFile>
    ) => {
        setMetaImageFile(info.fileList.length === 0 ? null : info.file);
    };

    const getMetaImageSrc = () => {
        if (serviceProviderDetails?.consultancySEO?.image) {
            return getImageUrl(serviceProviderDetails.consultancySEO.image);
        }

        return "does-not-exist";
    };

    return (
        <div>
            {!displayInput && (
                <ExtraInfoWrapper>
                    <ExtraInfoTrigger
                        token={token}
                        onClick={() => setDisplayInput(true)}
                    >
                        <PlusOutlined />
                        <span>SEO</span>
                    </ExtraInfoTrigger>
                </ExtraInfoWrapper>
            )}

            {displayInput && (
                <ExtraContentFormWrapper className="space-y-3">
                    <ExtraContentFormHeading
                        title="Add SEO"
                        onDeleteClick={() => setDisplayInput(false)}
                    />

                    <Row gutter={16} className="rounded-md">
                        <Col span={12}>
                            <Form.Item
                                name="title"
                                label="Meta Title"
                                rules={[
                                    {
                                        required: true,
                                        message: "Please enter meta title",
                                    },
                                ]}
                            >
                                <Input />
                            </Form.Item>
                        </Col>

                        <Col span={12}>
                            <Form.Item
                                name="seoDescription"
                                label="Meta Description"
                                rules={[
                                    {
                                        required: true,
                                        message:
                                            "Please enter meta description",
                                    },
                                ]}
                            >
                                <Input />
                            </Form.Item>
                        </Col>

                        <Col span={12}>
                            <Form.Item label="Meta Image" className="mb-0">
                                <div className="flex gap-x-2">
                                    {providerId && (
                                        <div className="shrink-0">
                                            <ImagePreview
                                                imagePath={
                                                    serviceProviderDetails
                                                        ?.consultancySEO
                                                        ?.image || ""
                                                }
                                                alt={
                                                    serviceProviderDetails
                                                        ?.consultancy?.name ||
                                                    "service provider seo image"
                                                }
                                                width={100}
                                                className="rounded-md"
                                            />
                                        </div>
                                    )}

                                    <Upload
                                        listType="picture-card"
                                        accept={imageExtensions}
                                        fileList={
                                            metaImageFile ? [metaImageFile] : []
                                        }
                                        beforeUpload={() => false}
                                        onChange={handleMetaImageUpload}
                                    >
                                        <div className="flex items-center space-x-2 text-primary">
                                            <UploadOutlined />
                                            <span>Upload</span>
                                        </div>
                                    </Upload>
                                </div>
                            </Form.Item>
                        </Col>
                    </Row>
                </ExtraContentFormWrapper>
            )}
        </div>
    );
};

export default ProviderSeo;
