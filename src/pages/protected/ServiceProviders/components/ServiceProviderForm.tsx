import { useState, useEffect } from "react";
import {
    Button,
    Col,
    Form,
    FormInstance,
    Input,
    Row,
    Select,
    Spin,
    message,
} from "antd";
import TextArea from "antd/es/input/TextArea";
import { PlusOutlined } from "@ant-design/icons";
import { useParams } from "react-router-dom";

import { providerTypes } from "@/utils/service-provider.utils";
import { ServiceProviderSocialMedia } from "@/schema/service-provider.schema";

import { StyledFormItem } from "../styles";
import { useGetServiceProviderDetails } from "../api/queries";

import ProviderImageUpload from "./ProviderImageUpload";
import ProviderSeo from "./ProviderSeo";
import ProviderContact from "./ProviderContact";
import ProviderServices from "./ProviderServices";

export const addedSocialMediaListInitialValue = [
    {
        link: "",
        socialMedia: undefined,
    },
];

interface IServiceProviderFormProps {
    form: FormInstance<any>;
    isLoading: boolean;
    providerImageFile: any;
    setProviderImageFile: React.Dispatch<any>;
    providerMetaImageFile: any;
    setProviderMetaImageFile: React.Dispatch<any>;
    onFinish: (values: any) => void;
}

const ServiceProviderForm = ({
    form,
    isLoading,
    providerImageFile,
    setProviderImageFile,
    providerMetaImageFile,
    setProviderMetaImageFile,
    onFinish,
}: IServiceProviderFormProps) => {
    const [displaySeoInput, setDisplaySeoInput] = useState(false);
    const [displayContactInput, setDisplayContactInput] = useState(false);
    const [removedSocialMediaList, setRemovedSocialMediaList] = useState<
        ServiceProviderSocialMedia[]
    >([]);
    const [isFormSubmitted, setIsFormSubmitted] = useState(false);

    const { providerId } = useParams();

    const { data: serviceProviderDetails } = useGetServiceProviderDetails(
        Number(providerId) || 0
    );

    useEffect(() => {
        if (!displaySeoInput) {
            form.setFieldsValue({
                title: "",
                seoDescription: "",
            });
            setProviderMetaImageFile(null);
        }
    }, [displaySeoInput, form, setProviderMetaImageFile]);

    useEffect(() => {
        if (!displayContactInput) {
            form.setFieldsValue({
                address: "",
                contact: "",
                email: "",
                addedSocialMediaList: addedSocialMediaListInitialValue,
            });
            setProviderImageFile(null);
        }
    }, [displayContactInput, form, setProviderImageFile]);

    useEffect(() => {
        if (providerId) {
            setDisplaySeoInput(true);
            setDisplayContactInput(true);
        }
    }, [providerId]);

    const handleFormFinish = (values: any) => {
        let errorMsg = "";

        if (!providerImageFile && !providerId) {
            errorMsg = "Please provide a provider image";
        }

        if (!providerMetaImageFile && !providerId) {
            errorMsg = "Please provide a meta image";
        }

        if (!displaySeoInput) {
            errorMsg = "Please provide SEO information";
        }

        if (!displayContactInput) {
            errorMsg = "Please provide contact information";
        }

        if (errorMsg) {
            return message.error(errorMsg);
        }

        setIsFormSubmitted(true);

        onFinish({
            ...values,
            removedSocialMediaList,
        });

        setTimeout(() => {
            setIsFormSubmitted(false);
        }, 250);
    };

    const handleSocialMediaRemove = (fieldKey: number) => {
        if (!serviceProviderDetails) {
            return;
        }

        const newRemovedSocialMedia =
            serviceProviderDetails.consultancyNetworks.find(
                (_, index) => index === fieldKey
            );

        if (newRemovedSocialMedia) {
            setRemovedSocialMediaList([
                ...removedSocialMediaList,
                {
                    id: newRemovedSocialMedia.id,
                    link: newRemovedSocialMedia.link,
                    socialMedia: newRemovedSocialMedia.socialMedia,
                },
            ]);
        }
    };

    return (
        <Form
            layout="vertical"
            className="p-4 rounded-md bg-white max-w-[800px]"
            initialValues={{
                addedSocialMediaList: addedSocialMediaListInitialValue,
            }}
            form={form}
            scrollToFirstError
            onFinish={handleFormFinish}
        >
            <Spin spinning={isLoading}>
                <Row gutter={[16, 24]}>
                    <Col span={12}>
                        <StyledFormItem
                            name="name"
                            label="Name"
                            rules={[
                                {
                                    required: true,
                                    message: "Please enter provider name",
                                },
                            ]}
                        >
                            <Input />
                        </StyledFormItem>
                    </Col>

                    <Col span={12}>
                        <StyledFormItem
                            name="consultancyType"
                            label="Provider Type"
                            rules={[
                                {
                                    required: true,
                                    message: "Please select provider type",
                                },
                            ]}
                        >
                            <Select options={providerTypes} />
                        </StyledFormItem>
                    </Col>

                    <Col span={24}>
                        <StyledFormItem
                            name="about"
                            label="Description"
                            rules={[
                                {
                                    required: true,
                                    message:
                                        "Please enter provider description",
                                },
                            ]}
                        >
                            <TextArea className="h-[100px] resize-none" />
                        </StyledFormItem>
                    </Col>

                    <Col span={12}>
                        <StyledFormItem name="frontendUrl" label="Frontend URL">
                            <Input />
                        </StyledFormItem>
                    </Col>

                    <Col span={12}>
                        <StyledFormItem name="backendUrl" label="Backend URL">
                            <Input />
                        </StyledFormItem>
                    </Col>

                    <Col span={12}>
                        <StyledFormItem name="slug" label="Slug">
                            <Input />
                        </StyledFormItem>
                    </Col>

                    <Col span={24}>
                        <ProviderImageUpload
                            imageFile={providerImageFile}
                            setImageFile={setProviderImageFile}
                        />
                    </Col>

                    <Col span={24}>
                        <ProviderSeo
                            displayInput={displaySeoInput}
                            setDisplayInput={setDisplaySeoInput}
                            metaImageFile={providerMetaImageFile}
                            setMetaImageFile={setProviderMetaImageFile}
                        />
                    </Col>

                    <Col span={24}>
                        <ProviderContact
                            displayInput={displayContactInput}
                            setDisplayInput={setDisplayContactInput}
                            onSocialMediaRemove={handleSocialMediaRemove}
                        />
                    </Col>

                    {providerId && (
                        <Col span={24}>
                            <ProviderServices
                                isProviderFormSubmitted={isFormSubmitted}
                            />
                        </Col>
                    )}

                    <Col span={24}>
                        <StyledFormItem className="mb-0">
                            <Button
                                icon={<PlusOutlined />}
                                type="primary"
                                className="w-full"
                                htmlType="submit"
                            >
                                {providerId ? "Update" : "Add"} Service Provider
                            </Button>
                        </StyledFormItem>
                    </Col>
                </Row>
            </Spin>
        </Form>
    );
};

export default ServiceProviderForm;
