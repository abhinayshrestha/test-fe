import { useState, useEffect } from "react";
import { PlusOutlined } from "@ant-design/icons";
import { Col, Row, Spin, message, theme } from "antd";
import { useParams } from "react-router-dom";

import { IUpdatedProviderService } from "@/schema/service-provider.schema";
import DataUnavailable from "@/core/components/DataUnavailable";

import {
  useGetUpdatedProviderServices,
  useGetProviderServices,
  useSetProviderServices,
} from "../api/queries";
import {
  ExtraContentFormWrapper,
  ExtraInfoTrigger,
  ExtraInfoWrapper,
} from "../styles";

import ExtraContentFormHeading from "./ExtraContentFormHeading";
import ProviderServiceCard from "./ProviderServiceCard";

interface ISubmitProviderForm {
  isProviderFormSubmitted: boolean;
}

const ProviderServices = ({ isProviderFormSubmitted }: ISubmitProviderForm) => {
  const [showServicesForm, setShowServicesForm] = useState(false);
  const [displayedUpdatedServices, setDisplayedUpdatedServices] = useState<
    IUpdatedProviderService[]
  >([]);

  const { useToken } = theme;
  const { token } = useToken();
  const { providerId } = useParams();

  const { data: providerServices, isLoading: isLoadingProviderServices } =
    useGetProviderServices(Number(providerId) || 0);

  const {
    data: updatedProviderServices,
    isLoading: isLoadingUpdatedProviderServices,
  } = useGetUpdatedProviderServices(Number(providerId));

  const { mutate: setProviderServices, isLoading: isSettingProviderServices } =
    useSetProviderServices();

  useEffect(() => {
    if (isProviderFormSubmitted && showServicesForm && providerId) {
      setProviderServices(
        displayedUpdatedServices.map((service) => {
          return {
            name: service.serviceName,
            rate: service.rate,
            serviceReferenceId: service.id,
            consultancy: {
              id: Number(providerId),
            },
          };
        }),
        {
          onSuccess: () => {
            message.success("Provider services have been successfully updated");
          },
        }
      );
    }
  }, [
    showServicesForm,
    providerId,
    isProviderFormSubmitted,
    setProviderServices,
    displayedUpdatedServices,
  ]);

  useEffect(() => {
    if (updatedProviderServices) {
      setDisplayedUpdatedServices(updatedProviderServices);
    }
  }, [updatedProviderServices]);

  const handleUpdatedServiceDeletion = (serviceId: number) => {
    if (displayedUpdatedServices.length === 1) {
      setShowServicesForm(false);
    }

    setDisplayedUpdatedServices(
      displayedUpdatedServices.filter((service) => service.id !== serviceId)
    );
  };

  return (
    <Row gutter={[12, 12]}>
      <Col span={24}>
        <ExtraContentFormWrapper className="space-y-3">
          <div className="flex items-center justify-between">
            <h4 className="font-semibold text-primary">Existing Services</h4>
          </div>

          <Spin spinning={isLoadingProviderServices}>
            {!providerServices?.length && (
              <DataUnavailable title="No existing services" />
            )}

            <div className="space-y-3">
              {providerServices?.map((service) => {
                return (
                  <ProviderServiceCard
                    {...service}
                    isEnabled={service.active}
                    serviceReferenceId={service.serviceReferenceId}
                    key={service.id}
                  />
                );
              })}
            </div>
          </Spin>
        </ExtraContentFormWrapper>
      </Col>

      <Col span={24}>
        {!showServicesForm && (
          <ExtraInfoWrapper>
            <ExtraInfoTrigger
              token={token}
              onClick={() => setShowServicesForm(true)}
            >
              <PlusOutlined />
              <span>Update Services</span>
            </ExtraInfoTrigger>
          </ExtraInfoWrapper>
        )}

        {showServicesForm && (
          <ExtraContentFormWrapper className="space-y-3">
            <ExtraContentFormHeading
              title="Updated Provider Services"
              onDeleteClick={() => {
                setShowServicesForm(false);
              }}
            />

            <div className="space-y-3">
              {displayedUpdatedServices?.map((service) => {
                return (
                  <ProviderServiceCard
                    {...service}
                    name={service.serviceName}
                    key={service.id}
                    onDelete={handleUpdatedServiceDeletion}
                  />
                );
              })}
            </div>
          </ExtraContentFormWrapper>
        )}
      </Col>
    </Row>
  );
};

export default ProviderServices;
