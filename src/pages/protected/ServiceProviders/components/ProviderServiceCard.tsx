import { Switch, theme } from "antd";
import { FaRegTrashAlt } from "react-icons/fa";

import { StyledDeleteButton } from "../styles";

interface IProviderServiceCardProps {
    id: number;
    name: string;
    rate: string;
    isEnabled?: boolean;
    serviceReferenceId?: number;
    onDelete?: (serviceId: number) => void;
}

const ProviderServiceCard = ({
    id,
    name,
    rate,
    isEnabled,
    serviceReferenceId,
    onDelete,
}: IProviderServiceCardProps) => {
    const { useToken } = theme;
    const { token } = useToken();

    const handleDeleteClick = () => {
        onDelete?.(id);
    };

    return (
        <div className="relative flex items-center justify-between flex-wrap gap-3 bg-accent p-3 rounded-md max-w-[500px]">
            <div className="flex flex-col text-gray-800">
                <h5 className="font-semibold">{name}</h5>
                <h5>Rs. {rate}</h5>
            </div>

            <Switch
                size="small"
                checked={isEnabled}
                className="pointer-events-none"
            />

            {!serviceReferenceId && (
                <div className="absolute top-1/2 -right-12 -translate-y-1/2">
                    <StyledDeleteButton
                        token={token}
                        icon={<FaRegTrashAlt />}
                        type="text"
                        onClick={handleDeleteClick}
                    />
                </div>
            )}
        </div>
    );
};

export default ProviderServiceCard;
