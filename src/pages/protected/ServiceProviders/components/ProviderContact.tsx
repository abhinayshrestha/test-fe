import { Button, Col, Form, Input, Row, Select, theme } from "antd";
import { PlusOutlined } from "@ant-design/icons";
import { FaRegTrashAlt } from "react-icons/fa";

import { socialMediaTypes } from "@/utils/service-provider.utils";
import {
    emailValidator,
    generalContactValidator,
} from "@/utils/validation.utils";

import {
    ExtraContentFormWrapper,
    ExtraInfoTrigger,
    ExtraInfoWrapper,
    StyledDeleteButton,
    StyledFormItem,
} from "../styles";

import ExtraContentFormHeading from "./ExtraContentFormHeading";

interface IProviderContactProps {
    displayInput: boolean;
    setDisplayInput: React.Dispatch<React.SetStateAction<boolean>>;
    onSocialMediaRemove: (fieldKey: number) => void;
}

const ProviderContact = ({
    displayInput,
    setDisplayInput,
    onSocialMediaRemove,
}: IProviderContactProps) => {
    const { useToken } = theme;
    const { token } = useToken();

    return (
        <div>
            {!displayInput && (
                <StyledFormItem>
                    <ExtraInfoWrapper>
                        <ExtraInfoTrigger
                            token={token}
                            onClick={() => setDisplayInput(true)}
                        >
                            <PlusOutlined />
                            <span>Add Contact Information</span>
                        </ExtraInfoTrigger>
                    </ExtraInfoWrapper>
                </StyledFormItem>
            )}

            {displayInput && (
                <ExtraContentFormWrapper className="space-y-3">
                    <ExtraContentFormHeading
                        title="Add Contact Information"
                        onDeleteClick={() => setDisplayInput(false)}
                    />

                    <Row gutter={16}>
                        <Col span={12}>
                            <Form.Item
                                name="address"
                                label="Location"
                                rules={[
                                    {
                                        required: true,
                                        message:
                                            "Please enter provider address",
                                    },
                                ]}
                            >
                                <Input />
                            </Form.Item>
                        </Col>

                        <Col span={12}>
                            <Form.Item
                                name="contact"
                                label="Contact Number"
                                required
                                rules={[
                                    () => ({
                                        validator(_, value) {
                                            if (!value) {
                                                return Promise.reject(
                                                    new Error(
                                                        "Please enter attendee's contact number"
                                                    )
                                                );
                                            }

                                            return generalContactValidator(
                                                value
                                            );
                                        },
                                    }),
                                ]}
                            >
                                <Input />
                            </Form.Item>
                        </Col>

                        <Col span={12}>
                            <Form.Item
                                name="email"
                                label="Email Address"
                                required
                                rules={[
                                    () => ({
                                        validator(_, value) {
                                            return emailValidator(value);
                                        },
                                    }),
                                ]}
                            >
                                <Input />
                            </Form.Item>
                        </Col>

                        <Col span={12}>
                            <Form.Item name="longitude" label="Longitude">
                                <Input />
                            </Form.Item>
                        </Col>

                        <Col span={12}>
                            <Form.Item name="latitude" label="Latitude">
                                <Input />
                            </Form.Item>
                        </Col>
                    </Row>

                    <Form.List name="addedSocialMediaList">
                        {(fields, { add, remove }) => {
                            return (
                                <div className="border rounded-md p-4">
                                    <h4 className="font-semibold text-primary mb-3">
                                        Social Networks
                                    </h4>

                                    <div>
                                        {fields.map(
                                            (
                                                { key, name, ...restField },
                                                index
                                            ) => {
                                                return (
                                                    <Row key={key} gutter={16}>
                                                        <Form.Item
                                                            name={[name, "id"]}
                                                            hidden
                                                        >
                                                            <Input />
                                                        </Form.Item>

                                                        <Col span={10}>
                                                            <Form.Item
                                                                name={[
                                                                    name,
                                                                    "link",
                                                                ]}
                                                                label="Social Media Url"
                                                                rules={[
                                                                    {
                                                                        required:
                                                                            true,
                                                                        message:
                                                                            "Please enter social media url",
                                                                    },
                                                                ]}
                                                            >
                                                                <Input />
                                                            </Form.Item>
                                                        </Col>

                                                        <Col span={10}>
                                                            <Form.Item
                                                                name={[
                                                                    name,
                                                                    "socialMedia",
                                                                ]}
                                                                label="Social Media Type"
                                                                rules={[
                                                                    {
                                                                        required:
                                                                            true,
                                                                        message:
                                                                            "Please select social media type",
                                                                    },
                                                                ]}
                                                            >
                                                                <Select
                                                                    options={
                                                                        socialMediaTypes
                                                                    }
                                                                />
                                                            </Form.Item>
                                                        </Col>

                                                        {index > 0 && (
                                                            <Col
                                                                span={4}
                                                                className="flex items-center"
                                                            >
                                                                <StyledDeleteButton
                                                                    icon={
                                                                        <FaRegTrashAlt />
                                                                    }
                                                                    type="text"
                                                                    token={
                                                                        token
                                                                    }
                                                                    onClick={() => {
                                                                        remove(
                                                                            name
                                                                        );

                                                                        onSocialMediaRemove(
                                                                            key
                                                                        );
                                                                    }}
                                                                />
                                                            </Col>
                                                        )}
                                                    </Row>
                                                );
                                            }
                                        )}
                                    </div>

                                    <Button
                                        icon={<PlusOutlined />}
                                        className="text-primary "
                                        onClick={() => add()}
                                    >
                                        Add Social Link
                                    </Button>
                                </div>
                            );
                        }}
                    </Form.List>
                </ExtraContentFormWrapper>
            )}
        </div>
    );
};

export default ProviderContact;
