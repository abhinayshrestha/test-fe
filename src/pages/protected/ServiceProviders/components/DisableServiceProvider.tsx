import { useState } from "react";
import { Button, message } from "antd";
import { useNavigate } from "react-router-dom";

import ConfirmationModal from "@/core/components/ConfirmationModal";

import { useDisableServiceProvider } from "../api/queries";

interface IDisableServiceProviderProps {
  providerId: number;
}

const DisableServiceProvider = ({
  providerId,
}: IDisableServiceProviderProps) => {
  const [isOpen, setIsOpen] = useState(false);

  const navigate = useNavigate();

  const {
    mutate: disableServiceProvider,
    isLoading: isDisablingServiceProvider,
  } = useDisableServiceProvider();

  const handleConfirmation = () => {
    disableServiceProvider(providerId, {
      onSuccess: () => {
        setIsOpen(false);
        navigate(-1);
        message.success("Service provider has been disable successfully");
      },
    });
  };

  return (
    <>
      <ConfirmationModal
        title="Disable Service Provider"
        description="Are you sure you want to disable this service provider ? Please confirm to continue."
        isLoading={isDisablingServiceProvider}
        isOpen={isOpen}
        setIsOpen={setIsOpen}
        onConfirm={handleConfirmation}
      />

      <Button type="primary" onClick={() => setIsOpen(true)}>
        Disable Provider
      </Button>
    </>
  );
};

export default DisableServiceProvider;
