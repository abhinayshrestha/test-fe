import { useState, useEffect, useRef } from "react";
import { Form, message } from "antd";
import { useParams, useNavigate } from "react-router-dom";

import { protectedRoutePaths } from "@/router";
import { IServiceProviderAddRequest } from "@/schema/service-provider.schema";
import { useRevalidatePath } from "@/core/queries/revalidation/queries";
import { convertStrIntoSlug } from "@/utils/string.utils";

import {
  useGetServiceProviderDetails,
  useAddServiceProvider,
  useUpdateServiceProvider,
  useSetProviderServices,
} from "../api/queries";

import ServiceProviderForm from "../components/ServiceProviderForm";
import DisableServiceProvider from "../components/DisableServiceProvider";

const SetServiceProvider = () => {
  const [providerImageFile, setProviderImageFile] = useState<any>();
  const [providerMetaImageFile, setProviderMetaImageFile] = useState<any>();

  const [serviceProviderForm] = Form.useForm();
  const { providerId } = useParams();
  const navigate = useNavigate();
  /* provider details page path revalidation /service-providers/{providerId}/{providerNameConvertedIntoSlug} requires its previous name in case the name itself gets updated */
  const providerPrevName = useRef<string | null>(null);

  const { mutate: addServiceProvider, isLoading: isAddingServiceProvider } =
    useAddServiceProvider();

  const {
    mutate: updateServiceProvider,
    isLoading: isUpdatingServiceProvider,
  } = useUpdateServiceProvider();

  const {
    data: serviceProviderDetails,
    isLoading: isLoadingServiceProviderDetails,
  } = useGetServiceProviderDetails(Number(providerId) || 0);

  const { mutate: addProviderServices, isLoading: isSettingProviderServices } =
    useSetProviderServices();

  const { mutate: revalidatePath } = useRevalidatePath();

  useEffect(() => {
    if (serviceProviderDetails) {
      const {
        consultancy: { id, name, consultancyType, slug },
        consultancyInfo: {
          address,
          contact,
          email,
          about,
          frontendUrl,
          backendUrl,
          longitude,
          latitude,
        },
        consultancySEO,
        consultancyNetworks,
      } = serviceProviderDetails;

      serviceProviderForm.setFieldsValue({
        name,
        consultancyType,
        about,
        frontendUrl,
        backendUrl,
        slug,
        address,
        contact,
        email,
        latitude,
        longitude,
      });
      providerPrevName.current = name;

      if (consultancySEO) {
        const {
          title: seoTitle,
          description: seoDescription,
          image: seoImage,
        } = consultancySEO;

        serviceProviderForm.setFieldsValue({
          title: seoTitle,
          seoDescription,
        });
      }

      if (consultancyNetworks.length > 0) {
        serviceProviderForm.setFieldsValue({
          addedSocialMediaList: consultancyNetworks.map((network) => {
            return {
              id: network.id,
              link: network.link,
              socialMedia: network.socialMedia,
            };
          }),
        });
      }
    }
  }, [serviceProviderDetails, serviceProviderForm]);

  const handleSuccess = () => {
    message.success(
      `Service provider has been successfully ${providerId ? "updated" : "added"}`
    );
    serviceProviderForm.resetFields();

    revalidatePath("/");

    if (providerId && providerPrevName.current) {
      revalidatePath(
        `/service-providers/${providerId}/${convertStrIntoSlug(providerPrevName.current)}`
      );
    }

    navigate(protectedRoutePaths.serviceProviders);
  };

  const handleFormFinish = (values: IServiceProviderAddRequest) => {
    let payload: any = {
      name: values.name,
      consultancyType: values.consultancyType,
      about: values.about,
      frontendUrl: values.frontendUrl,
      backendUrl: values.backendUrl,
      consultancyImage: providerImageFile,
      slug: values.slug,
      latitude: values.latitude,
      longitude: values.longitude,
      // contact information
      address: values.address,
      contact: values.contact,
      email: values.email,
      // social media information
      addedSocialMediaList: values.addedSocialMediaList,
      removedSocialMediaList: values.removedSocialMediaList,
      // seo information
      title: values.title,
      seoDescription: values.seoDescription,
      seoImage: providerMetaImageFile,
    };

    if (providerId) {
      payload = {
        ...payload,
        id: Number(providerId),
        consultancy: {
          id: Number(providerId),
        },
      };

      if (!providerImageFile) {
        delete payload.consultancyImage;
      }

      if (!providerMetaImageFile) {
        delete payload.seoImage;
      }
    }

    if (providerId) {
      updateServiceProvider(payload, {
        onSuccess: () => {
          if (values.updatedProviderServices) {
            addProviderServices(values.updatedProviderServices, {
              onSuccess: () => {
                message.success(
                  "Provider services have been successfully updated"
                );
              },
            });
          }

          handleSuccess();
        },
      });
    } else {
      addServiceProvider(payload, {
        onSuccess: () => {
          handleSuccess();
        },
      });
    }
  };

  return (
    <div className="space-y-4">
      <div className="flex items-center justify-between">
        <h3 className="font-semibold text-base">
          {providerId ? "Update Service Provider" : "Add New Service Provider"}
        </h3>

        {providerId && (
          <DisableServiceProvider providerId={Number(providerId)} />
        )}
      </div>

      <ServiceProviderForm
        form={serviceProviderForm}
        isLoading={
          isAddingServiceProvider ||
          isUpdatingServiceProvider ||
          isLoadingServiceProviderDetails ||
          isSettingProviderServices
        }
        providerImageFile={providerImageFile}
        setProviderImageFile={setProviderImageFile}
        providerMetaImageFile={providerMetaImageFile}
        setProviderMetaImageFile={setProviderMetaImageFile}
        onFinish={handleFormFinish}
      />
    </div>
  );
};

export default SetServiceProvider;
