import { useState, useEffect } from "react";
import { ColumnsType } from "antd/es/table";
import { useNavigate } from "react-router-dom";

import { useTitle } from "@/providers/TitleProvider";
import GenericTable from "@/core/components/GenericTable";
import { protectedRoutePaths } from "@/router";
import { IServiceProvider } from "@/schema/service-provider.schema";
import ImagePreview from "@/core/components/ImagePreview";
import SearchBar from "@/core/components/SearchBar";

import { useGetServiceProviders } from "./api/queries";

const ServiceProviders = () => {
    const [searchQuery, setSearchQuery] = useState("");

    const navigate = useNavigate();

    const { setTitle } = useTitle();

    const { data: serviceProviders, isLoading: isLoadingServiceProviders } =
        useGetServiceProviders();

    useEffect(() => {
        setTitle("Service Providers");

        return () => {
            setTitle("");
        };
    }, [setTitle]);

    const columns: ColumnsType<IServiceProvider> = [
        {
            title: "Title",
            key: "name",
            dataIndex: "name",
            width: 300,
            fixed: "left",
            render: (name) => {
                return <span className="capitalize">{name}</span>;
            },
        },
        {
            title: "Description",
            key: "about",
            dataIndex: "about",
            width: 500,
            render: (about) => {
                const shortenedAbout =
                    about?.split(" ").slice(0, 20).join(" ") || "";
                const isAboutShortened = shortenedAbout.length < about?.length;

                return (
                    <p>
                        {`${shortenedAbout}${isAboutShortened ? "..." : ""}` ||
                            "-"}
                    </p>
                );
            },
        },
        {
            title: "Image",
            key: "profile_picture",
            dataIndex: "profile_picture",
            render: (profile_picture, record: IServiceProvider) => {
                return (
                    <ImagePreview
                        imagePath={profile_picture}
                        alt={record.name}
                        className="rounded mt-[5px]"
                        placeholderClassName="mt-[5px]"
                        width={70}
                    />
                );
            },
        },
        {
            title: "Created Date",
            key: "createdDate",
            dataIndex: "createdDate",
            render: (createdDate) => {
                return <span>{createdDate || "-"}</span>;
            },
        },
    ];

    const handleRowClick = (record: IServiceProvider) => {
        navigate(`/service-providers/${record.id}/update`);
    };

    return (
        <GenericTable
            dataSource={serviceProviders
                ?.filter((provider) =>
                    provider.name
                        .toLowerCase()
                        .trim()
                        .includes(searchQuery.toLowerCase().trim())
                )
                ?.map((provider) => {
                    return {
                        ...provider,
                        key: provider.name,
                    };
                })}
            columns={columns}
            hasAddBtn
            addBtnText="Add New Provider"
            addBtnAction={() =>
                navigate(protectedRoutePaths.createServiceProvider)
            }
            filterInfo={[]}
            isLoading={isLoadingServiceProviders}
            onRowClick={(record: IServiceProvider) => handleRowClick(record)}
            searchBar={<SearchBar onInputChange={setSearchQuery} />}
        />
    );
};

export default ServiceProviders;
