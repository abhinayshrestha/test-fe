import { useMutation, useQuery } from "react-query";

import { protectedApiList } from "@/api";
import performApiAction from "@/utils/http/perform-api-action";
import {
  IExistingProviderService,
  IProviderServiceSetRequestItem,
  IServiceProvider,
  IServiceProviderAddRequest,
  IServiceProviderDetails,
  IServiceProviderUpdateRequest,
  IUpdatedProviderService,
} from "@/schema/service-provider.schema";

const {
  getServiceProviders,
  addServiceProvider,
  getServiceProviderDetails,
  updateServiceProvider,
  getProviderServices,
  setProviderServices,
  getUpdatedProviderServices,
  disableServiceProvider,
} = protectedApiList.serviceProviders;

export const useGetServiceProviders = () => {
  return useQuery({
    queryKey: [getServiceProviders.queryKeyName],
    queryFn: () => {
      return performApiAction<IServiceProvider[]>(getServiceProviders, {
        disableSuccessToast: true,
      });
    },
  });
};

export const useAddServiceProvider = () => {
  return useMutation({
    mutationFn: (requestData: IServiceProviderAddRequest) => {
      return performApiAction(addServiceProvider, {
        requestData,
        disableSuccessToast: true,
      });
    },
  });
};

export const useUpdateServiceProvider = () => {
  return useMutation({
    mutationFn: (requestData: IServiceProviderUpdateRequest) => {
      return performApiAction(updateServiceProvider, {
        requestData,
        disableSuccessToast: true,
      });
    },
  });
};

export const useGetServiceProviderDetails = (providerId: number) => {
  return useQuery({
    queryKey: [getServiceProviderDetails.queryKeyName, providerId],
    queryFn: () => {
      return performApiAction<IServiceProviderDetails>(
        getServiceProviderDetails,
        {
          pathVariables: {
            providerId,
          },
          disableSuccessToast: true,
        }
      );
    },
    enabled: !!providerId,
    staleTime: 0,
  });
};

export const useGetProviderServices = (providerId: number) => {
  return useQuery({
    queryKey: [getProviderServices.queryKeyName, providerId],
    queryFn: () => {
      return performApiAction<IExistingProviderService[]>(getProviderServices, {
        pathVariables: {
          providerId,
        },
        disableSuccessToast: true,
      });
    },
    enabled: !!providerId,
    staleTime: 0,
  });
};

export const useSetProviderServices = () => {
  return useMutation({
    mutationFn: (requestData: IProviderServiceSetRequestItem[]) => {
      return performApiAction(setProviderServices, {
        requestData,
      });
    },
  });
};

export const useGetUpdatedProviderServices = (centreId: number) => {
  return useQuery({
    queryKey: [getUpdatedProviderServices.queryKeyName, centreId],
    queryFn: () => {
      return performApiAction<IUpdatedProviderService[]>(
        getUpdatedProviderServices,
        {
          requestData: {
            centreId,
          },
          disableSuccessToast: true,
        }
      );
    },
    enabled: !!centreId,
  });
};

export const useDisableServiceProvider = () => {
  return useMutation({
    mutationFn: (providerId: number) => {
      return performApiAction(disableServiceProvider, {
        pathVariables: {
          providerId,
        },
        disableSuccessToast: true,
      });
    },
  });
};
