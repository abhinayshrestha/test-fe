import {
    IApiDetails,
    RequestAuthType,
    RequestBodyType,
    RequestMethodEnum,
} from "@/schema/http.schema";

const prefix = "providers";

const serviceProviders: { [key: string]: IApiDetails } = {
    getServiceProviders: {
        controllerName: prefix,
        requestMethod: RequestMethodEnum.GET,
        queryKeyName: "GET_SERVICE_PROVIDERS",
        requestAuthType: RequestAuthType.NOAUTH,
    },
    addServiceProvider: {
        controllerName: prefix,
        requestMethod: RequestMethodEnum.POST,
        queryKeyName: "ADD_SERVICE_PROVIDER",
        requestBodyType: RequestBodyType.FORMDATA,
        requestAuthType: RequestAuthType.NOAUTH,
    },
    updateServiceProvider: {
        controllerName: prefix,
        requestMethod: RequestMethodEnum.PUT,
        queryKeyName: "UPDATE_SERVICE_PROVIDER",
        requestBodyType: RequestBodyType.FORMDATA,
        requestAuthType: RequestAuthType.NOAUTH,
    },
    getServiceProviderDetails: {
        controllerName: `${prefix}/{providerId}`,
        requestMethod: RequestMethodEnum.GET,
        queryKeyName: "GET_SERVICE_PROVIDER_DETAILS",
        requestAuthType: RequestAuthType.NOAUTH,
    },
    getProviderServices: {
        controllerName: `${prefix}/provider-services/{providerId}`,
        requestMethod: RequestMethodEnum.GET,
        queryKeyName: "GET_PROVIDER_SERVICES",
        requestAuthType: RequestAuthType.NOAUTH,
    },
    setProviderServices: {
        controllerName: `${prefix}/services`,
        requestMethod: RequestMethodEnum.POST,
        queryKeyName: "SET_PROVIDER_SERVICES",
        requestAuthType: RequestAuthType.NOAUTH,
    },
    getUpdatedProviderServices: {
        controllerName: `test_centre/services`,
        requestMethod: RequestMethodEnum.POST,
        queryKeyName: "GET_UPDATED_PROVIDER_SERVICES",
        requestAuthType: RequestAuthType.NOAUTH,
    },
    disableServiceProvider: {
        controllerName: `${prefix}/{providerId}`,
        requestMethod: RequestMethodEnum.DELETE,
        queryKeyName: "DISABLE_SERVICE_PROVIDER",
        requestAuthType: RequestAuthType.NOAUTH,
    },
};

export default serviceProviders;
