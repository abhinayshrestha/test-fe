import { useState } from "react";
import { Checkbox, Form, Input, Modal, Image, Col, Row, Button } from "antd";
import styled, { keyframes } from "styled-components";

import world from "@/assets/vectors/world.svg";

const slideInFromLeft = keyframes`
  from {
    transform: translateX(-25%);
    opacity: 0;
  }
  to {
    transform: translateX(0);
    opacity: 1;
  }
`;

const slideInFromRight = keyframes`
  from {
    transform: translateX(25%);
    opacity: 0;
  }
  to {
    transform: translateX(0);
    opacity: 1;
  }
`;

const HeaderDiv = styled.div`
  animation: ${slideInFromLeft} 1s ease;
`;

const LeftDiv = styled.div`
  animation: ${slideInFromLeft} 1s ease;
`;

const RightDiv = styled.div`
  animation: ${slideInFromRight} 1s ease;
`;

interface ApplyModalProps {
  isOpen: boolean;
  onClose: () => void;
}

const FormModal: React.FC<ApplyModalProps> = ({ isOpen, onClose }) => {
  const [form] = Form.useForm();
  const [confirmLoading, setConfirmLoading] = useState(false);

  const handleOk = () => {
    form
      .validateFields()
      .then((values) => {
        setConfirmLoading(true);
        console.log("Form values:", values);
        setTimeout(() => {
          setConfirmLoading(false);
          form.resetFields();
          onClose();
        }, 2000);
      })

      .catch((errorInfo) => {
        console.log("Validation failed:", errorInfo);
      });
  };

  const handleCancel = () => {
    onClose();
    form.resetFields();
  };

  return (
    <Modal
      open={isOpen}
      confirmLoading={confirmLoading}
      onCancel={handleCancel}
      width={1000}
      footer={null}
    >
      <HeaderDiv>
        <h2 className="text-center text-2xl text-blue-400 mt-6 font-bold">
          Apply to Your Dream Country
        </h2>
      </HeaderDiv>

      <div className="flex items-center justify-center">
        <LeftDiv>
          <div className="flex flex-col items-center p-8  rounded-md gap-6 ">
            <Form form={form} layout="vertical">
              <div className="400:flex  400:space-x-4">
                <div className="400:w-1/2">
                  <Form.Item
                    name="name"
                    label="First Name :"
                    rules={[
                      {
                        required: true,
                        message: "Please enter your first name",
                      },
                    ]}
                  >
                    <Input />
                  </Form.Item>
                </div>
                <div className="400:w-1/2">
                  <Form.Item
                    name="name"
                    label="Last Name :"
                    rules={[
                      {
                        required: true,
                        message: "Please enter your last name",
                      },
                    ]}
                  >
                    <Input />
                  </Form.Item>
                </div>
              </div>

              <div className="400:flex  400:space-x-4">
              <div className="400:w-1/2">
                  <Form.Item
                    name="address"
                    label="Address :"
                    rules={[
                      { required: true, message: "Please enter your address" },
                    ]}
                  >
                    <Input />
                  </Form.Item>
                </div>

                <div className="400:w-1/2">
                  <Form.Item
                    name="contact"
                    label="Contact :"
                    rules={[
                      {
                        required: true,
                        message: "Please enter your contact number",
                      },
                    ]}
                  >
                    <Input />
                  </Form.Item>
                </div>
              </div>
              <Row>
                <Col span={24}>
                  <Form.Item
                    name="email"
                    label="Email :"
                    rules={[
                      {
                        required: true,
                        type: "email",
                        message: "Please enter a valid email address",
                      },
                    ]}
                  >
                    <Input />
                  </Form.Item>
                </Col>

                <Col span={24}>
                  <Form.Item name="acceptTerms" valuePropName="checked">
                    <Checkbox>I accept the terms and conditions</Checkbox>
                  </Form.Item>
                </Col>

                <Col span={24}>
                  <Button
                    className=" bg-red-600 text-white w-full cursor-pointer hover:bg-red-700"
                    style={{ border: "none" }}
                  >
                    Apply
                  </Button>
                </Col>
              </Row>
            </Form>
          </div>
        </LeftDiv>

        <RightDiv>
          <Image
            src={world}
            alt="world"
            preview={false}
            height={300}
            className="hidden 700:block ml-5 p-2"
          />
        </RightDiv>
      </div>
    </Modal>
  );
};

export default FormModal;
