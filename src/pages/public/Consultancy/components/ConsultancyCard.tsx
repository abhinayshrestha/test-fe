import { Button } from "antd";
import {
  EnvironmentFilled,
  ArrowRightOutlined,
  CheckOutlined,
  PlusOutlined,
} from "@ant-design/icons";

import brihmingam from "@/assets/images/universities/brihmingam.jpg";
import centennial from "@/assets/images/universities/centennial.jpg";
import deakin from "@/assets/images/universities/deakin.jpg";
import melbourne from "@/assets/images/universities/melbourne.jpg";
import myhc from "@/assets/images/universities/myhc.jpg";
import rmit from "@/assets/images/universities/rmit.png";
import tafe from "@/assets/images/universities/tafe.jpg";
import trobe from "@/assets/images/universities/trobe.jpg";
import { useEffect, useState } from "react";

interface ConsultancyProps {
  id: number;
  name: string;
  image: string; 
  location: string;
}

interface ConsultancyCardProps {
  onApplyClick: () => void;
}

const consultancyData: ConsultancyProps[] = [
  {
    id: 1,
    name: "Brihmingam",
    image: brihmingam,
    location: "New Buspark, Kathmandu",
  },
  {
    id: 2,
    name: "Centennial",
    image: centennial,
    location: "New Buspark, Kathmandu",
  },
  { id: 3, name: "Deakin", image: deakin, location: "New Buspark, Kathmandu" },
  {
    id: 4,
    name: "Melbourne",
    image: melbourne,
    location: "New Buspark, Kathmandu",
  },
  { id: 5, name: "MyHC", image: myhc, location: "New Buspark, Kathmandu" },
  { id: 6, name: "RMIT", image: rmit, location: "New Buspark, Kathmandu" },
  { id: 7, name: "TAFE", image: tafe, location: "New Buspark, Kathmandu" },
  { id: 8, name: "Trobe", image: trobe, location: "New Buspark, Kathmandu" },
];

const ConsultancyCard: React.FC<ConsultancyCardProps> = ({ onApplyClick }) => {
  const [consultancyList, setConsultancyList] =
    useState<ConsultancyProps[]>(consultancyData);

  const [filters, setFilters] = useState<{
    name: string[];
  }>({
    name: [],
  });

  const handleApplyNow = () => {
    onApplyClick();
  };

  useEffect(() => {
    const filteredConsultancy = consultancyData.filter((consultancy) => {
      return (
        filters.name.length === 0 || filters.name.includes(consultancy.name)
      );
    });
    setConsultancyList(filteredConsultancy);
  }, [filters]);

  const applyFilter = (consultancy: string) => {
    console.log(consultancy);
    setFilters((prevState) => ({
      ...prevState,
      name: prevState.name.includes(consultancy)
        ? prevState.name.filter((item) => item !== consultancy)
        : [...prevState.name, consultancy],
    }));
  };

  return (
    <div className="bg-white">
      <div className="container mx-auto px-4 py-8">
        <p className="text-center text-3xl  my-16">
          Explore Consultancy services to optimize your Study Abroad Journey!
        </p>

        <div className="grid grid-cols-1 sm:grid-cols-4 sm:gap-8 ">
          {/* Left side div */}

          <div
            className="border p-4 rounded-lg shadow-md bg-white   sm:sticky sm:top-20 mb-4"
            style={{ height: "fit-content" }}
          >
            <div className="">
              <h3 className="text-sm font-bold mb-2 border-b py-3">Filters</h3>
              <div>
                <h3 className="text-sm font-semibold mb-2 ">Consultancy</h3>

                <div className="mb-4 border-b py-2 flex flex-wrap">
                  {consultancyData.map((consultancy, index) => (
                    <span
                      className={`rounded-lg mr-1 mb-2 border p-1 cursor-pointer  ${
                        filters.name.includes(consultancy.name) ? "" : "group"
                      }`}
                      key={index}
                      onClick={() => applyFilter(consultancy.name)}
                    >
                      <span>
                        {filters.name.includes(consultancy.name) ? (
                          <CheckOutlined className="bg-green-400  p-1 text-white rounded-full mr-2 group-hover:bg-blue-800" />
                        ) : (
                          <PlusOutlined className="bg-blue-400  p-1 text-white rounded-full mr-2 group-hover:bg-blue-800" />
                        )}
                      </span>

                      {consultancy.name}
                    </span>
                  ))}
                </div>
              </div>
            </div>
          </div>

          {/* Right side div */}
          <div
            className=" col-span-3 border p-4 rounded-lg shadow-md"
            style={{ height: "fit-content" }}
          >
            <div className="grid grid-cols-1 gap-8 xl:grid-cols-4 lg:grid-cols-3 md:grid-cols-2 sm:grid-cols-2 ">
              {consultancyList.map((consultancy, index) => (
                <div
                  className=" rounded-lg  border-2 hover:shadow-md"
                  key={index}
                >
                  <img
                    src={consultancy.image}
                    alt={consultancy.name}
                    className="w-full h-auto p-2 "
                  />

                  <div className="p-4">
                    <h2 className="consultancy-name text-lg font-semibold mb-2">
                      {consultancy.name}
                    </h2>

                    <p className="text-sm mb-4">
                      <EnvironmentFilled className="mr-1" />
                      {consultancy.location}
                    </p>

                    <span
                      className="hover:bg-gray-200 p-2  transition duration-200 cursor-pointer"
                      onClick={handleApplyNow}
                    >
                      Apply Now
                      {<ArrowRightOutlined className="ml-2 align-middle" />}
                    </span>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ConsultancyCard;
