import { useState } from "react";
import { Layout } from "antd";
import { Content } from "antd/es/layout/layout";

import ConsultancyCard from "./components/ConsultancyCard";
import FormModal from "./components/FormModal";

const Consultancy = () => {

  const [isModalOpen, setIsModalOpen] = useState(false);

  const handleApplyClick = () => {
    setIsModalOpen(true);
  };

  const handleCloseModal = () => {
    setIsModalOpen(false);
  };

  return (
    <Layout>
      <Content>
        <ConsultancyCard onApplyClick={handleApplyClick} />
        <FormModal isOpen={isModalOpen} onClose={handleCloseModal} />
      </Content>
    </Layout>
  );
};

export default Consultancy;
