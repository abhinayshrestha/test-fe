import { useMutation } from "react-query";

import performApiAction from "@/utils/http/perform-api-action";
import { publicApiList } from "@/api";
import { ISignInRequest, ISignInResponse } from "@/schema/auth.schema";

const { signUserIn } = publicApiList.auth;

export const useSignUserIn = () => {
  return useMutation({
    mutationFn: (requestData: ISignInRequest) => {
      const { email, password } = requestData;

      return performApiAction<ISignInResponse>(signUserIn, {
        params: {
          grant_type: "password",
          username: email,
          password,
        },
        disableSuccessToast: true,
      });
    },
  });
};
