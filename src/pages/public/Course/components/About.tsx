import { Link } from "react-router-dom";

const About = () => {
  const fetchedText: string =
    "The Computer Science Department offers a comprehensive Master of Computer Applications (MCA) program, catering to students' diverse interests and career aspirations in the field of computer science and technology. The MCA program equips students with a deep understanding of computer science principles, programming languages, software development methodologies, and advanced computing technologies.";

  const escapeSingleQuotes = (text: string) => {
    return text.replace(/'/g, "&apos;");
  };

  return (
    <div className=" space-y-3 p-6 rounded-lg shadow-sm border-2">
      <h2 className="text-2xl text-primary font-semibold">About</h2>
      <p>{escapeSingleQuotes(fetchedText)}</p>
      <p>
        The MCA program offers three specialized concentrations, each tailored
        to meet the unique needs and goals of students:
        <ul style={{ listStyleType: 'disc' }} className="pl-6 mt-2">
          <li>Art History & Visual Studies</li>
          <li>Studio Arts</li>
          <li>Design</li>
        </ul>
      </p>
      <p>
        <Link to="/course">
          {" "}
          Please visit the official program website for additional information.
        </Link>
      </p>
    </div>
  );
};

export default About;
