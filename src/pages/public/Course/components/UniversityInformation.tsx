import melbourne from "@/assets/images/universities/melbourne.jpg";

const UniversityInformation = () => {
  return (
    <div className=" space-y-4 p-6 rounded-lg shadow-sm border-2 flex flex-col justify-center items-center">
      <h2 className="text-xl text-center  text-primary font-semibold">
        University Information
      </h2>

      <div className=" w-full md:w-96 lg:w-full">
        <img src={melbourne} alt="Institute" className="w-full h-full rounded-lg" />
      </div>

      <p className="bg-blue-500 cursor-pointer rounded-sm text-white text-center p-2 w-full md:w-96 lg:w-full">
        Visit University Page
      </p>
    </div>
  );
};

export default UniversityInformation;
