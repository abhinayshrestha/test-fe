const buttons = [
  { id: 1, label: "Overall Course" },
  { id: 2, label: "Requirement" },
  { id: 3, label: "Fees & Funding" },
  { id: 4, label: "Careers" },
  { id: 5, label: "Student Visa" },
];

interface InformationSpansProps {
  buttonClick: (label: string) => void;
  selectedButton: string;
}

const InformationSpans: React.FC<InformationSpansProps> = ({ buttonClick, selectedButton }) => {
  return (
    <div className=" space-y-3 p-4 rounded-lg shadow-sm border-2 ">
      <div className="flex flex-wrap xl:justify-between lg:justify-between md:justify-between  justify-center px-10">
        {buttons.map((button) => (
          <span
            key={button.id}
            className={`p-2 cursor-pointer m-1 transition duration-300 rounded-sm ${selectedButton === button.label ? 'bg-blue-500 text-white' : 'hover:bg-gray-200 '}`}
            onClick={() => buttonClick(button.label)}
          >
            {button.label}
          </span>
        ))}
      </div>
    </div>
  );
};

export default InformationSpans;
