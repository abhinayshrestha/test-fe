const Requirement = () => {
  return (
    <div className=" space-y-3 p-4 rounded-lg shadow-sm border-2 ">
      <div className=" space-y-2  px-12 py-4">
        <h2 className="text-xl pb-3 font-semibold ">
          Requirements
        </h2>
        <div className="flex flex-col md:flex-row gap-8">
          {/* IELTS */}
          <div className="w-full md:w-1/3 bg-gray-100 p-6 rounded-lg space-y-2">
            <h2 className="text-xl font-semibold mb-4">IELTS</h2>
            <p>
              IELTS is an English language proficiency test required for
              admission to many universities worldwide.
            </p>
            <p>Required Section Scores:</p>
            <ul className="list-disc pl-6">
              <li>Listening: 7.0</li>
              <li>Reading: 6.5</li>
              <li>Writing: 6.5</li>
              <li>Speaking: 7.0</li>
            </ul>
            <p>Overall Band Score Allowed: 7.0</p>
          </div>

          {/* GRE */}
          <div className="w-full md:w-1/3 bg-gray-100 p-6 rounded-lg space-y-2">
            <h2 className="text-xl font-semibold mb-4">GRE</h2>
            <p>
              GRE is a standardized test required for admission to graduate
              programs in various disciplines.
            </p>
            <p>Required Section Scores:</p>
            <ul className="list-disc pl-6">
              <li>Verbal Reasoning: 160</li>
              <li>Quantitative Reasoning: 160</li>
              <li>Analytical Writing: 4.0</li>
            </ul>
            <p>Overall Score Allowed: 320</p>
          </div>

          {/* TOEFL */}
          <div className="w-full md:w-1/3 bg-gray-100 p-6 rounded-lg space-y-2">
            <h2 className="text-xl font-semibold mb-4">TOEFL</h2>
            <p>
              TOEFL is another English language proficiency test widely accepted
              by universities.
            </p>
            <p>Required Section Scores:</p>
            <ul className="list-disc pl-6">
              <li>Reading: 22</li>
              <li>Listening: 22</li>
              <li>Speaking: 23</li>
              <li>Writing: 24</li>
            </ul>
            <p>Overall Score Allowed: 100</p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Requirement;
