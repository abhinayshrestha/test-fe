import { useState } from "react";
import About from "./About";
import InformationSpans from "./InformationSpans";
import OverallCourse from "./OverallCourse";
import Requirement from "./Requirement";
import FeesFunding from "./FeesFunding";
import Careers from "./Careers";
import StudentVisa from "./StudentVisa";
import UniversityInformation from "./UniversityInformation";
import ApplyButton from "./ApplyButton";

const Container = () => {

  const [selectedButton, setSelectedButton] = useState('Overall Course');
  const [label, setLabel] = useState('Overall Course');

  const handleClick = (clickedLabel:string) => {
    setSelectedButton(clickedLabel);
    setLabel(clickedLabel);
  }

  return (
    <div className="bg-white xl:pt-24 lg:pt-24 py-12">
    <div className="mx-auto container  bg-white"> 
      <div className=" grid xl:grid-cols-4 xl:gap-8 lg:grid-cols-4 lg:gap-8 grid-cols-1 gap-y-8">
        <div className="bg-white col-span-3 space-y-6">
          <About/>

          <InformationSpans buttonClick={handleClick} selectedButton={selectedButton} />

          {label === "Overall Course" && <OverallCourse/> }
          {label === "Requirement" && <Requirement/> }
          {label === "Fees & Funding" && <FeesFunding/> }
          {label === "Careers" && <Careers/> }
          {label === "Student Visa" && <StudentVisa/> }
          
        </div>
        <div className="bg-white col-span-1 space-y-6">
          <UniversityInformation />
          <ApplyButton />
        </div>
      </div>
    </div>
    </div>
  );
};

export default Container;
