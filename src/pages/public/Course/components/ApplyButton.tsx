import { useNavigate } from "react-router-dom";

const ApplyButton = () => {

  const navigate = useNavigate();

  const navigateConsultancy = () => {
    navigate(`/consultancy`);
  };

  return (
    <div className=" space-y-4 p-6 rounded-lg shadow-sm border-2 flex flex-col justify-center items-center">
      <h2 className=" text-center  text-primary ">
        Are you interested in <span className=" font-semibold">Masters in Computer Applications?</span>
      </h2>

      <span className="bg-blue-500 cursor-pointer rounded-sm text-white text-center p-2 w-full md:w-96 lg:w-full" onClick={navigateConsultancy}>
        Book On Call Counseling
      </span>
    </div>
  );
};

export default ApplyButton;
