import { FaRegClock , FaRegMoneyBillAlt, FaRegCalendarAlt} from "react-icons/fa";

const CourseBanner: React.FC = () => {
  return (
    <div className="relative bg-gray-100  xl:h-64 lg:h-64 h-auto flex flex-col items-center justify-between pt-16">
      <div className="xl:absolute lg:absolute inset-0 flex items-center justify-center pb-10">
        <div className="text-center">
          <h2 className="text-4xl font-semibold text-primary">
            Masters in Computer Applications
          </h2>
        </div>
      </div>
      <div className="xl:absolute lg:absolute inset-52  z-10 border h-auto xl:h-24 lg:h-24 bg-white py-2 rounded-lg shadow-md text-primary w-3/4 xl:w-auto lg:w-auto mx-10">
        <div className=" flex flex-wrap justify-between h-full  xl:px-6 lg:px-6 px-10">
          <div className="flex items-center justify-center xl:w-1/3 lg:w-1/3 my-2">
            <div className="  justify-center xl:text-4xl lg:text-3xl text-2xl rounded-full bg-gray-100 p-4">
              <FaRegClock />
            </div>

            <div className="ml-4 text-gray-700">
              <p className="xl:text-2xl lg:text-lg font-bold">4 Year</p>
              <p className="xl:text-lg lg:text-sm">Duration</p>
            </div>
          </div>

          <div className="flex items-center justify-center  xl:w-1/3 lg:w-1/3 my-2">
            <div className="  justify-center xl:text-4xl lg:text-3xl text-2xl  rounded-full bg-gray-100 p-4">
              <FaRegMoneyBillAlt />
            </div>

            <div className="ml-4 text-gray-700">
              <p className="xl:text-2xl lg:text-lg font-bold">$ 15,000</p>
              <p className="xl:text-lg lg:text-sm">Tution Fees</p>
            </div>
          </div>

          <div className="flex items-center justify-center  xl:w-1/3 lg:w-1/3 my-2">
            <div className="  justify-center xl:text-4xl lg:text-3xl text-2xl  rounded-full bg-gray-100 p-4">
              <FaRegCalendarAlt />
            </div>

            <div className="ml-4 text-gray-700">
              <p className="xl:text-2xl lg:text-lg font-bold">March / April</p>
              <p className="xl:text-lg lg:text-sm">Intake</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CourseBanner;
