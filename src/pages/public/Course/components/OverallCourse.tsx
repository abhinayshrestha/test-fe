const courseDetails = [
  { name: "Study Time", value: "Full Time" },
  { name: "Course Category", value: "Computer Science" },
  { name: "Degree Level", value: "Masters" },
  { name: "Location", value: "250 Carlos Bee , Hayward, California, USA." },
];

const courseUnits = [
  {
    code: "CS 501",
    title: "Advanced Algorithms",
    units: 3,
    breadthArea: "Theory",
  },
  {
    code: "CS 502",
    title: "Artificial Intelligence",
    units: 3,
    breadthArea: "AI & Machine Learning",
  },
  {
    code: "CS 503",
    title: "Database Systems",
    units: 3,
    breadthArea: "Databases",
  },
  {
    code: "CS 504",
    title: "Software Engineering",
    units: 3,
    breadthArea: "Software Engineering",
  },
  {
    code: "CS 505",
    title: "Computer Networks",
    units: 3,
    breadthArea: "Networks",
  },
  {
    code: "CS 506",
    title: "Cybersecurity",
    units: 3,
    breadthArea: "Security",
  },
  {
    code: "CS 507",
    title: "Data Science",
    units: 3,
    breadthArea: "Data Science",
  },
  {
    code: "CS 508",
    title: "Human-Computer Interaction",
    units: 3,
    breadthArea: "HCI",
  },
  {
    code: "CS 509",
    title: "Cloud Computing",
    units: 3,
    breadthArea: "Cloud Computing",
  },
  {
    code: "CS 510",
    title: "Big Data Analytics",
    units: 3,
    breadthArea: "Big Data",
  },
  {
    code: "CS 600",
    title: "Master's Project",
    units: 6,
    breadthArea: "Capstone Project",
  },
];

const OverallCourse = () => {
  return (
    <div className=" space-y-3 p-4 rounded-lg shadow-sm border-2 ">
      <div className=" space-y-2  px-12 py-4">

        {courseDetails.map((course, index) => (
          <p key={index}>
            <span className=" font-semibold">{course.name} : </span>
            {course.value}
          </p>
        ))}

        <h2 className="text-xl pt-6 pb-3 font-semibold border-b">Core Coursework</h2>

        <p className="py-2">The following 20 core units are required :</p>

        <ul style={{ listStyleType: "disc" }} className="pl-6 mt-2">
          {courseUnits.map((course) => (
            <li key={course.code} className="mb-4">
              <span className="mr-3 font-semibold">
                {course.code} - {course.title}
              </span>
              Units: {course.units} - Breadth Area: {course.breadthArea}
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
};

export default OverallCourse;
