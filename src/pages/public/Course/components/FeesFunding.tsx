const FeesFunding = () => {
  return (
    <div className=" space-y-3 p-4 rounded-lg shadow-sm border-2 ">
      <div className=" space-y-2  px-12 py-4">

        <h2 className="text-xl pb-3 font-semibold ">Fees & Funding</h2>

        <div className="flex flex-col md:flex-row gap-8">
          <div className="w-full md:w-1/2 bg-gray-100 p-6 rounded-lg space-y-2">

            <h2 className="text-xl font-semibold mb-4">Tuition Fees</h2>

            <p>
              Tuition fees vary depending on the university, program, and
              country. Here are some general estimates:
            </p>

            <ul className="list-disc pl-6">
              <li>Undergraduate: $20,000 - $50,000 per year</li>
              <li>Graduate: $25,000 - $60,000 per year</li>
              <li>MBA: $30,000 - $100,000 per year</li>
            </ul>
          </div>

          <div className="w-full md:w-1/2 bg-gray-100 p-6 rounded-lg space-y-2">

            <h2 className="text-xl font-semibold mb-4">Scholarships</h2>

            <p>
              Many universities offer scholarships and financial aid to
              international students. Types of scholarships include:
            </p>

            <ul className="list-disc pl-6">
              <li>Merit-based scholarships</li>
              <li>Need-based scholarships</li>
              <li>Country-specific scholarships</li>
              <li>Program-specific scholarships</li>
            </ul>
            
            <p>
              Be sure to check the university website for scholarship
              opportunities and application details.
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FeesFunding;
