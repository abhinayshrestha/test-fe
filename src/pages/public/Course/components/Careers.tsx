const careers = [
  {
    title: "Software Developer",
    description:
      "Design, develop, and maintain software applications and systems.",
  },
  {
    title: "Database Administrator",
    description:
      "Manage and maintain databases to ensure they function efficiently and securely.",
  },
  {
    title: "Systems Analyst",
    description:
      "Analyze business requirements and design information systems solutions.",
  },
  {
    title: "Network Administrator",
    description:
      "Install, configure, and maintain network hardware and software.",
  },
  {
    title: "Web Developer",
    description: "Design and develop websites and web applications.",
  },
  {
    title: "Quality Assurance Analyst",
    description:
      "Test software applications to ensure they meet quality standards.",
  },
];

const Careers = () => {

  return (
    <div className=" space-y-3 p-4 rounded-lg shadow-sm border-2 ">
      <div className=" space-y-2  px-12 py-4">

        <h2 className="text-xl pb-3 font-semibold ">Careers</h2>

        <ul style={{ listStyleType: "disc" }} className="pl-6 mt-2">
          {careers.map((career, index) => (
            <li key={index} className="mb-4">
              <strong>{career.title}</strong> <br />
              {career.description}
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
};

export default Careers;
