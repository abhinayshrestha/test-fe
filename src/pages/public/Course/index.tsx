import { Layout } from "antd";
import { Content, Footer } from "antd/es/layout/layout";
import CourseBanner from "./components/CourseBanner";
import DetailContainer from "./components/DetailContainer";

const Course = () => {
  return (
    <Layout>
      <Content>
        <CourseBanner />
        <DetailContainer />
      </Content>
    </Layout>
  );
};

export default Course;
