import { Checkbox, Form, Input, Modal, Image, Col, Row, Button } from "antd";
import world from "@/assets/images/office.png";

const OurOffice = () => {
  
  return (
    <div className="py-20 bg-gray-100">
      <div className="container mx-auto">
        <div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 sm:gap-8">
          <div className="flex justify-center items-center p-4">

            <h1 className=" text-5xl font-bold  sm:hidden">Our Offices</h1>

            <Image
              src={world}
              alt="world"
              preview={false}
              // height={300}
              className=" ml-5 p-2 h-auto md:h-64 "
            />
          </div>

          <div className=" lg:col-span-2">
            <div className="space-y-12 px-4 lg:pl-20">

              <h1 className=" text-5xl font-bold hidden sm:block">
                Our Offices
              </h1>

              <div className="flex space-x-4  sm:space-x-4 lg:space-x-20">
                <div className=" lg:space-y-2 ">
                  <p className=" font-bold text-lg mb-4">Putalisadak Office</p>
                  <p>Putalisadak</p>
                  <p>Kathmandu</p>
                  <p>Nepal</p>
                  <p>(+977) 9818586848</p>
                </div>

                <div className=" lg:space-y-2">
                  <p className=" font-bold text-lg mb-4">Kalanki Office</p>
                  <p>Kalanki</p>
                  <p>Kathmandu</p>
                  <p>Nepal</p>
                  <p>(+977) 9818546848</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default OurOffice;
