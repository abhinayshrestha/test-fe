import {
  BankOutlined,
  MailOutlined,
  PhoneOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { FaBriefcase } from "react-icons/fa";
import { Form, Input, Col, Row, Button, Collapse, Select } from "antd";

const { Option } = Select;

const countryOptions = ["Australia", "USA", "UK", "Germany", "Canada"];

const InstitutionForm: React.FC = () => {
  const [form] = Form.useForm();

  return (
    <div className=" ">
      <div className="p-5 rounded-lg shadow-md border">
        <Form form={form} layout="vertical">
          <div className="500:flex  500:space-x-4">
            <div className="500:w-1/2">
              <Form.Item
                name="institution"
                label="Institution Name :"
                rules={[
                  {
                    required: true,
                    message: "Please enter your Institution name",
                  },
                ]}
              >
                <Input addonBefore={<BankOutlined />} />
              </Form.Item>
            </div>

            <div className="500:w-1/2">
              <Form.Item
                name="job"
                label="Job Title :"
                rules={[
                  {
                    required: true,
                    message: "Please enter your job title",
                  },
                ]}
              >
                <Input addonBefore={<FaBriefcase />} />
              </Form.Item>
            </div>
          </div>

          <div className="500:flex  500:space-x-4">
            <div className="500:w-1/2">
              <Form.Item
                name="name"
                label="First Name :"
                rules={[
                  {
                    required: true,
                    message: "Please enter your first name",
                  },
                ]}
              >
                <Input addonBefore={<UserOutlined />} />
              </Form.Item>
            </div>

            <div className="500:w-1/2">
              <Form.Item
                name="name"
                label="Last Name :"
                rules={[
                  {
                    required: true,
                    message: "Please enter your last name",
                  },
                ]}
              >
                <Input addonBefore={<UserOutlined />} />
              </Form.Item>
            </div>
          </div>

          <div className="500:flex  500:space-x-4">
            <div className="500:w-1/2">
              <Form.Item
                name="email"
                label="Email :"
                rules={[
                  {
                    required: true,
                    type: "email",
                    message: "Please enter a valid email address",
                  },
                ]}
              >
                <Input addonBefore={<MailOutlined />} />
              </Form.Item>
            </div>

            <div className="500:w-1/2">
              <Form.Item
                name="contact"
                label="Contact :"
                rules={[
                  {
                    required: true,
                    message: "Please enter your contact number",
                  },
                ]}
              >
                <Input addonBefore={<PhoneOutlined />} />
              </Form.Item>
            </div>
          </div>

          <div className="">
            <div className="">
              <Form.Item label="Destination Country">
                <Select>
                  {countryOptions.map((option) => (
                    <Option key={option} value={option}>
                      {option}
                    </Option>
                  ))}
                </Select>
              </Form.Item>
            </div>
          </div>

          <Row>
            <Col span={24}>
              <Form.Item
                label="Message"
                name="message"
                rules={[
                  { required: true, message: "Please input your message!" },
                ]}
              >
                <Input.TextArea rows={4} />
              </Form.Item>
            </Col>
          </Row>

          <Col span={24}>
            <Button
              className=" bg-red-600 text-white w-full cursor-pointer hover:bg-red-700"
              style={{ border: "none" }}
              htmlType="submit"
            >
              Submit
            </Button>
          </Col>
        </Form>
      </div>
    </div>
  );
};

export default InstitutionForm;
