import {
  CaretRightOutlined,
  EnvironmentOutlined,
  GlobalOutlined,
  MailOutlined,
  PhoneOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { Form, Input, Col, Row, Button, Collapse, Select } from "antd";

const { Panel } = Collapse;
const { Option } = Select;

const degreeOptions = ["Bachelor", "Master", "PhD"];
const courseCategoryOptions = ["Science", "Arts", "Engineering"];
const scoreTypeOptions = ["GPA", "Percentage"];
const englishProficiencyTestOptions = ["TOEFL", "IELTS"];
const countryOptions = ["Australia", "USA", "UK", "Germany", "Canada"];
const intakeOptions = ["2024", "2025", "2026", "2027"];

const StudentForm: React.FC = () => {
  const [form] = Form.useForm();

  return (
    <div className=" ">
      <div className="p-5 rounded-lg shadow-md border">
        <Form form={form} layout="vertical">
          <div className="500:flex  500:space-x-4">
            <div className="500:w-1/2">
              <Form.Item
                name="name"
                label="First Name :"
                rules={[
                  {
                    required: true,
                    message: "Please enter your first name",
                  },
                ]}
              >
                <Input addonBefore={<UserOutlined />} />
              </Form.Item>
            </div>
            <div className="500:w-1/2">
              <Form.Item
                name="name"
                label="Last Name :"
                rules={[
                  {
                    required: true,
                    message: "Please enter your last name",
                  },
                ]}
              >
                <Input addonBefore={<UserOutlined />} />
              </Form.Item>
            </div>
          </div>

          <div className="500:flex  500:space-x-4">
            <div className="500:w-1/2">
              <Form.Item
                name="address"
                label="Address :"
                rules={[
                  { required: true, message: "Please enter your address" },
                ]}
              >
                <Input addonBefore={<EnvironmentOutlined />} />
              </Form.Item>
            </div>

            <div className="500:w-1/2">
              <Form.Item
                name="contact"
                label="Contact :"
                rules={[
                  {
                    required: true,
                    message: "Please enter your contact number",
                  },
                ]}
              >
                <Input addonBefore={<PhoneOutlined />} />
              </Form.Item>
            </div>
          </div>

          <div className="500:flex  500:space-x-4">
            <div className="500:w-1/2">
              <Form.Item
                name="email"
                label="Email :"
                rules={[
                  {
                    required: true,
                    type: "email",
                    message: "Please enter a valid email address",
                  },
                ]}
              >
                <Input addonBefore={<MailOutlined />} />
              </Form.Item>
            </div>

            <div className="500:w-1/2">
              <Form.Item
                name="nationality"
                label="Nationality :"
                rules={[
                  {
                    required: true,
                    message: "Please enter your nationality",
                  },
                ]}
              >
                <Input addonBefore=<GlobalOutlined /> />
              </Form.Item>
            </div>
          </div>

          <div className="500:flex  500:space-x-4">
            <div className="500:w-1/2">
              <Form.Item label="Preferred Country">
                <Select>
                  {countryOptions.map((option) => (
                    <Option key={option} value={option}>
                      {option}
                    </Option>
                  ))}
                </Select>
              </Form.Item>
            </div>

            <div className="500:w-1/2">
              <Form.Item label="Preferred Intake">
                <Select>
                  {intakeOptions.map((option) => (
                    <Option key={option} value={option}>
                      {option}
                    </Option>
                  ))}
                </Select>
              </Form.Item>
            </div>
          </div>

          <Row>
            <Col span={24}>
              <Form.Item
                label="Message"
                name="message"
                rules={[
                  { required: true, message: "Please input your message!" },
                ]}
              >
                <Input.TextArea rows={4} />
              </Form.Item>
            </Col>
          </Row>

          <Collapse
            bordered={false}
            // defaultActiveKey={["1"]}
            expandIcon={({ isActive }) => (
              <CaretRightOutlined rotate={isActive ? 90 : 0} />
            )}
            className="bg-white "
          >
            <Panel header="Education Details" key="1" className="text-lg  ">
              <div className="500:flex  500:space-x-4">
                <div className="500:w-1/2">

                  <Form.Item label="Degree">
                    <Select>
                      {degreeOptions.map((option) => (
                        <Option key={option} value={option}>
                          {option}
                        </Option>
                      ))}
                    </Select>
                  </Form.Item>

                </div>

                <div className="500:w-1/2">

                  <Form.Item label="Course Category">
                    <Select>
                      {courseCategoryOptions.map((option) => (
                        <Option key={option} value={option}>
                          {option}
                        </Option>
                      ))}
                    </Select>
                  </Form.Item>

                </div>
              </div>

              <div className="500:flex  500:space-x-4">

                <div className="500:w-1/2">
                  <Form.Item label="Score Type">
                    <Select>
                      {scoreTypeOptions.map((option) => (
                        <Option key={option} value={option}>
                          {option}
                        </Option>
                      ))}
                    </Select>
                  </Form.Item>
                </div>

                <div className="500:w-1/2">
                  <Form.Item label="Academic Score">
                    <Input />
                  </Form.Item>
                </div>

              </div>
              <div className="500:flex  500:space-x-4">

                <div className="500:w-1/2">
                  <Form.Item label="English Proficiency Test">

                    <Select>
                      {englishProficiencyTestOptions.map((option) => (
                        <Option key={option} value={option}>
                          {option}
                        </Option>
                      ))}
                    </Select>

                  </Form.Item>
                </div>
                
                <div className="500:w-1/2">
                  <Form.Item label="Score">
                    <Input />
                  </Form.Item>
                </div>
              </div>
            </Panel>
          </Collapse>

          <Col span={24}>
            <Button
              className=" bg-red-600 text-white w-full cursor-pointer hover:bg-red-700"
              style={{ border: "none" }}
              htmlType="submit"
            >
              Submit
            </Button>
          </Col>

        </Form>
      </div>
    </div>
  );
};

export default StudentForm;
