import { useState } from "react";
import ContactDetail from "./ContactDetail";
import StudentForm from "./StudentForm";
import InstitutionForm from "./InstitutionForm";

const buttons = [
  { id: 1, label: "Student" },
  { id: 2, label: "Institution" },
];

const ContactContainer = () => {
  const [activeTab, setActiveTab] = useState<string>("Student");

  const handleTabClick = (tab: string) => {
    setActiveTab(tab);
  };

  return (
    <div className="py-10 bg-white">
      <div className="container mx-auto">
        <div className="grid grid-cols-1 lg:grid-cols-2 lg:gap-8">

          <div>
            <ContactDetail />
          </div>

          <div className=" px-16 space-y-3">
            <div className="flex justify-center ">

              {buttons.map((button) => (
                <span
                  key={button.id}
                  className={`p-2 cursor-pointer m-1 transition duration-300 rounded-sm ${activeTab === button.label ? "bg-blue-500 text-white" : "hover:bg-gray-200 border"}`}
                  onClick={() => handleTabClick(button.label)}
                >
                  {button.label}
                </span>
              ))}

            </div>

            {activeTab === "Student" && <StudentForm />}
            {activeTab === "Institution" && <InstitutionForm />}
          </div>
        </div>
      </div>
    </div>
  );
};

export default ContactContainer;
