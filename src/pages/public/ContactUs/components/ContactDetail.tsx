const ContactDetail = () => {
  return (
    <div className=" p-5">
      <div className="space-y-12">
        <h1 className=" text-5xl font-bold">Contact Us</h1>

        <p className=" font-semibold ">
          Explore endless opportunities for studying abroad with Study Best
          Feed! Discover universities, courses, and consultancy services all in
          one place, making your dream of international education a reality.
          Whether you are seeking guidance on applications or searching for the
          perfect program, our platform is tailored to your needs. Reach out to
          us for support, inquiries, or partnership opportunities.{" "}
        </p>

        <p className=" font-semibold ">
          Let Study Best Feed be your guide on the path to academic success and
          global exploration.
        </p>

        <p className=" font-semibold ">Contact us to get Started.</p>
      </div>
    </div>
  );
};

export default ContactDetail;
