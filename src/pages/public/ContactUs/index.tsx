import { Content } from "antd/es/layout/layout";
import ContactContainer from "./components/ContactContainer";
import OurOffice from "./components/OurOffice";

const ContactUs = () => {
  return (

    <Content>
      <ContactContainer />
      <OurOffice />
    </Content>

  );
};

export default ContactUs;
