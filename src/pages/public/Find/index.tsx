import Layout, { Content, Footer } from "antd/es/layout/layout";
import StepForm from "./components/StepForm";

const Find = () => {
  return (
    <div>
      <Layout>
        <Content>
          <StepForm />
        </Content>
      </Layout>
    </div>
  );
};

export default Find;
