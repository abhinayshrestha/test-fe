import React, { useState } from "react";

import flagUsa from "@/assets/images/flags/flagUsa.png";
import flagUk from "@/assets/images/flags/flagUk.png";
import flagAustralia from "@/assets/images/flags/flagAustralia.png";
import flagCanada from "@/assets/images/flags/flagCanada.png";
import flagGermany from "@/assets/images/flags/flagGermany.png";
import flagPoland from "@/assets/images/flags/flagPoland.png";
import flagPortugal from "@/assets/images/flags/flagPortugal.png";
import flagSingapore from "@/assets/images/flags/flagSingapore.png";
import flagFrance from "@/assets/images/flags/flagFrance.png";

import FormComponent from "./FormComponent";

const data = [
  {
    title: "Where do you want to study?",
    options: [
      { id: 1, name: "United States", icon: flagUsa },
      { id: 2, name: "United Kingdom", icon: flagUk },
      { id: 3, name: "Australia", icon: flagAustralia },
      { id: 4, name: "Canada", icon: flagCanada },
      { id: 5, name: "Germany", icon: flagGermany },
      { id: 6, name: "Poland", icon: flagPoland },
      { id: 7, name: "Portugal", icon: flagPortugal },
      { id: 8, name: "Singapore", icon: flagSingapore },
      { id: 9, name: "France", icon: flagFrance },
    ],
    // sliderOneTitle: "Slider 1",
    // sliderOneRange: {
    //   min: 0,
    //   max: 10,
    //   defaultValue: 0,
    //   dataType: "number",
    // },
    // sliderTwoTitle: "Slider 2",
    // sliderTwoRange: {
    //   min: 0,
    //   max: 10,
    //   defaultValue: 0,
    //   dataType: "points",
    // },
  },
];

const FirstForm = () => {
  return (
    <div>
      {data.map((formData, index) => (
        <FormComponent
          key={index}
          title={formData.title}
          options={formData.options}
          firstItemName={"country"}
        />
      ))}
    </div>
  );
};

export default FirstForm;
