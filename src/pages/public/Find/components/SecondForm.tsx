import React, { useState } from "react";

import flagUsa from "@/assets/images/flags/flagUsa.png";
import flagUk from "@/assets/images/flags/flagUk.png";

import FormComponent from "./FormComponent";

const data = [
  {
    title: "What degree you want to pursue?",
    options: [
      { id: 1, name: "Bachelors", icon: flagUsa },
      { id: 2, name: "Masters", icon: flagUk },
    ],
  },
];

const SecondForm = () => {
  return (
    <div>
      {data.map((formData, index) => (
        <FormComponent
          key={index}
          title={formData.title}
          options={formData.options}
          firstItemName="degree"
        />
      ))}
    </div>
  );
};

export default SecondForm;
