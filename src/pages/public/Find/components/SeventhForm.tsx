import React, { useState } from "react";

import flagUsa from "@/assets/images/flags/flagUsa.png";
import flagUk from "@/assets/images/flags/flagUk.png";
import flagAustralia from "@/assets/images/flags/flagAustralia.png";

import FormComponent from "./FormComponent";

const data = [
  {
    title: "which english test did you take?",
    options: [
      { id: 1, name: "SAT", icon: flagUsa },
      { id: 2, name: "ACT", icon: flagUk },
      { id: 3, name: "GMAT", icon: flagAustralia },
      { id: 3, name: "GRE", icon: flagAustralia },
    ],
    sliderOneTitle: "What was your Score  ? ",
    aptTestScore: {
      min: 0,
      max: 9,
      defaultValue: 0,
      dataType: "number ",
    },
  },
];

const SeventhForm = () => {
  return (
    <div>
      {data.map((formData, index) => (
        <FormComponent
          key={index}
          title={formData.title}
          options={formData.options}
          sliderOneTitle={formData.sliderOneTitle}
          sliderOneRange={formData.aptTestScore}
          firstItemName={"aptitudeTest"}
          secondItemName="TestScore"
        />
      ))}
    </div>
  );
};

export default SeventhForm;
