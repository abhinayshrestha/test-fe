import { useState } from "react";

import { Slider, Form, Image } from "antd";

import { useStepFormContextData } from "./StepFormWrapper";

interface Option {
  id: number;
  name: string;
  icon: string;
}

interface SliderProps {
  min: number;
  max: number;
  defaultValue: number;
}

interface Props {
  title: string;
  options: Option[];
  firstItemName: string;
  secondItemName?: string;
  sliderOneTitle?: string;
  sliderOneRange?: SliderProps;
  sliderTwoTitle?: string;
  sliderTwoRange?: SliderProps;
}

// export const StyledRadioGroupWithoutDot = styled(Radio.Group)`
//   .ant-radio-wrapper .ant-radio-inner {
//     display: none;
//   }
//   .ant-radio-wrapper span.ant-radio + * {
//     padding-inline: 0px;
//   }
// `;

// export const StyledCardWithoutPadding = styled(Card)`
//   &.css-dev-only-do-not-override-duyrbk.ant-card .ant-card-body {
//   }
// `;

const FormComponent: React.FC<Props> = ({
  title,
  options,
  sliderOneTitle,
  sliderOneRange,
  firstItemName,
  secondItemName,
}) => {
  const { form } = useStepFormContextData();
  const [sliderValue, setSliderValue] = useState<number>();

  return (
    <Form form={form} preserve layout="vertical">
      <div className="p-4 bg-indigo-50">
        <div className="mb-6">
          <h2 className="text-2xl font-semibold my-8 text-primary text-left">
            {title}
          </h2>
          <Form.Item
            noStyle
            shouldUpdate={(prevValues, curValues) => {
              const previousFirstItem = prevValues?.[firstItemName];
              const previousSecondName = [secondItemName, previousFirstItem];
              const isChanged =
                prevValues?.[firstItemName] !== curValues?.[firstItemName];
              if (isChanged && !!previousFirstItem) {
                form.setFieldValue(previousSecondName, null);
              }
              return isChanged;
            }}
          >
            {({ getFieldValue }) => {
              const isSelected = getFieldValue([firstItemName]);
              return (
                <Form.Item
                  name={[firstItemName]}
                  rules={[
                    {
                      required: true,
                      message: "Please Select the option",
                    },
                  ]}
                >
                  <div className="flex flex-wrap justify-center">
                    {(options || []).map((item) => {
                      const isActive = isSelected === item.name;
                      return (
                        <label
                          key={item.name}
                          className={`flex items-center justify-center px-8 py-3 rounded-3xl cursor-pointer mx-2 my-2 transition duration-300 ease-in-out ${
                            isActive
                              ? "bg-primary text-white"
                              : "bg-white text-primary border border-gray-300"
                          }`}
                          htmlFor={item.name}
                        >
                          <input
                            type="radio"
                            className="sr-only"
                            id={item.name}
                            name={firstItemName}
                            value={item.name}
                            defaultChecked={isSelected === item.name}
                          />
                          <div className="flex items-center">
                            <Image
                              src={item.icon}
                              height={30}
                              width={30}
                              preview={false}
                              className="rounded-full"
                            />
                            <p className="ml-2 font-semibold md:text-lg">
                              {item.name}
                            </p>
                          </div>
                        </label>
                      );
                    })}
                  </div>
                </Form.Item>
              );
            }}
          </Form.Item>{" "}
          {secondItemName && (
            <Form.Item
              noStyle
              shouldUpdate={(prevValues, curValues) => {
                return (
                  prevValues?.[firstItemName] !== curValues?.[firstItemName]
                );
              }}
            >
              {({ getFieldValue }) => {
                const optionValue = getFieldValue([firstItemName]);
                const getMaxValue = () => {
                  switch (optionValue) {
                    case "IELTS":
                      return 9;
                    case "PTE":
                      return 90;
                    case "TOEFL":
                      return 120;
                    case "SAT":
                      return 1600;
                    case "ACT":
                      return 36;
                    case "GMAT":
                      return 800;
                    case "GRE":
                      return 340;
                    default:
                      return 4;
                  }
                };

                return optionValue && optionValue !== "None" ? (
                  <div className="mt-6 ">
                    <h2 className="text-2xl font-semibold mb-4 text-primary ">
                      {sliderOneTitle}
                    </h2>
                    <Form.Item
                      name={[secondItemName, optionValue]}
                      rules={[
                        {
                          required: true,
                          message: "Please select your score",
                        },
                      ]}
                    >
                      <Slider
                        onChange={setSliderValue}
                        step={
                          optionValue === "12th" ||
                          optionValue === "Bachelors" ||
                          optionValue === "Masters"
                            ? 0.1
                            : 1
                        }
                        min={1}
                        max={getMaxValue()}
                        defaultValue={getMaxValue() / 2}
                        className="w-full"
                      />
                    </Form.Item>
                    <Form.Item>
                      <span className=" text-lg bg-white p-2  font-semibold  text-primary">
                        {sliderValue}
                      </span>
                    </Form.Item>
                  </div>
                ) : null;
              }}
            </Form.Item>
          )}
        </div>
      </div>
    </Form>
  );
};

export default FormComponent;
