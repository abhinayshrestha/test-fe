import React, { useState } from "react";

import flagUsa from "@/assets/images/flags/flagUsa.png";
import flagUk from "@/assets/images/flags/flagUk.png";
import flagAustralia from "@/assets/images/flags/flagAustralia.png";

import FormComponent from "./FormComponent";

const data = [
  {
    title: "What is your preferred intake?  ",
    options: [
      { id: 1, name: "Jul-Sep", icon: flagUsa },
      { id: 2, name: "Oct-Nov", icon: flagUk },
      { id: 3, name: "Jan-March", icon: flagAustralia },
      { id: 4, name: "Feb-Apr", icon: flagUsa },
      { id: 5, name: "Jun-Aug", icon: flagUk },
      { id: 6, name: "Dec-Feb", icon: flagAustralia },
    ],
  },
];

const FifthForm = () => {
  return (
    <div>
      {data.map((formData, index) => (
        <FormComponent
          key={index}
          title={formData.title}
          options={formData.options}
          firstItemName={"intake"}
        />
      ))}
    </div>
  );
};

export default FifthForm;
