import React, { useEffect, useState } from "react";
import { Button, Form, message, Steps, theme } from "antd";
import { ArrowRightOutlined } from "@ant-design/icons";

import Sidecard from "./Sidecard";
import FirstForm from "./FirstForm";
import SecondForm from "./SecondForm";
import ThirdForm from "./ThirdForm";
import FourthForm from "./FourthForm";
import FifthForm from "./FifthForm";
import SixthForm from "./SixthForm";
import { useStepFormContextData } from "./StepFormWrapper";
import SeventhForm from "./SeventhForm";

const StepForm: React.FC = () => {
  const { form } = useStepFormContextData();
  const steps = [
    {
      title: "First",
      content: <FirstForm />,
    },
    {
      title: "Second",
      content: <SecondForm />,
    },
    {
      title: "Third",
      content: <ThirdForm />,
    },
    {
      title: "Fourth",
      content: <FourthForm />,
    },
    {
      title: "Fifth",
      content: <FifthForm />,
    },
    {
      title: "Sixth",
      content: <SixthForm />,
    },

    {
      title: "Seventh",
      content: <SeventhForm />,
    },
  ];

  const { token } = theme.useToken();
  const [current, setCurrent] = useState(0);

  const next = () => {
    form.validateFields().then(() => {
      setCurrent(current + 1);
    });
  };

  const prev = () => {
    setCurrent(current - 1);
  };

  // const removeNullProperties = (obj: any, parentKey = ""): any => {
  //   if (typeof obj !== "object" || obj === null || Array.isArray(obj)) {
  //     return obj;
  //   }
  //   const cleanedObj: any = {};
  //   Object.entries(obj).forEach(([key, value]) => {
  //     const combinedKey = parentKey ? `${parentKey}_${key}` : key;
  //     const cleanedValue = removeNullProperties(value, combinedKey);
  //     if (
  //       cleanedValue !== null &&
  //       (typeof cleanedValue !== "object" ||
  //         Object.keys(cleanedValue).length !== 0)
  //     ) {
  //       if (typeof cleanedValue === "object" && !Array.isArray(cleanedValue)) {
  //         Object.entries(cleanedValue).forEach(([nestedKey, nestedValue]) => {
  //           cleanedObj[nestedKey] = nestedValue;
  //         });
  //       } else {
  //         cleanedObj[combinedKey] = cleanedValue;
  //       }
  //     }
  //   });
  //   return cleanedObj;
  // };

  const SumbitHandler = () => {
    const formValues = form.getFieldsValue(true);
    // const cleanFormValues = removeNullProperties(formValues);

    // console.log(cleanFormValues);
    console.log(formValues);

    message.success("Processing complete!");
  };

  const contentStyle: React.CSSProperties = {
    lineHeight: "260px",
    textAlign: "center",
    color: token.colorTextTertiary,
    backgroundColor: token.colorFillAlter,
    borderRadius: token.borderRadiusLG,
    border: `1px dashed ${token.colorBorder}`,
  };

  window.onbeforeunload = function confirmNavigation() {
    return `Data will be lost if you leave the page, are you sure?`;
  };

  return (
    <div className=" 2xl:w-[75vw] md:container md:mx-auto px-4 py-8 justify-center flex flex-col xl:flex-row">
      <div className=" flex flex-col flex-grow flex-wrap">
        <Sidecard />
      </div>

      <div className=" lg:ml-8 md:container md:mx-auto flex flex-col flex-grow">
        {/* <Steps responsive current={current} items={items}  /> */}
        <div style={contentStyle}>
          {steps[current].content}
          <div
            style={{ marginTop: "10px", marginBottom: "60px" }}
            className="flex  justify-center"
          >
            {current > 0 && (
              <Button
                className=" flex items-center text-base rounded-xl h-10  mx-2 border-1 border-gray-500"
                onClick={() => prev()}
              >
                Previous
              </Button>
            )}
            {current < steps.length - 1 && (
              <Button
                type="primary"
                className=" text-base flex items-center rounded-xl h-10  mx-2 "
                onClick={() => next()}
              >
                Next
                <ArrowRightOutlined className="bg-white text-primary p-1 inline-flex rounded-full" />
              </Button>
            )}

            {current === steps.length - 1 && (
              <Button
                type="primary"
                className=" text-base rounded-xl h-10  mx-2"
                onClick={() => {
                  form.validateFields().then(() => SumbitHandler());
                }}
              >
                Done
              </Button>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default StepForm;
