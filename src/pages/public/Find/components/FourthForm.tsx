import flagUsa from "@/assets/images/flags/flagUsa.png";
import flagUk from "@/assets/images/flags/flagUk.png";
import flagAustralia from "@/assets/images/flags/flagAustralia.png";

import FormComponent from "./FormComponent";

const data = [
  {
    title: "Your Current Educational Level?",
    options: [
      { id: 1, name: "12th", icon: flagUsa },
      { id: 2, name: "Bachelors", icon: flagUk },
      { id: 3, name: "Masters", icon: flagAustralia },
    ],
    sliderOneTitle: "What your GPA  ",
    GPA: {
      step: 0.1,
      min: 0,
      max: 4,
      defaultValue: 0,
      dataType: "points",
    },
  },
];

// const data = [
//   {
//     title: "Your Current Educational Level?",
//     options: [
//       {
//         id: 1,
//         name: "12th",
//         icon: flagUsa,
//         GPA: {
//           step: 0.1,
//           min: 0,
//           max: 4,
//           defaultValue: 0,
//           dataType: "points",
//         },
//       },
//       {
//         id: 2,
//         name: "Bachelors",
//         icon: flagUk,
//         GPA: {
//           step: 0.1,
//           min: 0,
//           max: 4,
//           defaultValue: 0,
//           dataType: "points",
//         },
//       },
//       {
//         id: 3,
//         name: "Masters",
//         icon: flagAustralia,
//         GPA: {
//           step: 0.1,
//           min: 0,
//           max: 4,
//           defaultValue: 0,
//           dataType: "points",
//         },
//       },
//     ],
//     sliderOneTitle: "What your GPA  ",
//   },
// ];

const FourthForm = () => {
  return (
    <div>
      {data.map((formData, index) => (
        <FormComponent
          key={index}
          title={formData.title}
          options={formData.options}
          sliderOneTitle={formData.sliderOneTitle}
          sliderOneRange={formData.GPA}
          firstItemName={"education"}
          secondItemName="GPA"
        />
      ))}
    </div>
  );
};

export default FourthForm;
