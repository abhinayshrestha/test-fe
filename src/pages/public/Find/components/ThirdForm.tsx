import React, { useState } from "react";

import flagUsa from "@/assets/images/flags/flagUsa.png";
import flagUk from "@/assets/images/flags/flagUk.png";
import flagAustralia from "@/assets/images/flags/flagAustralia.png";
import flagCanada from "@/assets/images/flags/flagCanada.png";
import flagGermany from "@/assets/images/flags/flagGermany.png";
import flagPoland from "@/assets/images/flags/flagPoland.png";
import flagPortugal from "@/assets/images/flags/flagPortugal.png";
import flagSingapore from "@/assets/images/flags/flagSingapore.png";
import flagFrance from "@/assets/images/flags/flagFrance.png";

import FormComponent from "./FormComponent";

const data = [
  {
    title: "What course do you plan on studying?",
    options: [
      { id: 1, name: "Computer Science & IT", icon: flagUsa },
      { id: 2, name: "English Literature", icon: flagUk },
      { id: 3, name: "Digital Arts", icon: flagAustralia },
      { id: 4, name: "Information Technology", icon: flagCanada },
      { id: 5, name: "Computer Engineering", icon: flagGermany },
      { id: 6, name: "Fine Arts", icon: flagPoland },
      { id: 7, name: "Literature & Creative Writing", icon: flagPortugal },
      { id: 8, name: "Computer Networking", icon: flagSingapore },
      { id: 9, name: "Digital Media", icon: flagFrance },
    ],
  },
];

const ThirdForm = () => {
  return (
    <div>
      {data.map((formData, index) => (
        <FormComponent
          key={index}
          title={formData.title}
          options={formData.options}
          firstItemName={"faculty"}
        />
      ))}
    </div>
  );
};

export default ThirdForm;
