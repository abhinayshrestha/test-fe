import React, { useState } from "react";

import flagUsa from "@/assets/images/flags/flagUsa.png";
import flagUk from "@/assets/images/flags/flagUk.png";
import flagAustralia from "@/assets/images/flags/flagAustralia.png";

import FormComponent from "./FormComponent";

const data = [
  {
    title: "which english test did you take?",
    options: [
      { id: 1, name: "IELTS", icon: flagUsa },
      { id: 2, name: "TOEFL", icon: flagUk },
      { id: 3, name: "PTE", icon: flagAustralia },
      { id: 3, name: "None", icon: flagAustralia },
    ],
    sliderOneTitle: "What was your Score  ? ",
    engTestScore: {
      min: 0,
      max: 9,
      defaultValue: 0,
      dataType: "number ",
    },
  },
];

const SixthForm = () => {
  return (
    <div>
      {data.map((formData, index) => (
        <FormComponent
          key={index}
          title={formData.title}
          options={formData.options}
          sliderOneTitle={formData.sliderOneTitle}
          sliderOneRange={formData.engTestScore}
          firstItemName={"engishTest"}
          secondItemName="Score"
        />
      ))}
    </div>
  );
};

export default SixthForm;
