import React from 'react';
import study from "@/assets/vectors/study.svg"

const Sidecard: React.FC = () => {
  return (
    <div className="bg-gradient-to-b from-blue-900 to-blue-700 rounded-xl shadow-lg p-6 xl:h-[60vh] xl:w-[48vh] ">
      <div className="flex justify-between items-center mb-4">
      
        <div>
          {/* <h2 className="text-3xl font-bold text-white">Choose Bhasika</h2> */}
        </div>
      </div>
      <img src={study} alt="StudySvg" />
    </div>
  );
};

export default Sidecard;
