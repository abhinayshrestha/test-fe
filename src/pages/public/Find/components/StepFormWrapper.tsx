import { Form } from "antd";
import React, { useContext, useMemo, useState } from "react";
import { Outlet } from "react-router-dom";

interface StepProps {
  form: any;
  optionSelected: any;
  setOptionSelectedd: (isActive: any) => void;
}

const StepFormContext = React.createContext<StepProps>({
  form: null,
  optionSelected: null,
  setOptionSelectedd: () => {},
});

const StepFormWrapper = () => {
  const [form] = Form.useForm();
  const [optionSelected, setOptionSelectedd] = useState(null);

  const providerValue = useMemo(
    () => ({
      form,
      optionSelected,
      setOptionSelectedd,
    }),
    [form, optionSelected, setOptionSelectedd]
  );

  return (
    <StepFormContext.Provider value={providerValue}>
      <Outlet />
    </StepFormContext.Provider>
  );
};

export const useStepFormContextData = () => {
  return useContext(StepFormContext);
};

export default StepFormWrapper;
