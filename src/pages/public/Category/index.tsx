import Layout, { Content, Footer } from "antd/es/layout/layout";
import CategoriesList from "./components/CategoriesList";

const index = () => {
  return (
    <div>
      <Layout>
        <Content>
          <CategoriesList />
        </Content>
      </Layout>
    </div>
  );
};

export default index;
