import React, { useEffect, useState } from "react";
import { Checkbox, Divider, Collapse, Card } from "antd";
import { useWindowSize } from "usehooks-ts";
import { IoLocationOutline } from "react-icons/io5";

import brihmingam from "@/assets/images/universities/brihmingam.jpg";
import centennial from "@/assets/images/universities/centennial.jpg";
import deakin from "@/assets/images/universities/deakin.jpg";
import melbourne from "@/assets/images/universities/melbourne.jpg";
import rmit from "@/assets/images/universities/myhc.jpg";
// import rmit from "@/assets/images/universities/rmit.png";
import tafe from "@/assets/images/universities/tafe.jpg";
import { useNavigate } from "react-router-dom";

interface Course {
  name: string;
  description: string;
  universityName: string;
  universityImage: string;
  country: string;
  category: string;
  degree: string;
  duration: string;
  feePerYear: number;
}

const CourseList: Course[] = [
  {
    name: "Computer Science",
    description:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Harum quas, soluta hic facere ut repellat porro cupiditate praesentium perferendis alias cumque facilis minima exercitationem eius accusantium atque perspiciatis magni possimus",
    universityName: "Harvard University",
    universityImage: brihmingam,
    country: "USA",
    category: "Computer and IT",
    degree: "Bachelors",
    duration: "4 years",
    feePerYear: 15000,
  },
  {
    name: "Business Administration",
    description:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Harum quas, soluta hic facere ut repellat porro cupiditate praesentium perferendis alias cumque facilis minima exercitationem eius accusantium atque perspiciatis magni possimus",
    universityName: "Stanford University",
    universityImage: centennial,
    country: "USA",
    category: "Business and Management",
    degree: "Masters",
    duration: "2 years",
    feePerYear: 20000,
  },
  {
    name: "Art History",
    description:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Atque, doloribus in sed maxime non soluta minima recusandae placeat nulla mollitia architecto optio eveniet a blanditiis nesciunt itaque quia magnam harum",
    universityName: "New York University",
    universityImage: melbourne,
    country: "USA",
    category: "Arts and Creativity",
    degree: "Masters",
    duration: "2 years",
    feePerYear: 18000,
  },
  {
    name: "Marketing",
    description:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Harum quas, soluta hic facere ut repellat porro cupiditate praesentium perferendis alias cumque facilis minima exercitationem eius accusantium atque perspiciatis magni possimus",
    universityName: "Melbourne University",
    universityImage: deakin,
    country: "Australia",
    category: "Business and Management",
    degree: "Bachelors",
    duration: "3 years",
    feePerYear: 17000,
  },
  {
    name: "Journalism",
    description:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Harum quas, soluta hic facere ut repellat porro cupiditate praesentium perferendis alias cumque facilis minima exercitationem eius accusantium atque perspiciatis magni possimus",
    universityName: "Michigan State University",
    universityImage: tafe,
    country: "USA",
    category: "Communications & Media",
    degree: "Masters",
    duration: "2 years",
    feePerYear: 19000,
  },
  {
    name: "Finance",
    description:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Harum quas, soluta hic facere ut repellat porro cupiditate praesentium perferendis alias cumque facilis minima exercitationem eius accusantium atque perspiciatis magni possimus",
    universityName: "Kellogg Business School",
    universityImage: rmit,
    country: "USA",
    category: "Business and Management",
    degree: "Masters",
    duration: "2 years",
    feePerYear: 22000,
  },
  {
    name: "Electrical Engineering",
    description:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Harum quas, soluta hic facere ut repellat porro cupiditate praesentium perferendis alias cumque facilis minima exercitationem eius accusantium atque perspiciatis magni possimus",
    universityName: "Harvard University",
    universityImage: brihmingam,
    country: "USA",
    category: "Engineering",
    degree: "Bachelors",
    duration: "4 years",
    feePerYear: 16000,
  },
  {
    name: "Graphic Design",
    description:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Harum quas, soluta hic facere ut repellat porro cupiditate praesentium perferendis alias cumque facilis minima exercitationem eius accusantium atque perspiciatis magni possimus",
    universityName: "Stanford University",
    universityImage: centennial,
    country: "USA",
    category: "Arts and Creativity",
    degree: "Bachelors",
    duration: "3 years",
    feePerYear: 17500,
  },
  {
    name: "International Business",
    description:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Harum quas, soluta hic facere ut repellat porro cupiditate praesentium perferendis alias cumque facilis minima exercitationem eius accusantium atque perspiciatis magni possimus",
    universityName: "New York University",
    universityImage: melbourne,
    country: "USA",
    category: "Business and Management",
    degree: "Masters",
    duration: "2 years",
    feePerYear: 19500,
  },
  {
    name: "Film Production",
    description:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Harum quas, soluta hic facere ut repellat porro cupiditate praesentium perferendis alias cumque facilis minima exercitationem eius accusantium atque perspiciatis magni possimus",
    universityName: "Melbourne University",
    universityImage: deakin,
    country: "Australia",
    category: "Arts and Creativity",
    degree: "Masters",
    duration: "2 years",
    feePerYear: 18500,
  },
  {
    name: "Psychology",
    description:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Harum quas, soluta hic facere ut repellat porro cupiditate praesentium perferendis alias cumque facilis minima exercitationem eius accusantium atque perspiciatis magni possimus",
    universityName: "Michigan State University",
    universityImage: tafe,
    country: "USA",
    category: "Social Sciences",
    degree: "Masters",
    duration: "2 years",
    feePerYear: 21000,
  },
  {
    name: "Fashion Design",
    description:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Harum quas, soluta hic facere ut repellat porro cupiditate praesentium perferendis alias cumque facilis minima exercitationem eius accusantium atque perspiciatis magni possimus",
    universityName: "Kellogg Business School",
    universityImage: rmit,
    country: "USA",
    category: "Arts and Creativity",
    degree: "Bachelors",
    duration: "3 years",
    feePerYear: 18000,
  },
  {
    name: "Computer Engineering",
    description:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Harum quas, soluta hic facere ut repellat porro cupiditate praesentium perferendis alias cumque facilis minima exercitationem eius accusantium atque perspiciatis magni possimus",
    universityName: "Harvard University",
    universityImage: brihmingam,
    country: "USA",
    category: "Engineering",
    degree: "Masters",
    duration: "2 years",
    feePerYear: 19500,
  },
  {
    name: "Music Production",
    description:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Harum quas, soluta hic facere ut repellat porro cupiditate praesentium perferendis alias cumque facilis minima exercitationem eius accusantium atque perspiciatis magni possimus",
    universityName: "Stanford University",
    universityImage: centennial,
    country: "USA",
    category: "Arts and Creativity",
    degree: "Masters",
    duration: "2 years",
    feePerYear: 18000,
  },
  {
    name: "Public Relations",
    description:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Harum quas, soluta hic facere ut repellat porro cupiditate praesentium perferendis alias cumque facilis minima exercitationem eius accusantium atque perspiciatis magni possimus",

    universityName: "New York University",
    universityImage: melbourne,
    country: "USA",
    category: "Communications & Media",
    degree: "Bachelors",
    duration: "3 years",
    feePerYear: 17000,
  },
  {
    name: "Software Engineering",
    description:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Harum quas, soluta hic facere ut repellat porro cupiditate praesentium perferendis alias cumque facilis minima exercitationem eius accusantium atque perspiciatis magni possimus",

    universityName: "Melbourne University",
    universityImage: deakin,
    country: "Australia",
    category: "Computer and IT",
    degree: "Masters",
    duration: "2 years",
    feePerYear: 20000,
  },
  {
    name: "Environmental Science",
    description:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Harum quas, soluta hic facere ut repellat porro cupiditate praesentium perferendis alias cumque facilis minima exercitationem eius accusantium atque perspiciatis magni possimus",
    universityName: "Michigan State University",
    universityImage: tafe,
    country: "USA",
    category: "Science and Technology",
    degree: "Bachelors",
    duration: "4 years",
    feePerYear: 16000,
  },
  {
    name: "Digital Marketing",
    description:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Harum quas, soluta hic facere ut repellat porro cupiditate praesentium perferendis alias cumque facilis minima exercitationem eius accusantium atque perspiciatis magni possimus",
    universityName: "Kellogg Business School",
    universityImage: rmit,
    country: "USA",
    category: "Business and Management",
    degree: "Bachelors",
    duration: "4 years",
    feePerYear: 17500,
  },
  {
    name: "Civil Engineering",
    description:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Harum quas, soluta hic facere ut repellat porro cupiditate praesentium perferendis alias cumque facilis minima exercitationem eius accusantium atque perspiciatis magni possimus",
    universityName: "Harvard University",
    universityImage: brihmingam,
    country: "USA",
    category: "Engineering",
    degree: "Masters",
    duration: "2 years",
    feePerYear: 19000,
  },
  {
    name: "Photography",
    description:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Harum quas, soluta hic facere ut repellat porro cupiditate praesentium perferendis alias cumque facilis minima exercitationem eius accusantium atque perspiciatis magni possimus",
    universityName: "Stanford University",
    universityImage: centennial,
    country: "USA",
    category: "Arts and Creativity",
    degree: "Bachelors",
    duration: "3 years",
    feePerYear: 16500,
  },
  {
    name: "Human Resource Management",
    description:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Harum quas, soluta hic facere ut repellat porro cupiditate praesentium perferendis alias cumque facilis minima exercitationem eius accusantium atque perspiciatis magni possimus",
    universityName: "New York University",
    universityImage: melbourne,
    country: "USA",
    category: "Business and Management",
    degree: "Masters",
    duration: "2 years",
    feePerYear: 19500,
  },
  {
    name: "Computer Networking",
    description:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Harum quas, soluta hic facere ut repellat porro cupiditate praesentium perferendis alias cumque facilis minima exercitationem eius accusantium atque perspiciatis magni possimus",
    universityName: "Melbourne University",
    universityImage: deakin,
    country: "Australia",
    category: "Computer and IT",
    degree: "Masters",
    duration: "2 years",
    feePerYear: 20500,
  },
  {
    name: "Social Work",
    description:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Harum quas, soluta hic facere ut repellat porro cupiditate praesentium perferendis alias cumque facilis minima exercitationem eius accusantium atque perspiciatis magni possimus",
    universityName: "Michigan State University",
    universityImage: tafe,
    country: "USA",
    category: "Social Sciences",
    degree: "Bachelors",
    duration: "4 years",
    feePerYear: 17000,
  },
  {
    name: "Digital Art",
    description:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Harum quas, soluta hic facere ut repellat porro cupiditate praesentium perferendis alias cumque facilis minima exercitationem eius accusantium atque perspiciatis magni possimus",
    universityName: "Kellogg Business School",
    universityImage: rmit,
    country: "USA",
    category: "Arts and Creativity",
    degree: "Masters",
    duration: "2 years",
    feePerYear: 18500,
  },
  {
    name: "Biomedical Engineering",
    description:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Harum quas, soluta hic facere ut repellat porro cupiditate praesentium perferendis alias cumque facilis minima exercitationem eius accusantium atque perspiciatis magni possimus",
    universityName: "Harvard University",
    universityImage: brihmingam,
    country: "USA",
    category: "Engineering",
    degree: "Masters",
    duration: "2 years",
    feePerYear: 19500,
  },
  {
    name: "Creative Writing",
    description:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Harum quas, soluta hic facere ut repellat porro cupiditate praesentium perferendis alias cumque facilis minima exercitationem eius accusantium atque perspiciatis magni possimus",
    universityName: "Stanford University",
    universityImage: centennial,
    country: "USA",
    category: "Arts and Creativity",
    degree: "Bachelors",
    duration: "3 years",
    feePerYear: 16000,
  },
  {
    name: "Data Analysis",
    description:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Harum quas, soluta hic facere ut repellat porro cupiditate praesentium perferendis alias cumque facilis minima exercitationem eius accusantium atque perspiciatis magni possimus",
    universityName: "New York University",
    universityImage: melbourne,
    country: "USA",
    category: "Computer and IT",
    degree: "Masters",
    duration: "2 years",
    feePerYear: 20000,
  },
  {
    name: "Environmental Engineering",
    description:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Harum quas, soluta hic facere ut repellat porro cupiditate praesentium perferendis alias cumque facilis minima exercitationem eius accusantium atque perspiciatis magni possimus",
    universityName: "Melbourne University",
    universityImage: deakin,
    country: "Australia",
    category: "Engineering",
    degree: "Masters",
    duration: "2 years",
    feePerYear: 21000,
  },
  {
    name: "Sociology",
    description:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Harum quas, soluta hic facere ut repellat porro cupiditate praesentium perferendis alias cumque facilis minima exercitationem eius accusantium atque perspiciatis magni possimus",
    universityName: "Michigan State University",
    universityImage: tafe,
    country: "USA",
    category: "Social Sciences",
    degree: "Bachelors",
    duration: "4 years",
    feePerYear: 17500,
  },
  {
    name: "UX/UI Design",
    description:
      "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Harum quas, soluta hic facere ut repellat porro cupiditate praesentium perferendis alias cumque facilis minima exercitationem eius accusantium atque perspiciatis magni possimus",
    universityName: "Kellogg Business School",
    universityImage: rmit,
    country: "USA",
    category: "Computer and IT",
    degree: "Bachelors",
    duration: "4 years",
    feePerYear: 18000,
  },
];

const { Panel } = Collapse;

const CategoriesList: React.FC = () => {
  const { width = 0 } = useWindowSize();
  const [courseList, setCourseList] = useState<Course[]>(CourseList);

  const [filters, setFilters] = useState<{
    universityName: string[];
    category: string[];
    country: string[];
    degree: string[];
    feePerYear: string[];
  }>({
    universityName: [],
    category: [],
    country: [],
    degree: [],
    feePerYear: [],
  });

  const [activekeys, setActiveKeys] = useState(
    width > 768 ? [1, 2, 3, 4, 5] : []
  );

  const navigate = useNavigate();

  const navigateCourse = () => {
    navigate(`/course`);
  };

  useEffect(() => {
    if (width > 768) {
      setActiveKeys([1, 2, 3, 4, 5]);
    } else {
      setActiveKeys([]);
    }
  }, [width]);

  const filterOptions = {
    universityName: [
      "Harvard University",
      "Stanford University",
      "New York University",
      "Melbourne University",
      "Michigan State University",
      "Kellogg Business School",
    ],
    category: [
      "Computer and IT",
      "Business and Management",
      "Arts and Creativity",
      "Communications & Media",
      "Engineering",
      "Psychology",
    ],
    country: ["USA", "Australia"],
    degree: ["Bachelors", "Masters"],
    feePerYear: [
      "$15000-$17000",
      "$17001-$19000",
      "$19001-$21000",
      "$21001-$23000",
    ],
  };

  const getFeeRange = (fee: number) => {
    if (fee <= 17000) {
      return "$15000-$17000";
    }
    if (fee <= 19000) {
      return "$17001-$19000";
    }
    if (fee <= 21000) {
      return "$19001-$21000";
    }
    return "$21001-$23000";
  };

  useEffect(() => {
    const filteredCourses = CourseList.filter((course) => {
      return (
        (filters.universityName.length === 0 ||
          filters.universityName.includes(course.universityName)) &&
        (filters.category.length === 0 ||
          filters.category.includes(course.category)) &&
        (filters.country.length === 0 ||
          filters.country.includes(course.country)) &&
        (filters.degree.length === 0 ||
          filters.degree.includes(course.degree)) &&
        (filters.feePerYear.length === 0 ||
          filters.feePerYear.includes(getFeeRange(course.feePerYear)))
      );
    });
    setCourseList(filteredCourses);
  }, [filters]);

  const handleCheckboxChange = (
    key: keyof typeof filters,
    value: string,
    checked: boolean
  ) => {
    if (checked) {
      setFilters((prevFilters) => ({
        ...prevFilters,
        [key]: [...prevFilters[key], value],
      }));
    } else {
      setFilters((prevFilters) => ({
        ...prevFilters,
        [key]: prevFilters[key].filter((item) => item !== value),
      }));
    }
  };

  return (
    <div className=" md:mx-auto md:container flex flex-col md:flex-row md:gap-4 gap-8 md:my-4 my-4 ">
      <div className="bg-white rounded-lg shadow-md md:mb-0 pb-0 h-[100%]">
        <h2 className="text-2xl font-bold text-primary py-4 text-center mx-28">
          Filter
        </h2>
        <div className="p-6 flex flex-wrap">
          <Collapse
            accordion={false}
            className="bg-white text-sm rounded-md flex flex-col flex-grow"
            activeKey={activekeys}
            bordered={false}
            onChange={(value: any) => setActiveKeys(value)}
          >
            {/* Panels for different filter options */}
            {/* University Name Panel */}
            <Panel
              header={<span className="text-black font-bold"> University</span>}
              key="1"
            >
              {filterOptions.universityName.map((option, index) => (
                <div key={index} className="flex flex-wrap">
                  <Checkbox
                    className="mb-2"
                    onChange={(e) =>
                      handleCheckboxChange(
                        "universityName",
                        option,
                        e.target.checked
                      )
                    }
                  >
                    {option}
                  </Checkbox>
                </div>
              ))}
            </Panel>
            {/* Category Panel */}
            <Panel
              header={<span className="text-black font-bold"> Category</span>}
              key="2"
            >
              {filterOptions.category.map((option, index) => (
                <div key={index} className="flex flex-wrap">
                  <Checkbox
                    className="mb-2"
                    onChange={(e) =>
                      handleCheckboxChange("category", option, e.target.checked)
                    }
                  >
                    {option}
                  </Checkbox>
                </div>
              ))}
            </Panel>
            {/* Country Panel */}
            <Panel
              header={<span className="text-black font-bold"> Country</span>}
              key="3"
            >
              {filterOptions.country.map((option, index) => (
                <div key={index} className="flex flex-wrap">
                  <Checkbox
                    className="mb-2"
                    onChange={(e) =>
                      handleCheckboxChange("country", option, e.target.checked)
                    }
                  >
                    {option}
                  </Checkbox>
                </div>
              ))}
            </Panel>
            {/* Degree Panel */}
            <Panel
              header={<span className="text-black font-bold"> Degree</span>}
              key="4"
            >
              {filterOptions.degree.map((option, index) => (
                <div key={index} className="flex flex-wrap">
                  <Checkbox
                    className="mb-2"
                    onChange={(e) =>
                      handleCheckboxChange("degree", option, e.target.checked)
                    }
                  >
                    {option}
                  </Checkbox>
                </div>
              ))}
            </Panel>
            {/* Fee Per Year Panel */}
            <Panel
              header={
                <span className="text-black font-bold"> Fee Per Year</span>
              }
              key="5"
            >
              {filterOptions.feePerYear.map((option, index) => (
                <div key={index} className="flex flex-wrap">
                  <Checkbox
                    className="mb-2"
                    onChange={(e) =>
                      handleCheckboxChange(
                        "feePerYear",
                        option,
                        e.target.checked
                      )
                    }
                  >
                    {option}
                  </Checkbox>
                </div>
              ))}
            </Panel>
          </Collapse>
        </div>
      </div>

      <div className="rounded-lg  space-y-4 space-x-4 py-4 px-4">
        <h2 className="text-2xl font-bold  text-primary  py-4 px-6 ">
          Available Courses
        </h2>
        <div className="bg-white rounded-md py-2 px-2 ">
          <h2 className="text-md font-bold  text-gray-700  ">
            {" "}
            Applied Filters :
          </h2>
        </div>
        {/*  card div start */}
        <div className="">
          {courseList.map((course, index) => (
            <Card
              key={index}
              className="flex items-center  lg:flex-row flex-col space-x-4 py-2 md:px-6 px-2  gap-y-4 mb-4 cursor-pointer"
              onClick={navigateCourse}
              cover={
                <img
                  alt={course.name}
                  src={course.universityImage}
                  // className="h-[12vh] md:w-[45vh]  w-[25vh] "
                  className=" xl:h-[12vh] xl:w-[45vh] lg:h-[12vh] lg:w-[70vh] mt-4"
                />
              }
            >
              <div className="space-y-2">
                <h3 className="text-xl text-primary font-bold">
                  {course.category}
                </h3>
                <p className="text-lg text-gray-700 font-bold">
                  {course.universityName}
                </p>
                <p>{course.description}</p>
                <div className="flex items-center">
                  <div className="flex flex-row mr-4 bg-blue-100 rounded-lg p-1">
                    <IoLocationOutline className="font-bold text-black text-xl" />
                    <p className="font-semibold text-black">{course.country}</p>
                  </div>
                  <div style={{ marginLeft: "4px" }} />
                  <p className="text-sm text-orange-600 bg-blue-100 p-1 inline-flex rounded-lg font-bold">
                    <span>${course.feePerYear}</span>
                  </p>
                </div>
              </div>
            </Card>
          ))}
        </div>
        {/*  card div ends */}
      </div>
    </div>
  );
};
export default CategoriesList;
