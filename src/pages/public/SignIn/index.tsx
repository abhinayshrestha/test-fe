import { Button, Col, Form, Image, Input, Row, message } from "antd";
import Password from "antd/es/input/Password";

import signInHuman from "@/assets/vectors/sign-in.svg";
import Logo from "@/core/components/Logo";
import { ISignInRequest } from "@/schema/auth.schema";
import { useAuth } from "@/providers/AuthProvider";
import TokenService from "@/services/token-storage";
import { protectedRoutePaths } from "@/router";

import { useSignUserIn } from "./api/queries";

const SignIn = () => {
  const { mutate: signUserIn, isLoading: isSigningUserIn } = useSignUserIn();

  const [signInForm] = Form.useForm();

  const { setAuthUser } = useAuth();

  const handleFormFinish = (values: ISignInRequest) => {
    signUserIn(values, {
      onSuccess: (res) => {
        if (res.access_token) {
          // sign in was successful
          setAuthUser({ email: res.userEmail });
          TokenService.setAccessToken(res.access_token);
          TokenService.setRefreshToken(res.refresh_token);
          window.location.replace(protectedRoutePaths.serviceProviders);
        } else {
          message.error("Sign in failed, please try again later");
        }
      },
    });
  };

  return (
    <div className="flex items-center justify-center min-h-screen p-3 900:gap-12">
      <div className="flex flex-col items-center p-5 rounded-md gap-6 shadow">
        <Logo width={150} />

        <Form
          layout="vertical"
          className="500:max-w-[350px]"
          form={signInForm}
          onFinish={handleFormFinish}
        >
          <Row>
            <Col span={24}>
              <Form.Item
                name="email"
                label="Email"
                rules={[
                  {
                    required: true,
                    message: "Please enter your email address",
                  },
                ]}
              >
                <Input />
              </Form.Item>
            </Col>

            <Col span={24}>
              <Form.Item
                name="password"
                label="Password"
                rules={[
                  {
                    required: true,
                    message: "Please enter your password",
                  },
                ]}
              >
                <Password />
              </Form.Item>
            </Col>

            <Col>
              <Form.Item className="mb-3 -mt-3">
                <Button type="link" className="px-1">
                  Forgot Your Password ?
                </Button>
              </Form.Item>
            </Col>

            <Col span={24}>
              <Form.Item className="mb-0">
                <Button
                  type="primary"
                  className="w-full"
                  htmlType="submit"
                  loading={isSigningUserIn}
                >
                  Sign In
                </Button>
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </div>

      <Image
        src={signInHuman}
        alt="sign in human"
        preview={false}
        height={350}
        className="hidden 900:block"
      />
    </div>
  );
};

export default SignIn;
