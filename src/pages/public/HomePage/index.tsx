import CourseCategoryCards from "@/pages/public/HomePage/components/CourseCategories";
import { Divider, Layout } from "antd";
import { Content, Footer, Header } from "antd/es/layout/layout";
import Country from "./components/Country";
import InstititeCarousel from "./components/InstitutesCarousel";
import StudentReview from "./components/StudentReview";
import OurMission from "./components/OurMission";
import HowItWorksSection from "./components/HowItWorks";
import Hero from "./components/Hero";

const HomePage = () => {
  return (
    <Layout>
      <Content>
        <Hero />
        <HowItWorksSection />
        <Country />
        <CourseCategoryCards />
        <InstititeCarousel />
        <OurMission />
        <StudentReview />
      </Content>
    </Layout>
  );
};

export default HomePage;
