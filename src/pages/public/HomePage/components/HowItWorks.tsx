import {
  CheckOutlined,
  FormOutlined,
  PhoneOutlined,
  SearchOutlined,
} from "@ant-design/icons/lib/icons";
import React from "react";

const HowItWorksSection: React.FC = () => {
  const items = [
    {
      id: 1,
      icon: <SearchOutlined />,
      title: "Find a Course",
      description:
        "Discover your desired course amidst a selection of 1000 diverse options from various countries.",
    },
    {
      id: 2,
      icon: <FormOutlined />,
      title: "Fill Basic Questions",
      description:
        "Fill out your basic info and  the about course and country you're interested in to further ease your process. ",
    },
    {
      id: 3,
      icon: <PhoneOutlined />,
      title: "Get a Call",
      description:
        "Receive a call from our representative who will guide you through the process and address any your inquiries .",
    },
    {
      id: 4,
      icon: <CheckOutlined />,
      title: "Process Your Application",
      description:
        "Start the initiation of your application process for your selected course and preferred country with ease.",
    },
  ];

  return (
    <div className="py-10 bg-white">
      <div className="container mx-auto px-4 py-8 md:space-y-14  space-x-8">
        <div className="relative  px-4 ">
          <h2 className="text-2xl text-center text-primary font-bold mb-14">
            How Study Best works
          </h2>
          <h2 className="md:mb-20 mb-10 text-primary text-4xl md:text-4xl  text-center font-bold">
            We are there with you at every step
          </h2>
          <div className="flex flex-wrap ">
            {items.map((item, index) => (
              <div key={item.id} className="w-full md:w-1/2 lg:w-1/4 p-4">
                <div className="text-center">
                  <div className="relative z-10 bg-white w-12 h-12 mb-8 mx-auto border border-gray-200 rounded-full ">
                    <div className="absolute left-1/2 top-1/2 transform -translate-x-1/2 -translate-y-1/2 text-3xl text-primary  ">
                      {item.icon}
                    </div>
                    {index !== items.length - 1 && (
                      <div className="hidden xl:block absolute left-12 top-1/2 transform -translate-y-1/2 w-[35vh]  h-px bg-gray-400" />
                    )}
                  </div>
                  <div className="md:max-w-xs mx-auto">
                    <h3 className="mb-4 font-heading text-xl font-bold font-heading ">
                      {item.title}
                    </h3>
                    <p className="text-gray-600 font-medium leading-relaxed">
                      {item.description}
                    </p>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default HowItWorksSection;
