import { Carousel } from "antd";
import brihmingam from "@/assets/images/universities/brihmingam.jpg";
import centennial from "@/assets/images/universities/centennial.jpg";
import deakin from "@/assets/images/universities/deakin.jpg";
import melbourne from "@/assets/images/universities/melbourne.jpg";
import myhc from "@/assets/images/universities/myhc.jpg";
import rmit from "@/assets/images/universities/rmit.png";
import tafe from "@/assets/images/universities/tafe.jpg";
import trobe from "@/assets/images/universities/trobe.jpg";
import styled from "styled-components";

export const StyledCarousel = styled(Carousel)`
  .slick-dots li button {
    width: 10px !important;
    height: 3px !important;
    border-radius: 10px !important;
    background: blue !important;
    margin-top: 30px;
  }
  .slick-dots li.slick-active button {
    width: 20px !important;
    height: 4px !important;
    border-radius: 10px !important;
    background: #002e6d !important;
  }
`;

const universities = [
  { id: 1, name: "Brihmingam", image: brihmingam },
  { id: 2, name: "Centennial", image: centennial },
  { id: 3, name: "Deakin", image: deakin },
  { id: 4, name: "Melbourne", image: melbourne },
  { id: 5, name: "MyHC", image: myhc },
  { id: 6, name: "RMIT", image: rmit },
  { id: 7, name: "TAFE", image: tafe },
  { id: 8, name: "Trobe", image: trobe },
];

const InstititeCarousel: React.FC = () => {
  return (
    <div className="py-14 space-y-14">
      <h2 className="text-2xl text-center text-primary font-bold ">
        Partnered Universities
      </h2>
      <StyledCarousel
        autoplay
        className="mx-auto container mb-8"
        dots={{ className: "mt-4" }}
        dotPosition="bottom"
        slidesToShow={6}
        autoplaySpeed={5000}
        infinite
        responsive={[
          {
            breakpoint: 600,
            settings: { slidesToShow: 2 },
          },
          {
            breakpoint: 768,
            settings: { slidesToShow: 3 },
          },
          {
            breakpoint: 1024,
            settings: { slidesToShow: 4 },
          },
          {
            breakpoint: 1280,
            settings: { slidesToShow: 6 },
          },
        ]}
      >
        {universities.map((university, index) => (
          <div key={index} className="px-2 pb-10">
            <div className="border border-gray-100 rounded-lg">
              <img
                src={university.image}
                className="w-full h-full object-cover rounded-lg shadow-4xl "
                alt={university.name}
                // style={{ margin: "8px" }}
              />
            </div>
          </div>
        ))}
      </StyledCarousel>
    </div>
  );
};

export default InstititeCarousel;
