import React from "react";
import { Link } from "react-router-dom";
import { ArrowRightOutlined, VideoCameraOutlined } from "@ant-design/icons";

import thegirls from "@/assets/images/thegirls.png";
import SearchComponent from "./SearchComponent";

// import SearchComponent from "./SearchComponent";

const Hero: React.FC = () => {
  return (
    <div className=" bg-stone px-2 pt-2 pb-8 mb-0 ">
      <div className=" grid  lg:grid-cols-12 lg:gap-8  xl:gap-0 container mx-auto px-4 mt-4  space-x-24">
        <div className="  place-self-center py-4 lg:col-span-7">
          <h1 className="mb-4 max-w-2xl text-4xl font-extrabold leading-none md:text-5xl xl:text-6xl text-primary">
            Find out your dream country to study.
          </h1>
          <p className="mb-6 max-w-2xl font-light md:text-lg lg:mb-8 lg:text-xl text-black">
            Select from a wide range of courses and pursue studies overseas,
            receiving expert guidance along the way.
          </p>
          <Link
            to="/find"
            className="transform hover:scale-95 bg-primary mb-2 hover:bg-indigo-800 mr-4 inline-flex items-center justify-center rounded-lg px-5 py-3 text-center text-base font-medium text-white focus:ring-4"
          >
            Find my dream country
            <ArrowRightOutlined className="text-primary ml-4 bg-white rounded-full  p-2 inline-block " />
          </Link>
          <Link
            to="hi"
            className=" bg-white hover:scale-95 inline-flex items-center justify-center rounded-lg border border-gray-500 px-5 py-3 text-center text-base font-medium text-gray-900  hover:border-primary "
          >
            Book Virtual Counselling
            <VideoCameraOutlined className="ml-4 mr-0 text-xl text-primary  inline-block mx-0 " />
          </Link>
        </div>
        <div className=" hidden lg:col-span-5 lg:mt-0 lg:flex  ">
          <img src={thegirls} alt="girl" className="h-[100%] w-[80%]" />
        </div>
      </div>
      {/* <div className="mx-auto bg-indigo-700 container rounded-lg ">
        <SearchComponent />
      </div>{" "} */}
    </div>
  );
};

export default Hero;
