import library from "@/assets/images/library.webp";
import student from "@/assets/images/student.webp";

const OurMission = () => {
  return (
    <div className="pb-10 bg-white px-2">
      <div className="container mx-auto px-2 py-8 space-y-14">
        <div className="grid lg:grid-cols-2 xl:grid-cols-2 md:grid-cols-1 sm:grid-cols-1 gap-4 xl:h-[600px] lg:h-[450px]">
          <div className="flex justify-center items-center">
            <div className=" relative">
              <div className="absolute xl:top-24 xl:left-24 lg:top-16 lg:left-16 md:top-14 md:left-14 sm:top-14 sm:left-14 xl:w-96 lg:w-80 md:w-64 sm:w-64 w-52 top-10 left-10">
                <img
                  alt="Student"
                  src={student}
                  className=" xl:h-96 lg:h-80 md:h-64 sm:h-64 h-52 rounded-xl object-cover"
                />
              </div>
              <div className=" xl:w-96 lg:w-80 md:w-64 sm:w-64 w-52">
                <img
                  alt="Library"
                  src={library}
                  className="xl:h-96 lg:h-80 md:h-64 sm:h-64 h-52 rounded-xl object-cover"
                />
              </div>
            </div>
          </div>
          <div className="flex  justify-center flex-col space-y-6 text-center lg:text-start xl:text-start">
            <h2 className="text-2xl  text-primary font-bold mt-16 ">
              Our Mission
            </h2>{" "}
            <p className="font-light text-gray-700 ">
              At Study Best Feed, our mission is to empower students worldwide
              by providing a comprehensive platform that simplifies the daunting
              task of choosing the right university and courses tailored to
              their unique preferences and chosen location. We strive to
              revolutionize the education search process by offering detailed
              information, including fees, syllabus, and other essential
              details, ensuring transparency and clarity. By leveraging
              cutting-edge technology and data-driven insights, we aim to guide
              students towards making informed decisions that align with their
              academic aspirations and career goals. Our commitment lies in
              facilitating access to quality education and empowering the next
              generation of leaders to unlock their full potential and thrive in
              their chosen fields.
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default OurMission;
