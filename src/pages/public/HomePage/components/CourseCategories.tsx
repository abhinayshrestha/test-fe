import { useNavigate } from "react-router-dom";
import { Card, Typography } from "antd";
import { FaBuilding, FaMusic } from "react-icons/fa";
import { GiFarmer } from "react-icons/gi";
import { PiMountainsFill } from "react-icons/pi";
import { AiFillCalculator, AiFillMedicineBox } from "react-icons/ai";
import { GrCloudComputer } from "react-icons/gr";
import { IoLanguageSharp } from "react-icons/io5";
import { FaComputer, FaPeopleArrows, FaPeopleLine } from "react-icons/fa6";
import { GoLaw } from "react-icons/go";
import {
  MdSportsBaseball,
  MdBusinessCenter,
  MdVideoCameraFront,
} from "react-icons/md";

const courseCategories = [
  {
    id: 1,
    name: "Quantitative & Mathematics",
    icon: <AiFillCalculator />,
  },
  {
    id: 2,
    name: "Athletics & Physical Education",
    icon: <MdSportsBaseball />,
  },
  {
    id: 3,
    name: "Medical Studies & Wellness",
    icon: <AiFillMedicineBox />,
  },
  {
    id: 4,
    name: "Corporate Management & Leadership",
    icon: <MdBusinessCenter />,
  },
  {
    id: 5,
    name: "Music & Theatrical Arts",
    icon: <FaMusic />,
  },
  {
    id: 6,
    name: "Information Technology & Computing",
    icon: <FaComputer />,
  },
  {
    id: 7,
    name: "Linguistics & Global Studies",
    icon: <IoLanguageSharp />,
  },
  {
    id: 8,
    name: "Humanities & Social Sciences",
    icon: <FaPeopleArrows />,
  },
  {
    id: 9,
    name: "Engineering & Applied Technology",
    icon: <GrCloudComputer />,
  },
  {
    id: 10,
    name: "Environmental Sciences & Agriculture",
    icon: <GiFarmer />,
  },
  {
    id: 11,
    name: "Architectural Design & Urban Planning",
    icon: <FaBuilding />,
  },
  {
    id: 12,
    name: "Media Production & Communications",
    icon: <MdVideoCameraFront />,
  },
  {
    id: 13,
    name: "Legal Studies & Criminal Justice",
    icon: <GoLaw />,
  },
  {
    id: 14,
    name: "Education & Community Outreach",
    icon: <FaPeopleLine />,
  },
  {
    id: 15,
    name: "Travel Industry & Hospitality Management",
    icon: <PiMountainsFill />,
  },
];

const CourseCategoryCards: React.FC = () => {
  const navigate = useNavigate();

  const handleCategoryClick = () => {
    navigate("/categories");
  };

  return (
    <div className="text-center px-4 py-14 space-y-14 bg-white">
      <h2 className="text-2xl text-center text-primary font-bold">
        Course Categories
      </h2>
      <div className="grid grid-cols-2 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-5 gap-y-6 gap-x-6 container mx-auto">
        {courseCategories.map((category) => (
          <Card
            key={category.id}
            onClick={handleCategoryClick}
            hoverable
            style={{ padding: 0 }}
            className="bg-gray-50  flex justify-center border shadow-lg hover:shadow-xl hover:scale-95 transition duration-300 ease-in-out rounded-2xl text-black"
          >
            <div className="flex flex-col items-center justify-start space-y-4">
              <Typography className="text-4xl mb-2 text-primary ">
                {category.icon}
              </Typography>
              <Typography className=" text-primary font-semibold">
                {category.name}
              </Typography>
            </div>
          </Card>
        ))}
      </div>
    </div>
  );
};
export default CourseCategoryCards;
