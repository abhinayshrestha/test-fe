import australia from "@/assets/images/country/australia.jpg";
import canada from "@/assets/images/country/canada.jpg";
import germany from "@/assets/images/country/germany.jpg";
import india from "@/assets/images/country/india.jpg";
import russia from "@/assets/images/country/russia.jpg";
import uae from "@/assets/images/country/uae.jpg";
import uk from "@/assets/images/country/uk.jpg";
import usa from "@/assets/images/country/usa.jpg";
import { Link } from "react-router-dom";

const countries = [
  { name: "Australia", image: australia },
  { name: "Canada", image: canada },
  { name: "Germany", image: germany },
  { name: "India", image: india },
  { name: "Russia", image: russia },
  { name: "UAE", image: uae },
  { name: "England", image: uk },
  { name: "USA", image: usa },
];

const Country = () => {
  return (
    <div className="py-10 ">
      <div className="container mx-auto px-4 py-8 space-y-14">
        <h2 className=" text-2xl text-center text-primary font-bold">
          Where would you like to pursue your studies ?
        </h2>
        <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4">
          {countries.map((country, index) => (
            <div
              key={index}
              className="relative overflow-hidden rounded-lg aspect-w-1 aspect-h-1"
            >
              {/* <Link to={`/country/${country.name}`}> */}
              <Link to={`/country`}>
                <div className="group h-full">
                  <img
                    alt={country.name}
                    src={country.image}
                    className="w-full h-full object-cover transition duration-600 transform group-hover:scale-105 filter brightness-125"
                  />

                  <div className="absolute inset-0 flex items-center justify-center opacity-0 group-hover:opacity-100 transition-opacity duration-300 bg-black bg-opacity-50">
                    <p className="text-white text-2xl font-bold">
                      {country.name}
                    </p>
                  </div>
                </div>
              </Link>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Country;
