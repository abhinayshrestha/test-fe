import { Carousel } from "antd";
import styled from "styled-components";

export const StyledCarousel = styled(Carousel)`
  .slick-dots li button {
    width: 10px !important;
    height: 3px !important;
    border-radius: 10px !important;
    background: blue !important;
    margin-top: 30px;
  }
  .slick-dots li.slick-active button {
    width: 20px !important;
    height: 4px !important;
    border-radius: 10px !important;
    background: #002e6d !important;
  }
`;

const StudentReview = () => {
  const reviews = [
    {
      id: 1,
      name: "Anna Smith",
      university: "Harvard University (USA)",
      avatar: "https://tecdn.b-cdn.net/img/Photos/Avatars/img%20%2810%29.jpg",
      text: "Study Best Feed simplified my search, connecting me with the ideal university and course effortlessly. Highly recommend for stress-free decision-making! Study Best Feed simplified my search, connecting me with the ideal university and course effortlessly. Highly recommend for stress-free decision-making! Study Best Feed simplified my search, connecting me with the ideal university and course effortlessly. Highly recommend for stress-free decision-making! Study Best Feed simplified my search, connecting me with the ideal university and course effortlessly. Highly recommend for stress-free decision-making! ",
    },
    {
      id: 2,
      name: "Tanna Smith",
      university: "University of Oxford (UK)",
      avatar: "https://tecdn.b-cdn.net/img/Photos/Avatars/img%20%2810%29.jpg",
      text: "Navigating Study Best Feed was intuitive, and I quickly found the perfect degree program. The testimonials were invaluable in confirming my choice.",
    },
    {
      id: 3,
      name: "Rihana Smith",
      university: "Stanford University (USA)",
      avatar: "https://tecdn.b-cdn.net/img/Photos/Avatars/img%20%2810%29.jpg",
      text: "Study Best Feed's platform streamlined my university search. The testimonials provided genuine insights that helped me confidently choose my path",
    },
    {
      id: 4,
      name: "Rahana Smith",
      university: "Massachusetts Institute of Technology (USA)",
      avatar: "https://tecdn.b-cdn.net/img/Photos/Avatars/img%20%2810%29.jpg",
      text: "Thanks to Study Best Feed, I found the best-fit university and course with ease. The testimonials reassured me of my  decision decision decision.",
    },
    {
      id: 5,
      name: "Rihina Smith",
      university: "University of Cambridge (UK)",
      avatar: "https://tecdn.b-cdn.net/img/Photos/Avatars/img%20%2810%29.jpg",
      text: "Study Best Feed's user-friendly interface made finding my dream university stress-free. The testimonials provided useful perspectives from fellow students.",
    },
    {
      id: 6,
      name: "Rihina Smith",
      university: "California Institute of Technology (USA)",
      avatar: "https://tecdn.b-cdn.net/img/Photos/Avatars/img%20%2810%29.jpg",
      text: "Study Best Feed's comprehensive database helped me discover the perfect education path. The testimonials were incredibly helpful in making my final decision.",
    },
  ];

  return (
    <div className="py-10">
      <div className="container mx-auto px-4 py-8 space-y-14 ">
        <h2 className="text-2xl text-center text-primary font-bold">
          What students have to say about us?
        </h2>

        <StyledCarousel
          autoplay
          dots
          dotPosition="bottom"
          slidesToShow={3}
          slidesToScroll={1}
          responsive={[
            {
              breakpoint: 1280,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                infinite: true,
                dots: true,
              },
            },
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                infinite: true,
                dots: true,
              },
            },
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true,
                dots: true,
              },
            },
            {
              breakpoint: 640,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true,
                dots: true,
              },
            },
          ]}
        >
          {reviews.map((review) => (
            <div key={review.id} className=" p-2 pb-10">
              {/* Testimonial */}
              <div className="flex flex-row rounded-lg p-8 shadow-lg border bg-white h-64">
                <div className="mx-auto  flex w-1/6   items-end justify-center">
                  <img
                    src={review.avatar}
                    className="rounded-full shadow-md "
                    alt="Avatar"
                  />
                </div>
                <div className="ms-6 flex flex-col justify-between w-5/6">
                  <div className=" overflow-auto mb-6 custom-scrollbar">
                    <p className=" font-light text-gray-700 pr-2">
                      <span className="text-xl font-bold">&ldquo; </span>
                      {review.text}
                      <span className="text-xl font-bold"> &rdquo;</span>
                    </p>
                  </div>
                  <div>
                    <p className="mb-2 font-semibold text-gray-900">
                      {review.name}
                    </p>
                    <p className="text-gray-600 text-[0.70rem]">
                      {review.university}
                    </p>
                  </div>
                </div>
              </div>
            </div>
          ))}
        </StyledCarousel>
      </div>
    </div>
  );
};

export default StudentReview;
