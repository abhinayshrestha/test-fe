import React from "react";
import { Input, Button } from "antd";

const SearchComponent: React.FC = () => {
  return (
    <div className="container mx-auto p-6 gap-x-4 grid grid-cols-1 md:grid-cols-4 gap-4">
      <Input className="" placeholder="Search 1" />
      <Input placeholder="Search 2" />
      <Input placeholder="Search 3" />
      <Button type="primary" className="bg-white text-black">
        Search
      </Button>
    </div>
  );
};

export default SearchComponent;
