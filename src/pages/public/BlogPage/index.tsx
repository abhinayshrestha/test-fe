import Layout, { Content, Footer } from "antd/es/layout/layout";
import PageContent from "./components/PageContent";

const BlogsPage = () => {
  return (
    <div>
      <Layout>
        <Content>
          <PageContent />
        </Content>
      </Layout>
    </div>
  );
};

export default BlogsPage;
