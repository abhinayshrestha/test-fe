import React from "react";
import { Link } from "react-router-dom";
import { Card } from "antd";
import {
  FieldTimeOutlined,
  CommentOutlined,
  ProfileOutlined,
} from "@ant-design/icons";
import blog1 from "@/assets/images/blog/blog1.jpg";
import blog2 from "@/assets/images/blog/blog2.jpg";
import blog3 from "@/assets/images/blog/blog3.jpg";

const RelatedPostsData = [
  {
    title: "Study in Australia: Top Universities and Scholarships",
    description:
      "Learn about the visa process, admission requirements, and tips for studying in the USA. Learn about the visa process, admission requirements, and tips for studying in the USA. Learn about the visa process, admission requirements, and tips for studying in the USALearn about the visa process, admission requirements, and tips for studying in the USA Learn about the visa process, admission requirements, and tips for studying in the USALearn about the visa process, admission requirements, and tips for studying in the USA",
    image: blog1,
    time: "6 mins ago",
    comments: 39,
    category: "Australia",
    author: "Sabin Pandey",
  },
  {
    title: "Guide to Studying in the USA: Visa  and Admission Tips",
    description:
      "Learn about the visa process, admission requirements, and tips for studying in the USA. Learn about the visa process, and tips for studying in the USA. Learn about the visa process, admission requirements, and tips for studying in the USALearn about the visa process, admission requirements, and tips for studying in the USA Learn about the visa process, admission requirements, and tips for studying in the USALearn about the visa process, admission requirements, and tips for studying in the USA",
    image: blog2,
    time: "10 days ago",
    comments: 0,
    category: "USA",
    author: "Dibesh Babu Shah",
  },
  {
    title: "Studying in the UK: Popular Courses and Student Life",
    description:
      "Discover popular courses, student life, and cultural experiences in the UK. Learn about the visa process, admission requirements, and tips for studying in the USA.  Learn about the visa process, admission requirements, and tips for studying in the USA.Learn about the visa process, admission requirements, and tips for studying in the USALearn about the visa process, admission requirements, and tips for studying in the USA",
    image: blog3,
    time: "16 hours ago",
    comments: 9,
    category: "UK",
    author: "Gaurab Bhandari",
  },
  {
    title: "Studying in the UK: Popular Courses and Student Life",
    description:
      "Discover popular courses, student life, and cultural experiences in the UK. Learn about the visa process, admission requirements, and tips for studying in the USA.  Learn about the visa process, admission requirements, and tips for studying in the USA.Learn about the visa process, admission requirements, and tips for studying in the USALearn about the visa process, admission requirements, and tips for studying in the USA",
    image: blog3,
    time: "16 hours ago",
    comments: 9,
    category: "Poland",
    author: "Pratik Bhandari",
  },
];

const RelatedPosts = () => {
  return (
    <div className="flex flex-wrap">
      {RelatedPostsData.map((post, index) => (
        <Card
          hoverable
          key={index}
          className="m-2 rounded-lg shadow  border-[1px] border-indigo-200 p-0 "
        >
          <img
            src={post.image}
            alt={post.title}
            className="rounded-t-lg mb-2"
          />

          <h3 className="sm:text-lg text-base text-primary font-semibold mb-3">
            {post.title}
          </h3>
          <div className="flex items-center mb-3 text-black">
            <FieldTimeOutlined className="mr-1" />
            <span className="text-xs">{post.time}</span>
            <CommentOutlined className="ml-4 mr-1" />
            <span className="text-xs">{post.comments} Comments</span>
          </div>
        </Card>
      ))}
    </div>
  );
};

export default RelatedPosts;
