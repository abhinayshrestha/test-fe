import React, { useState } from "react";
import { Breadcrumb, Button, Modal } from "antd";
import {
  BookOutlined,
  CommentOutlined,
  HomeOutlined,
  ShareAltOutlined,
  UnorderedListOutlined,
} from "@ant-design/icons";

import blog1 from "@/assets/images/blog/blog1.jpg";
import avatar from "@/assets/images/avatar.png";
import RelatedPosts from "./RelatedPosts";
import ShareBlog from "./ShareBlog";

const staticData = [
  {
    title: "Study in Australia: Top Universities and Scholarships",
    description:
      "Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolor, saepe odit? Quae aliquid cum odio vel atque. Illum qui ducimus fugit blanditiis minus ex eius, magnam quo? Odio molestiae iste cumque modi error recusandae odit repellat quis asperiores dolorum, corporis ea. In ea temporibus facilis at iure odit neque dicta mollitia accusantium qui. Voluptatum, commodi id. Numquam, suscipit ab veniam asperiores deleniti alias doloremque sed sint quisquam nulla soluta pariatur porro dignissimos. Assumenda alias explicabo cupiditate magni, nobis quo ea dignissimos quidem inventore, harum laboriosam dolor consectetur natus blanditiis architecto fuga aspernatur omnis ad ipsam aut libero voluptatum atque hic sapiente. Ad dolorum iusto labore corrupti delectus fugiat quos eum aperiam quod asperiores et doloremque blanditiis doloribus mollitia reprehenderit ab quaerat, voluptatem non sit fuga vero. Itaque eligendi eaque optio culpa exercitationem mollitia cumque quam aperiam? Sapiente possimus quae minus? Voluptates alias aspernatur delectus laborum, laudantium facere corporis, sed animi labore vero doloremque molestias debitis aperiam numquam pariatur eligendi, aliquid qui vel maiores inventore itaque porro! Facere sunt sapiente amet dignissimos veritatis nam, laborum inventore suscipit culpa aut, ipsum dolore fuga at asperiores. Sed voluptates accusantium hic eius consequatur iste maxime dicta iure dignissimos repellendus optio dolorum, veritatis ducimus porro. Modi possimus, atque nobis dignissimos non explicabo repellendus consequuntur quia quam cumque ducimus, veritatis rerum eaque inventore quo adipisci ipsam in? Ea explicabo exercitationem cumque voluptas nesciunt veniam laboriosam voluptatem asperiores tempora iure assumenda quod quisquam, id dignissimos sint earum doloremque. Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dolor, saepe odit? Quae aliquid cum odio vel atque. Illum qui ducimus fugit blanditiis minus ex eius, magnam quo? Odio molestiae iste cumque modi error recusandae odit repellat quis asperiores dolorum, corporis ea. In ea temporibus facilis at iure odit neque dicta mollitia accusantium qui. Voluptatum, commodi id. Numquam, suscipit ab veniam asperiores deleniti alias doloremque sed sint quisquam nulla soluta pariatur porro dignissimos. Assumenda alias explicabo cupiditate magni, nobis quo ea dignissimos quidem inventore, harum laboriosam dolor consectetur natus blanditiis architecto fuga aspernatur omnis ad ipsam aut libero voluptatum atque hic sapiente. Ad dolorum iusto labore corrupti delectus fugiat quos eum aperiam quod asperiores et doloremque blanditiis doloribus mollitia reprehenderit ab quaerat, voluptatem non sit fuga vero. Itaque eligendi eaque optio culpa exercitationem mollitia cumque quam aperiam? Sapiente possimus quae minus? Voluptates alias aspernatur delectus laborum, laudantium facere corporis, sed animi labore vero doloremque molestias debitis aperiam numquam pariatur eligendi, aliquid qui vel maiores inventore itaque porro! Facere sunt sapiente amet dignissimos veritatis nam, laborum inventore suscipit culpa aut, ipsum dolore fuga at asperiores. Sed voluptates accusantium hic eius consequatur iste maxime dicta iure dignissimos repellendus optio dolorum, veritatis ducimus porro. Modi possimus, atque nobis dignissimos non explicabo repellendus consequuntur quia quam cumque ducimus, veritatis rerum eaque inventore quo adipisci ipsam in? Ea explicabo exercitationem cumque voluptas nesciunt veniam laboriosam voluptatem asperiores tempora iure assumenda quod quisquam, id dignissimos sint earum doloremque. Temporibus maiores consectetur alias vitae voluptate Temporibus maiores consectetur alias vitae voluptates porro quibusdam quam.",
    image: blog1,
    time: "6 mins ago",
    authorAvatar: avatar,
    comments: 39,
    category: "Australia",
    author: "Sabin Pandey",
  },
];

const PageContent = () => {
  const [isModalOpen, setIsModalOpen] = useState(false);

  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleOk = () => {
    setIsModalOpen(false);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  // const isAffixed = (affixed: any) => {
  //   if (affixed) {
  //     setIsScrolled(true);
  //   } else if (!isAffixed) {
  //     setIsScrolled(false);
  //   }
  //   console.log(` Scolled: ${isScrolled}`);
  // };

  return (
    <div className="bg-stone ">
      <div className="lg:container mx-auto md:px-8 flex flex-col lg:flex-row  ">
        {/* Blog Content */}
        <div className="w-full lg:w-3/4 md:pr-8 mb-8">
          {staticData.map((item, index) => (
            <div
              key={index}
              className="mx-auto max-w-5xl bg-white p-12 rounded-lg mb-8"
            >
              {/* Share Button */}
              <div className="text-center mb-6">
                <Button
                  onClick={showModal}
                  className="bg-primary text-white px-8 py-3 rounded-full flex items-center justify-center hover:bg-indigo-800  "
                >
                  <ShareAltOutlined />
                  <span className="ml-2">Share</span>
                </Button>
              </div>
              {/* Blog Content */}
              <div>
                <Breadcrumb
                  separator={<span className="mx-2">/</span>}
                  className="mb-6"
                >
                  <Breadcrumb.Item>
                    <HomeOutlined />
                    <span className="ml-2">Home</span>
                  </Breadcrumb.Item>
                  <Breadcrumb.Item>
                    <BookOutlined />
                    <span className="ml-2">Blogs</span>
                  </Breadcrumb.Item>
                  <Breadcrumb.Item>
                    <BookOutlined />
                    <span className="ml-2">Article</span>
                  </Breadcrumb.Item>
                </Breadcrumb>
                <h1 className="text-3xl lg:text-4xl font-bold text-primary text-left mb-6">
                  {item.title}
                </h1>
                <div className="flex items-center mb-6">
                  <img
                    src={item.authorAvatar}
                    alt="Author"
                    className="w-10 h-10 rounded-full mr-3"
                  />
                  <div>
                    <h3 className="text-lg font-semibold">{item.author}</h3>
                    <p className="text-gray-900">{item.time}</p>
                  </div>
                </div>
                <div className="flex items-center mb-6">
                  <strong className="mr-2">
                    <UnorderedListOutlined />
                  </strong>
                  <p className="mr-6 text-gray-800">{item.category}</p>
                  <strong className="mr-2">
                    <CommentOutlined />
                  </strong>
                  <p className="text-gray-800">{item.comments}</p>
                </div>
                <img
                  src={blog1}
                  alt="Article"
                  className="w-full rounded-lg mb-6"
                />
                {/* <p className="text-gray-700 text-lg font-sans">
                  {item.description}
                </p> */}

                <div className="space-y-8">
                  <p className="text-gray-800 text-[18px] font-sans leading-9  tracking-normal	">
                    {" "}
                    Bachelor in Business Administration (BBA) in India is
                    affordable, with plenty of specializations and career
                    outcomes. It is one of the most popular subjects given its
                    flexibility- students from any background can study BBA.
                    There are many reasons why students choose to study BBA in
                    India. One of the reasons is its affordability. Bachelor in
                    Business Administration (BBA) costs around INR 50,000 to INR
                    5 Lakhs. Additionally, there are many options for students
                    to pick from. Third on this list is the placement packages
                    offered by colleges with handsome salaries. There are both
                    private and government BBA colleges in India. Both private
                    and public colleges teach students about finance,
                    administration, business, leadership, teamwork, management,
                    and problem-solving. This BBA guide brings you everything
                    you need to know about BBA in India. Read till the end to
                    find out about the BBA entrance exams, eligibility criteria,
                    best BBA Colleges, the scope of BBA, Bachelor in Business
                    Administration (BBA) colleges in different states of India
                    and so on.{" "}
                  </p>
                  <h2 className="font-bold text-gray-700 text-2xl">
                    {" "}
                    Highlights of BBA In USA
                  </h2>
                  <p className="text-gray-800 text-[18px] font-sans leading-9  tracking-normal">
                    {" "}
                    These are vital points students enquire about doing an MBA
                    in India.
                    <br />
                    <br />
                    <ol type="1" className=" space-y-4 ml-2">
                      <li>
                        1. Bachelor in Business Administration (BBA) is
                        generally a 3-year undergraduate course.
                      </li>
                      <li>
                        2. Many management colleges in India also offer an
                        integrated BBA+MBA/ IPM Program, a 5-year graduate
                        program.
                      </li>
                      <li>
                        3. IPM Programme stands for Integrated Programme in
                        Management.
                      </li>
                      <li>
                        4. The examinations take place in a semester pattern
                        (Twice every year).
                      </li>
                      <li>
                        5. There are both private and public BBA Colleges. The
                        fees of private BBA colleges are higher than those of
                        public BBA Colleges.
                      </li>
                      <li>
                        6. Many top BBA Colleges require students to appear in
                        the Entrance exams to get selected. However, some
                        colleges offer direct BBA admissions without entrance
                        exams.
                      </li>
                      <li>
                        7. There are more than 15 BBA specializations that
                        students can pick from.
                      </li>
                      <li>
                        8. Students from any stream (science, management or
                        humanities) can study BBA.
                      </li>
                      <li> 9.Top BBA colleges provide placement offers.</li>
                    </ol>
                  </p>

                  <h2 className="font-bold text-gray-700 text-2xl">
                    {" "}
                    Eligibiity Criteria
                  </h2>
                  <p className="text-gray-800 text-[18px] font-sans leading-9  tracking-normal	">
                    {" "}
                    Bachelor in Business Administration (BBA) in India is
                    affordable, with plenty of specializations and career
                    outcomes. It is one of the most popular subjects given its
                    flexibility- students from any background can study BBA.
                    There are many reasons why students choose to study BBA in
                    India. One of the reasons is its affordability. Bachelor in
                    Business Administration (BBA) costs around INR 50,000 to INR
                    5 Lakhs. Additionally, there are many options for students
                    to pick from. Third on this list is the placement packages
                    offered by colleges with handsome salaries. There are both
                    private and government BBA colleges in India. Both private
                    and public colleges teach students about finance,
                    administration, business, leadership, teamwork, management,
                    and problem-solving. This BBA guide brings you everything
                    you need to know about BBA in India. Read till the end to
                    find out about the BBA entrance exams, eligibility criteria,
                    best BBA Colleges, the scope of BBA, Bachelor in Business
                    Administration (BBA) colleges in different states of India
                    and so on.{" "}
                  </p>

                  <h2 className="font-bold text-gray-700 text-2xl">
                    {" "}
                    Points to consider before travelling
                  </h2>
                  <p className="text-gray-800 text-[18px] font-sans leading-9  tracking-normal">
                    {" "}
                    Points to be considered
                    <br />
                    <br />
                    <ol type="1" className=" space-y-4 ml-2">
                      <li>
                        1. Bachelor in Business Administration (BBA) is
                        generally a 3-year undergraduate course.
                      </li>
                      <li>
                        2. Many management colleges in India also offer an
                        integrated BBA+MBA/ IPM Program, a 5-year graduate
                        program.
                      </li>
                      <li>
                        3. IPM Programme stands for Integrated Programme in
                        Management.
                      </li>
                      <li>
                        4. The examinations take place in a semester pattern
                        (Twice every year).
                      </li>
                      <li>
                        5. There are both private and public BBA Colleges. The
                        fees of private BBA colleges are higher than those of
                        public BBA Colleges.
                      </li>
                      <li>
                        6. Many top BBA Colleges require students to appear in
                        the Entrance exams to get selected. However, some
                        colleges offer direct BBA admissions without entrance
                        exams.
                      </li>
                      <li>
                        7. There are more than 15 BBA specializations that
                        students can pick from.
                      </li>
                      <li>
                        8. Students from any stream (science, management or
                        humanities) can study BBA.
                      </li>
                      <li> 9.Top BBA colleges provide placement offers.</li>
                    </ol>
                  </p>

                  <h2 className="font-bold text-gray-700 text-2xl">
                    {" "}
                    Entrace Exam
                  </h2>
                  <p className="text-gray-800 text-[18px] font-sans leading-9  tracking-normal	">
                    {" "}
                    The BBA entrance exams inspect the students verbal ability,
                    reading comprehension skills, quantitative skills, data
                    interpretation skills, analytical skills, basic management
                    skills and problem-solving skills. Different boards conduct
                    different entrance exams. Cracking these entrance exams is
                    crucial for getting admission into your desired BBA college.
                    Before starting to prepare, you need to know which BBA exam
                    test is accepted by the college you want to pursue so that
                    you can prepare accordingly. The BBA entrance exams inspect
                    the students verbal ability, reading comprehension skills,
                    quantitative skills, data interpretation skills, analytical
                    skills, basic management skills and problem-solving skills.
                    Different boards conduct different entrance exams. Cracking
                    these entrance exams is crucial for getting admission into
                    your desired BBA college. Before starting to prepare, you
                    need to know which BBA exam test is accepted by the college
                    you want to pursue so that you can prepare accordingly.
                  </p>
                </div>
              </div>
            </div>
          ))}
        </div>
        {/* Related Posts */}
        <div className=" rounded-lg lg:w-[40vh] gap-2 mr-2 lg:pt-6 ">
          <h2 className="text-2xl font-semibold mb-2 ml-4 text-primary">
            Related Posts
          </h2>
          <RelatedPosts />
        </div>

        <Modal
          className="w-[35vh] text-center "
          title="Share On.... "
          open={isModalOpen}
          onOk={handleOk}
          onCancel={handleCancel}
          footer={null}
        >
          <ShareBlog />
        </Modal>
      </div>
    </div>
  );
};

export default PageContent;
