import { Button, message } from "antd";
import {
  FacebookShareButton,
  TwitterShareButton,
  InstapaperShareButton,
  FacebookMessengerShareButton,
  EmailShareButton,
  LinkedinShareButton,
  WhatsappShareButton,
  ViberShareButton,
  FacebookIcon,
  TwitterIcon,
  LinkedinIcon,
  WhatsappIcon,
  EmailIcon,
  ViberIcon,
  FacebookShareCount,
} from "react-share";

import { CopyOutlined } from "@ant-design/icons";

const ShareBlog = () => {
  const link = window.location.href;

  const shareButtonProps = {
    windowWidth: window.innerWidth,
    windowHeight: window.innerHeight,
    url: link,
    className: "hover:scale-[1.10]",
    onShareWindowClose: () => console.log("hoooo"),
  };

  const copyToClipboard = () => {
    navigator.clipboard
      .writeText(link)
      .then(() => message.success("Link copied to clipboard!"))
      .catch((error) => console.error("Error copying link: ", error));
  };
  return (
    <div className="gap-y-4 gap-x-4">
      <div className="gap-x-4 gap-y-4 flex flex-wrap text-6xl m-4 justify-center ">
        <FacebookShareButton {...shareButtonProps}>
          <FacebookIcon size={32} round />
        </FacebookShareButton>
        <TwitterShareButton {...shareButtonProps}>
          <TwitterIcon size={32} round />
        </TwitterShareButton>
        <WhatsappShareButton {...shareButtonProps}>
          <WhatsappIcon size={32} round />
        </WhatsappShareButton>
        <ViberShareButton {...shareButtonProps}>
          <ViberIcon size={32} round />
        </ViberShareButton>
        <LinkedinShareButton {...shareButtonProps}>
          <LinkedinIcon size={32} round />
        </LinkedinShareButton>
        <FacebookShareCount url={link}>
          {(shareCount) => (
            <span className="myShareCountWrapper">{shareCount}</span>
          )}
        </FacebookShareCount>
      </div>
      <h3 className="font-semibold mb-4"> OR</h3>
      <Button
        onClick={copyToClipboard}
        className="bg-primary rounded-md w-full"
      >
        <CopyOutlined className="text-white font-medium text-balance" />{" "}
        <span className="text-white font-medium  tracking-wide">
          {" "}
          Copy Link{" "}
        </span>
      </Button>
    </div>
  );
};

export default ShareBlog;
