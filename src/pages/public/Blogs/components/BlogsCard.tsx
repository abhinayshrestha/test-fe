import React, { useEffect, useState } from "react";
import { Card, Row, Col, Select, Button } from "antd";
import { Link, useNavigate, useLocation, useParams } from "react-router-dom";
import { BsPersonCheck } from "react-icons/bs";

import {
  FieldTimeOutlined,
  CommentOutlined,
  ProfileOutlined,
} from "@ant-design/icons";

import blog1 from "@/assets/images/blog/blog1.jpg";
import blog2 from "@/assets/images/blog/blog2.jpg";
import blog3 from "@/assets/images/blog/blog3.jpg";

const staticData = [
  {
    title: "Study in Australia: Top Universities and Scholarships",
    description:
      "Learn about the visa process, admission requirements, and tips for studying in the USA. Learn about the visa process, admission requirements, and tips for studying in the USA. Learn about the visa process, admission requirements, and tips for studying in the USALearn about the visa process, admission requirements, and tips for studying in the USA Learn about the visa process, admission requirements, and tips for studying in the USALearn about the visa process, admission requirements, and tips for studying in the USA",
    image: blog1,
    time: "6 mins ago",
    comments: 39,
    category: "Australia",
    author: "Sabin Pandey",
  },
  {
    title: "Guide to Studying in the USA: Visa  and Admission Tips",
    description:
      "Learn about the visa process, admission requirements, and tips for studying in the USA. Learn about the visa process, and tips for studying in the USA. Learn about the visa process, admission requirements, and tips for studying in the USALearn about the visa process, admission requirements, and tips for studying in the USA Learn about the visa process, admission requirements, and tips for studying in the USALearn about the visa process, admission requirements, and tips for studying in the USA",
    image: blog2,
    time: "10 days ago",
    comments: 0,
    category: "USA",
    author: "Dibesh Babu Shah",
  },
  {
    title: "Studying in the UK: Popular Courses and Student Life",
    description:
      "Discover popular courses, student life, and cultural experiences in the UK. Learn about the visa process, admission requirements, and tips for studying in the USA.  Learn about the visa process, admission requirements, and tips for studying in the USA.Learn about the visa process, admission requirements, and tips for studying in the USALearn about the visa process, admission requirements, and tips for studying in the USA",
    image: blog3,
    time: "16 hours ago",
    comments: 9,
    category: "UK",
    author: "Gaurab Bhandari",
  },
  {
    title: "Studying in the UK: Popular Courses and Student Life",
    description:
      "Discover popular courses, student life, and cultural experiences in the UK. Learn about the visa process, admission requirements, and tips for studying in the USA.  Learn about the visa process, admission requirements, and tips for studying in the USA.Learn about the visa process, admission requirements, and tips for studying in the USALearn about the visa process, admission requirements, and tips for studying in the USA",
    image: blog3,
    time: "16 hours ago",
    comments: 9,
    category: "Poland",
    author: "Pratik Bhandari",
  },

  {
    title: "Studying in the UK: Popular Courses and Student Life",
    description:
      "Discover popular courses, student life, and cultural experiences in the UK. Learn about the visa process, admission requirements, and tips for studying in the USA.  Learn about the visa process, admission requirements, and tips for studying in the USA.Learn about the visa process, admission requirements, and tips for studying in the USALearn about the visa process, admission requirements, and tips for studying in the USA",
    image: blog3,
    time: "16 hours ago",
    comments: 9,
    category: "Finland",
    author: "Dibesh Babu Shah",
  },

  {
    title: "Studying in the UK: Popular Courses and Student Life",
    description:
      "Discover popular courses, student life, and cultural experiences in the UK. Learn about the visa process, admission requirements, and tips for studying in the USA.  Learn about the visa process, admission requirements, and tips for studying in the USA.Learn about the visa process, admission requirements, and tips for studying in the USALearn about the visa process, admission requirements, and tips for studying in the USA",
    image: blog2,
    time: "16 hours ago",
    comments: 9,
    category: "Russia",
    author: "Sabin Pandey",
  },

  {
    title: "Studying in the UK: Popular Courses and Student Life",
    description:
      "Discover popular courses, student life, and cultural experiences in the UK. Learn about the visa process, admission requirements, and tips for studying in the USA.  Learn about the visa process, admission requirements, and tips for studying in the USA.Learn about the visa process, admission requirements, and tips for studying in the USALearn about the visa process, admission requirements, and tips for studying in the USA",
    image: blog2,
    time: "16 hours ago",
    comments: 9,
    category: "Ukraine",
    author: "Gaurab Bhandari",
  },
];

const blogData = Array(7).fill(staticData).flat();

const { Meta } = Card;

const BlogsCard = () => {
  const navigate = useNavigate();
  const { category: categoryParam } = useParams();
  const [openTab, setOpenTab] = useState("All");

  useEffect(() => {
    if (categoryParam) {
      setOpenTab(categoryParam);
    }
  }, [categoryParam]);

  const handleTabClick = (category: string) => {
    setOpenTab(category);
    if (category === "All") {
      navigate("/blogs");
    } else {
      navigate(`/blogs/${category.toLowerCase()}`);
    }
  };

  const filteredBlogData =
    openTab === "All"
      ? blogData
      : blogData.filter((item) => item.category === openTab);

  const categories = ["All", ...new Set(blogData.map((item) => item.category))];

  return (
    <div className="bg-stone">
      <div className="mx-auto md:container p-5 sm:p-10 md:p-8 ">
        <div className="flex mb-12 list-none flex-wrap  pb-2  flex-row  justify-center ">
          {categories.map((category, index) => (
            <Button
              key={index}
              className={` text-md  text-sm lg:w-[10%] xl:h-[5vh]  font-bold uppercase px-2  shadow  block leading-normal m-1 sm:my-2  rounded-xl  ${
                openTab === category
                  ? "text-white bg-primary"
                  : "text-primary bg-white"
              }`}
              onClick={() => handleTabClick(category)}
            >
              {category}
            </Button>
          ))}
        </div>
        <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-x-4 gap-y-4  lg:gap-x-8 lg:gap-y-8">
          {filteredBlogData.map((item, index) => (
            <Link key={index} to={`/blogs/article1`} className="block">
              <Card
                className="relative rounded-lg bg-white shadow hover:border-indigo-100 hover:scale-[.98] transition-all duration-200 "
                hoverable
                cover={
                  <div className="relative">
                    <img alt={item.title} src={item.image} className="w-full" />
                    <div className="absolute top-2 right-2 bg-primary text-white font-semibold px-2 py-1 rounded-lg shadow-lg ">
                      {item.category}
                    </div>
                  </div>
                }
              >
                <h2 className="text-primary font-bold text-xl mb-4">
                  {item.title}
                </h2>

                <p>
                  <FieldTimeOutlined />
                  &nbsp; {item.time}
                </p>
                <p>
                  <CommentOutlined />
                  &nbsp; {item.comments} Comments
                </p>
                <p>
                  <ProfileOutlined />
                  &nbsp;Author: {item.author}
                </p>
              </Card>
            </Link>
          ))}
        </div>
      </div>
    </div>
  );
};

export default BlogsCard;
