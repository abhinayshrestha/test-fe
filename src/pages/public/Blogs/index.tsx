import Layout, { Content, Footer } from "antd/es/layout/layout";
import BlogsCard from "./components/BlogsCard";

const Blogs = () => {
  return (
    <div>
      <Layout>
        <Content>
          <BlogsCard />
        </Content>
      </Layout>
    </div>
  );
};

export default Blogs;
