import { Layout } from "antd";
import { Content, Footer } from "antd/es/layout/layout";
import CountryBanner from "./components/CountryBanner";
import UniversityDetail from "./components/University";

const University = () => {
  return (
    <Layout>
      <Content>
        <CountryBanner />
        <UniversityDetail />
      </Content>
    </Layout>
  );
};

export default University;
