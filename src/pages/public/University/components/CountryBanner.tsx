import australia from "@/assets/images/country/australia.jpg";

const CountryBanner: React.FC  = ( ) => {
  return (
    <div className="relative bg-gray-200 mb-6">
      <img
        src={australia}
        alt="Country "
        className="w-full h-auto max-h-72 object-cover "
        style={{ filter: 'brightness(0.7)' }}
      />
      <div className="absolute inset-0 flex items-center justify-center ">
        <div className="text-center">
          <h2 className="text-4xl font-bold text-white">Australia</h2>
          {/* <p className="text-sm">{currentUrlSlug}</p> */}
        </div>
      </div>
    </div>
  );
};

export default CountryBanner;
