import {
  CloseOutlined,
  EnvironmentOutlined,
  PlusOutlined,
} from "@ant-design/icons";
import { Button } from "antd";

import brihmingam from "@/assets/images/universities/brihmingam.jpg";
import centennial from "@/assets/images/universities/centennial.jpg";
import deakin from "@/assets/images/universities/deakin.jpg";
import melbourne from "@/assets/images/universities/melbourne.jpg";
import myhc from "@/assets/images/universities/myhc.jpg";
import rmit from "@/assets/images/universities/rmit.png";
import tafe from "@/assets/images/universities/tafe.jpg";
import trobe from "@/assets/images/universities/trobe.jpg";
import { Link, useNavigate } from "react-router-dom";

const universities = [
  { id: 1, name: "Brihmingam", image: brihmingam },
  { id: 2, name: "Centennial", image: centennial },
  { id: 3, name: "Deakin", image: deakin },
  { id: 4, name: "Melbourne", image: melbourne },
  { id: 5, name: "MyHC", image: myhc },
  { id: 6, name: "RMIT", image: rmit },
  { id: 7, name: "TAFE", image: tafe },
  { id: 8, name: "Trobe", image: trobe },
];

const cities = [
  { id:1, name: "Canbera"},
  { id:1, name: "Melbourne"},
  { id:1, name: "Brisbane"},
  { id:1, name: "Perth"},
  { id:1, name: "Adelaide"},
];

const degrees = [
  { id: 1, name: "Bachelor"},
  { id: 1, name: "Masters"},
  { id: 1, name: "Phd"},
  { id: 1, name: "Diploma"},
]

const UniversityDetail: React.FC = () => {
  const navigate = useNavigate();

  const navigateUniversity = () => {
    navigate(`university-details`);
  };

  return (
    <div className="py-2 ">
      <div className="container mx-auto px-4 py-8 space-y-14">
        <div className="grid xl:grid-cols-4 lg:grid-cols-4 md:grid-cols-4 grid-cols-1 xl:gap-4 lg:gap-4 md:gap-4 gap-y-4">
          {/* left side div */}
          <div className="border p-4 rounded-lg shadow-md bg-white ">
            <div className="">
              <h3 className="text-sm font-bold mb-2 border-b py-3">Filters</h3>
              <div>
                <h3 className="text-sm font-semibold mb-2 ">Cities</h3>

                <div className="mb-4 border-b py-2 flex flex-wrap">
                  {cities.map((city)=>(
                    <span className="rounded-lg mr-2 mb-2 border p-2 cursor-pointer" key={city.id}>
                    <span>
                      <PlusOutlined className="bg-blue-600  p-1 text-white rounded-full mr-2 " />
                    </span>
                    {city.name}
                  </span>
                  ))}
                  
                </div>

                <h3 className="text-sm font-semibold mb-2 ">Degree</h3>
                <div className="mb-4 border-b py-2 flex flex-wrap">
                  
                  {degrees.map((degree)=>(
                    <span className="rounded-lg mr-2 mb-2 border p-2 cursor-pointer" key={degree.id}>
                    <span>
                      <PlusOutlined className="bg-blue-600 p-1 text-white rounded-full mr-2 " />
                    </span>
                    {degree.name}
                  </span>
                  ))}
                  
                </div>
              </div>
            </div>
          </div>

          {/* right side div */}
          <div className="border col-span-3 bg-white p-2 shadow-md rounded-lg">
            <p className="mb-4 border-b px-4 py-2 space-y-2">
              <span className="mr-4 p-2 px-4 rounded-sm  bg-blue-500  text-white">
                Applied Filter :
              </span>
              <span className="inline-block bg-gray-200 rounded-full px-2 py-1 mr-2">
                <CloseOutlined className="mr-2 cursor-pointer bg-red-600 text-white p-1 rounded-full" />
                Bachelor
              </span>
              <span className="inline-block bg-gray-200 rounded-full px-2 py-1 mr-2">
                <CloseOutlined className="mr-2 cursor-pointer bg-red-600 text-white p-1 rounded-full" />
                Canbera
              </span>
              <span className="inline-block bg-gray-200 rounded-full px-2 py-1 mr-2">
                <CloseOutlined className="mr-2 cursor-pointer bg-red-600 text-white p-1 rounded-full" />
                Masters
              </span>
            </p>

            <div className="p-4">
              <h3 className="text-2xl font-semibold mb-2 ">University</h3>
              <div className=" grid xl:grid-cols-4 lg:grid-cols-3 md:grid-cols-2 sm:grid-cols-2 grid-cols-1 mx-2 ">
                {universities.map((university, index) => (
                  <div
                    key={index}
                    className="w-full sm:w-64 md:w-56 lg:w-52 xl:w-52 2xl:w-60 flex flex-col items-center my-2 lg:m-4 md:m-4 sm:m-4 xl:m-2 border p-4 shadow-md rounded-lg cursor-pointer"
                    onClick={navigateUniversity}
                  >
                    <img
                      src={university.image}
                      className="w-full h-full object-cover rounded-lg mb-2"
                      alt={university.name}
                    />
                    <div className="text-center mt-2 p-2">
                      <p className=" text-gray-800">
                        <EnvironmentOutlined className="mr-1"/> {university.name}
                      </p>
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default UniversityDetail;
