import { BookOutlined, HomeOutlined } from "@ant-design/icons";
import { Breadcrumb, Button } from "antd";
import { useNavigate } from "react-router-dom";

interface ConsultancyCardProps {
  onApplyClick: () => void;
}

const PageHeader: React.FC<ConsultancyCardProps> = ({ onApplyClick }) => {

  const handleApplyNow = () => {
    onApplyClick();
  };

  return (
    <div className=" relative bg-white">
      <div className="items-start py-16 p-4 space-y-8 bg-cover bg-center mx-auto container ">
        <Breadcrumb
          separator={<span style={{ color: "black" }}>/</span>}
          className="bg-indigo-100 ml-2 rounded-md p-2 text-xs inline-flex text-black "
        >
          <Breadcrumb.Item>
            <HomeOutlined />
            <span>Home</span>
          </Breadcrumb.Item>

          <Breadcrumb.Item>
            <BookOutlined />
            <span>Country</span>
          </Breadcrumb.Item>

          <Breadcrumb.Item className="text-black">
            <BookOutlined />
            <span>University-Details</span>
          </Breadcrumb.Item>
        </Breadcrumb>
        <div className="flex-col justify-between space-y-6">
          <div className="text-primary font-bold text-6xl">
            Adelphi University
          </div>

          {/* <Button
            className=" bg-blue-600 text-white rounded-md hover:bg-blue-800 cursor-pointer "
            onClick={handleApplyNow}
          >
            Book On Call Counseling
          </Button> */}
        </div>
      </div>
    </div>
  );
};

export default PageHeader;
