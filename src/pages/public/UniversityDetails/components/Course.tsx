import { useNavigate } from "react-router-dom";

interface CourseContent {
  id: number;
  name: string;
  description: string;
  location: string;
  fees: string;
}

interface CourseProps {
  courses: CourseContent[];
}

const Course: React.FC<CourseProps> = ({ courses }) => {
  const navigate = useNavigate();

  const navigateCourse = () => {
    navigate(`/course`);
  };

  return (
    <div className="">
      {courses.map((course, index) => (
        <div
          className="border border-gray-200 p-4 rounded-md shadow-md mb-2 cursor-pointer"
          onClick={navigateCourse}
          key={index}
        >
          <h2 className="text-lg font-semibold mb-2">{course.name}</h2>
          <p className="text-sm text-gray-600 mb-2">{course.description}</p>
          <div className="flex justify-between text-sm">
            <div>
              <strong>Location:</strong> {course.location}
            </div>
            <div>
              <strong>Fees:</strong> {course.fees}
            </div>
          </div>
        </div>
      ))}
    </div>
  );
};

export default Course;
