import { Typography } from "antd";
import adelphi from "@/assets/images/adelphi.png";

const { Title, Paragraph } = Typography;

const AboutSection: React.FC = () => {
  return (
    <div className=" py-12 bg-gray-100">
      <div className="container mx-auto px-4  lg:space-x-60 500:space-x-10  flex flex-col  md:flex-row">
        <div className="flex flex-col items-start md:w-1/2">
          <Title level={2} className="text-primary">
            Know More
          </Title>
          <Paragraph className="text-gray-900">
            Adelphi graduates stand out from — and often out-earn — their peers.
            Our unwavering focus on your success means individualized attention,
            immersive learning opportunities, important connections and
            award-winning career support.
          </Paragraph>
          <Paragraph className="text-gray-900">Country: USA</Paragraph>
          <Paragraph className="text-gray-900">
            Proficiency Test: IELTS, PTE
          </Paragraph>
        </div>
        <div className="xl:w-[30%] ">
          <img src={adelphi} alt="adelphi" />
        </div>
      </div>
    </div>
  );
};

export default AboutSection;
