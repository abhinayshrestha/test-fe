import { Table } from "antd";

const dataSource = [
  {
    key: "1",
    intake: "September / October",
    deadline: "January 15, 2025",
  },
  {
    key: "2",
    intake: "Rolling Admissions",
    deadline: "No fixed deadline varies by university",
  },
];

const columns = [
  {
    title: "Intake",
    dataIndex: "intake",
    key: "intake",
  },
  {
    title: "Deadline",
    dataIndex: "deadline",
    key: "deadline",
  },
];

const Intake: React.FC = () => {
  return (
    <div className="py-12 bg-gray-100">
      <div className="container mx-auto px-4  flex flex-col text-center space-y-10 ">
        <h2 className="font-bold text-3xl text-primary">Intake</h2>
        <div className="mx-auto max-w-lg w-full">
          <Table dataSource={dataSource} columns={columns} pagination={false} />
        </div>
      </div>
    </div>
  );
};

export default Intake;
