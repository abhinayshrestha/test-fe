import { useEffect, useRef, useState } from "react";
import {
  CaretDownFilled,
  CaretUpFilled,
  CheckCircleOutlined,
  CheckOutlined,
  DownOutlined,
  EnvironmentFilled,
  EnvironmentOutlined,
  PlusOutlined,
  UpOutlined,
} from "@ant-design/icons";
import { Button, Collapse } from "antd";
import { useNavigate } from "react-router-dom";

const { Panel } = Collapse;

interface Course {
  id: number;
  name: string;
  description: string;
  location: string;
  fees: string;
}

interface FacultyProps {
  faculty: string;
  courses: Course[];
}

interface ConsultancyFormProps {
  onApplyClick: () => void;
}

const Faculty: React.FC<ConsultancyFormProps> = ({ onApplyClick }) => {

  const handleApplyNow = () => {
    onApplyClick();
  };

  const facultyCoursesData: FacultyProps[] = [
    {
      faculty: "IT",
      courses: [
        {
          id: 1,
          name: "Web Development",
          description: "Learn  to build websites and web applications.",
          location: "Otawa",
          fees: "$200",
        },
        {
          id: 2,
          name: "Data Science",
          description: "Learn to analyze and interpret complex data.",
          location: "New York",
          fees: "$300",
        },
        {
          id: 3,
          name: "Graphic Design",
          description: "Learn to create visually appealing designs.",
          location: "Los Angeles",
          fees: "$250",
        },
        {
          id: 4,
          name: "Web Development",
          description: "Learn to build websites and web applications.",
          location: "Otawa",
          fees: "$200",
        },
        {
          id: 5,
          name: "Data Science",
          description: "Learn to analyze and interpret complex data.",
          location: "New York",
          fees: "$300",
        },
        {
          id: 6,
          name: "Graphic Design",
          description: "Learn to create visually appealing designs.",
          location: "Los Angeles",
          fees: "$250",
        },
      ],
    },
    {
      faculty: "Finance",
      courses: [
        {
          id: 1,
          name: "Financial Accounting",
          description: "Learn principles of financial accounting.",
          location: "San Fransico",
          fees: "$250",
        },
        {
          id: 2,
          name: "Investment Analysis",
          description: "Study techniques for investment analysis.",
          location: "New York",
          fees: "$350",
        },
        {
          id: 3,
          name: "Corporate Finance",
          description: "Explore corporate finance strategies.",
          location: "Los Angeles",
          fees: "$300",
        },
        {
          id: 4,
          name: "Financial Accounting",
          description: "Learn principles of financial accounting.",
          location: "San Fransico",
          fees: "$250",
        },
        {
          id: 5,
          name: "Investment Analysis",
          description: "Study techniques for investment analysis.",
          location: "New York",
          fees: "$350",
        },
        {
          id: 6,
          name: "Corporate Finance",
          description: "Explore corporate finance strategies.",
          location: "Los Angeles",
          fees: "$300",
        },
      ],
    },
    {
      faculty: "Management",
      courses: [
        {
          id: 1,
          name: "Strategic Management",
          description: "Learn strategic planning and management.",
          location: "San Fransico",
          fees: "$280",
        },
        {
          id: 2,
          name: "Operations Management",
          description: "Study principles of operations management.",
          location: "New York",
          fees: "$320",
        },
        {
          id: 3,
          name: "Human Resource Management",
          description: "Explore human resource management practices.",
          location: "Los Angeles",
          fees: "$300",
        },
        {
          id: 4,
          name: "Strategic Management",
          description: "Learn strategic planning and management.",
          location: "San Fransico",
          fees: "$280",
        },
        {
          id: 5,
          name: "Operations Management",
          description: "Study principles of operations management.",
          location: "New York",
          fees: "$320",
        },
        {
          id: 6,
          name: "Human Resource Management",
          description: "Explore human resource management practices.",
          location: "Los Angeles",
          fees: "$300",
        },
      ],
    },
    {
      faculty: "Science",
      courses: [
        {
          id: 1,
          name: "Physics",
          description: "Learn principles of classical physics.",
          location: "San Fransico",
          fees: "$280",
        },
        {
          id: 2,
          name: "Chemistry",
          description: "Study chemical properties and reactions.",
          location: "New York",
          fees: "$320",
        },
        {
          id: 3,
          name: "Biology",
          description: "Explore concepts of biology and life sciences.",
          location: "Los Angeles",
          fees: "$300",
        },
        {
          id: 4,
          name: "Physics",
          description: "Learn principles of classical physics.",
          location: "San Fransico",
          fees: "$280",
        },
        {
          id: 5,
          name: "Chemistry",
          description: "Study chemical properties and reactions.",
          location: "New York",
          fees: "$320",
        },
        {
          id: 6,
          name: "Biology",
          description: "Explore concepts of biology and life sciences.",
          location: "Los Angeles",
          fees: "$300",
        },
      ],
    },
    {
      faculty: "Mathematics",
      courses: [
        {
          id: 1,
          name: "Algebra",
          description: "Learn basic algebraic concepts.",
          location: "San Fransico",
          fees: "$250",
        },
        {
          id: 2,
          name: "Calculus",
          description: "Study differential and integral calculus.",
          location: "New York",
          fees: "$350",
        },
        {
          id: 3,
          name: "Statistics",
          description: "Explore statistical analysis techniques.",
          location: "Los Angeles",
          fees: "$300",
        },
        {
          id: 4,
          name: "Algebra",
          description: "Learn basic algebraic concepts.",
          location: "San Fransico",
          fees: "$250",
        },
        {
          id: 5,
          name: "Calculus",
          description: "Study differential and integral calculus.",
          location: "New York",
          fees: "$350",
        },
        {
          id: 6,
          name: "Statistics",
          description: "Explore statistical analysis techniques.",
          location: "Los Angeles",
          fees: "$300",
        },
      ],
    },
  ];

  const [numColumns, setNumColumns] = useState(4);
  const [expandedFaculties, setExpandedFaculties] = useState<string[]>([]);
  const [filteredFaculty, setFilteredFaculty] =
    useState<FacultyProps[]>(facultyCoursesData);

  const [filters, setFilters] = useState<{
    faculty: string[];
  }>({
    faculty: [],
  });

  const facultyCoursesDataRef = useRef(facultyCoursesData);
  const navigate = useNavigate();

  useEffect(() => {
    const updateNumColumns = () => {
      if (window.innerWidth < 640) {
        setNumColumns(2);
      } else if (window.innerWidth < 768) {
        setNumColumns(2);
      } else if (window.innerWidth < 1024) {
        setNumColumns(2);
      } else if (window.innerWidth < 1280) {
        setNumColumns(3);
      } else {
        setNumColumns(4);
      }
    };

    updateNumColumns();
    window.addEventListener("resize", updateNumColumns);

    return () => {
      window.removeEventListener("resize", updateNumColumns);
    };
  }, []);

  useEffect(() => {
    const filteredCourses = facultyCoursesDataRef.current.filter((course) => {
      return (
        filters.faculty.length === 0 || filters.faculty.includes(course.faculty)
      );
    });
    setFilteredFaculty(filteredCourses);
  }, [filters]);

  const navigateCourse = () => {
    navigate(`/course`);
  };

  const applyFilter = (faculty: string) => {
    setFilters((prevState) => ({
      ...prevState,
      faculty: prevState.faculty.includes(faculty)
        ? prevState.faculty.filter((item) => item !== faculty)
        : [...prevState.faculty, faculty],
    }));
  };

  const toggleCollapse = (faculty: string) => {
    setExpandedFaculties((prevState) =>
      prevState.includes(faculty)
        ? prevState.filter((item) => item !== faculty)
        : [...prevState, faculty]
    );
  };

  return (
    <div className="py-10 flex flex-col items-start  px-4 md:px-0 bg-white border">
      <div className="  mx-auto container relative">
        
        <div className="lg:absolute  border p-4 shadow-sm rounded-lg space-y-3 mb-10 text-center bg-yellow-300">
          <p>Are you intersted in Adelphi University ?</p>

          <Button
            className=" bg-green-500 text-white rounded-md hover:bg-green-600 cursor-pointer "
            onClick={handleApplyNow}
            style={{border:"none"}}
          >
            Book On Call Counseling
          </Button>
        </div>

        <div className="flex flex-col items-center gap-4">
          <h2 className="font-bold text-3xl text-primary ">Faculty</h2>
          <p className="text-gray-900 mb-12">
            Here are the details of courses offered by this university.
          </p>
        </div>

        <div className="grid grid-cols-1 sm:grid-cols-4 sm:gap-8 ">
          {/* Left side div */}
          <div
            className="border p-4 rounded-lg shadow-md bg-white   sm:sticky sm:top-20 mb-4"
            style={{ height: "fit-content" }}
          >
            <div className="">
              <h3 className="text-sm font-bold mb-2 border-b py-3">Filters</h3>
              <div>
                <h3 className="text-sm font-semibold mb-2 ">Faculties</h3>

                <div className="mb-4 border-b py-2 flex flex-wrap">
                  {facultyCoursesData.map((facultyCourse, index) => (
                    <span
                      className={`rounded-lg mr-1 mb-2 border p-1 cursor-pointer  ${
                        filters.faculty.includes(facultyCourse.faculty)
                          ? ""
                          : "group"
                      }`}
                      key={index}
                      onClick={() => applyFilter(facultyCourse.faculty)}
                    >
                      <span>
                        {filters.faculty.includes(facultyCourse.faculty) ? (
                          <CheckOutlined className="bg-green-400  p-1 text-white rounded-full mr-2 group-hover:bg-blue-800" />
                        ) : (
                          <PlusOutlined className="bg-blue-400  p-1 text-white rounded-full mr-2 group-hover:bg-blue-800" />
                        )}
                      </span>
                      {facultyCourse.faculty}
                    </span>
                  ))}
                </div>
              </div>
            </div>
          </div>

          {/* Right side div */}
          <div
            className=" col-span-3 border p-4 rounded-lg shadow-md"
            style={{ height: "fit-content" }}
          >
            <div>
              <div className="">
                {filteredFaculty.map((facultyCourse, index) => (
                  <div key={index} className="mb-6">
                    <h2 className="text-xl font-semibold mb-4 bg-gray-100 py-2 px-4">
                      {facultyCourse.faculty}
                    </h2>
                    <div
                      className={` grid grid-cols-1 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-4 `}
                    >
                      {facultyCourse.courses
                        .slice(
                          0,
                          expandedFaculties.includes(facultyCourse.faculty)
                            ? undefined
                            : numColumns
                        )
                        .map((course, courseIndex) => (
                          <div
                            className="px-6 py-4 shadow-sm border rounded-lg  flex flex-col justify-between cursor-pointer hover:shadow-md"
                            key={courseIndex}
                            onClick={navigateCourse}
                          >
                            <div className="h-full flex flex-col justify-center">
                              <div className="font-bold text-xl mb-2 text-center text-gray-32">
                                {course.name}
                              </div>
                              <p className="text-gray-700 text-sm text-center mb-4">
                                {course.description}
                              </p>
                            </div>
                            <div className="flex justify-between items-end border-t pt-2">
                              <p className="text-gray-700 text-sm flex items-center">
                                <EnvironmentFilled className="mr-1" />
                                {course.location}
                              </p>
                            </div>
                          </div>
                        ))}
                    </div>
                    {facultyCourse.courses.length > numColumns && (
                      <div className="text-center mt-4 border-b py-2">
                        <Button
                          onClick={() => toggleCollapse(facultyCourse.faculty)}
                        >
                          {expandedFaculties.includes(facultyCourse.faculty) ? (
                            <>
                              <CaretUpFilled /> See Less
                            </>
                          ) : (
                            <>
                              <CaretDownFilled /> See More
                            </>
                          )}
                        </Button>
                      </div>
                    )}
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Faculty;
