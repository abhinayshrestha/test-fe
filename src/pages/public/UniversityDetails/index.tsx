import { useState } from "react";
import { Layout } from "antd";
import { Content, Footer } from "antd/es/layout/layout";

import PageHeader from "./components/PageHeader";
import AboutSection from "./components/About";
import Faculty from "./components/Faculty";
import Intake from "./components/Intake";
import FormModal from "./components/FormModal";

const UniversityDetail = () => {

  const [isModalOpen, setIsModalOpen] = useState(false);

  const handleApplyClick = () => {
    setIsModalOpen(true);
  };

  const handleCloseModal = () => {
    setIsModalOpen(false);
  };
  
  return (
    <div>
      <Layout>
        <Content>
          <PageHeader onApplyClick={handleApplyClick} />
          <AboutSection />
          <Faculty onApplyClick={handleApplyClick}/>
          <FormModal isOpen={isModalOpen} onClose={handleCloseModal} />
        </Content>
      </Layout>
    </div>
  );
};

export default UniversityDetail;
