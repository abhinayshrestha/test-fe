// import React, { createContext, useState, useMemo, useCallback } from "react";

// interface FormContextData {
//   stepFormValues: any;
//   updateFormValues: (data: any) => void;
// }

// const StepContext = createContext<FormContextData>({
//   stepFormValues: {},
//   updateFormValues: () => {},
// });

// const StepProvider: React.FC = ({ children }: any) => {
//   const [stepFormValues, setFormValues] = useState<any>({});

//   const updateFormValues = useCallback((data: any) => {
//     setFormValues(data);
//   }, []);

//   console.log(stepFormValues);

//   const memoizedValue = useMemo(
//     () => ({ stepFormValues, updateFormValues }),
//     [stepFormValues, updateFormValues]
//   );

//   return (
//     <StepContext.Provider value={memoizedValue}>
//       {children}
//     </StepContext.Provider>
//   );
// };

// export { StepContext, StepProvider };
