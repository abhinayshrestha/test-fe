import React, { FC, useContext, useMemo } from "react";
import { StompSessionProvider } from "react-stomp-hooks";

interface SocketContextStructure {
    notificationDetails: unknown;
    setNotificationDetails: React.Dispatch<React.SetStateAction<any>>;
}

const SocketContext = React.createContext<SocketContextStructure>({
    notificationDetails: null,
    setNotificationDetails: () => {},
});

const SocketProvider: FC<{ children: React.ReactNode }> = ({ children }) => {
    const socketProvider = useMemo(
        () => (
            <StompSessionProvider
                url="https://testnpapi.hobes.tech/testnp/ws/gth?userId=1"
                debug={(str: string) => {
                    console.log(str);
                }}
                beforeConnect={() => {
                    return new Promise((resolve) => {
                        resolve();
                    });
                }}
            >
                {children}
            </StompSessionProvider>
        ),
        [children]
    );

    return socketProvider;
};

export const useSocketContext = () => {
    return useContext(SocketContext);
};

export default SocketProvider;
